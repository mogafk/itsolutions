--
-- Структура таблицы `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `things` varchar(255) NOT NULL,
  `count` varchar(255) NOT NULL,
  `count_per_one` varchar(255) NOT NULL,
  `prices_per_one` varchar(255) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `adress` longtext NOT NULL,
  `post_index` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `fio` varchar(255) NOT NULL,
  `whenadd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `whenchecked` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `checked` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `cart`
--

INSERT INTO `cart` (`id`, `user`, `things`, `count`, `count_per_one`, `prices_per_one`, `summary`, `adress`, `post_index`, `phone`, `fio`, `whenadd`, `whenchecked`, `checked`) VALUES
(14, 3, '5-2-3-4', '10', '1-1-3-5', '12000-10000-5000-3000', '52000', 'г.Магнитогорск, ул.Тевосяна 25, 222', '455000', '89525200185', 'Барановский Давид Игоревич', '2015-01-16 14:41:55', '2015-01-16 14:42:13', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `cats`
--

CREATE TABLE IF NOT EXISTS `cats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `cats`
--

INSERT INTO `cats` (`id`, `title`, `description`) VALUES
(1, 'Товары для отдыха', ''),
(2, 'Товары для дома', '');

-- --------------------------------------------------------

--
-- Структура таблицы `things`
--

CREATE TABLE IF NOT EXISTS `things` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `poster` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `count` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `things`
--

INSERT INTO `things` (`id`, `cat`, `title`, `price`, `poster`, `description`, `count`) VALUES
(2, 1, 'Лисопед', '10000', '2.gif', 'Ололо лололо123', '19'),
(3, 2, 'Как-то так', '5000', '3.jpg', 'Текст описания', '57'),
(4, 2, 'Лал', '3000', '1421322115.jpg', 'фыв', '45'),
(5, 1, 'Велосипед', '12000', '1421322168.jpg', 'тт', '29');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` longtext NOT NULL,
  `rank` varchar(255) NOT NULL DEFAULT 'user',
  `fio` varchar(255) NOT NULL,
  `adress` longtext NOT NULL,
  `post_index` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `rank`, `fio`, `adress`, `post_index`, `phone`) VALUES
(3, 'bimmy25@mail.ru', '20df036f267a13da7a99ee4866779bd2', 'admin', 'Барановский Давид Игоревич', 'г.Магнитогорск, ул.Тевосяна 25, 222', '455000', '89525200185');
