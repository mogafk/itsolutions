$(document).ready(function() {
    var loc = document.location.href; // Адрес текущей страницы в виде массива, 0 элемент - адрес сайта, 1 элемент - get-параметры
	var loc2 = loc.split("?"); // Адрес текущей страницы в виде массива, 0 элемент - адрес сайта, 1 элемент - get-параметры
	
	// Если в строке есть get-параметры, то подсвечиваем красным активный пункт в правом меню сайта
	if(loc2.length > 1) {
	    var param = loc2[1].split("&"); // get-параметры в виде массива
	    var param2 = [[],[]]; // Двумерный массив, положим сюда распиленные на части get-параметры
	    
	    // Перебираем параметры и раскладываем на 2 массива - ключ/значение
	    for(i=0;i<param.length;i++) {
	        p = param[i].split("=");
	    	param2[0].push(p[0]);
	    	param2[1].push(p[1]);
	    }
        
	    // Подсвечиваем красным активный пункт меню
	    if(param2[1][param2[0].indexOf('page')] == "cat" || param2[1][param2[0].indexOf('page')] == "item") {
	        if(param2[1][param2[0].indexOf('page')] == "cat") $("#cat-" + param2[1][param2[0].indexOf('id')]).addClass("now");
	    	if(param2[1][param2[0].indexOf('page')] == "item") $("#cat-" + param2[1][param2[0].indexOf('cat')]).addClass("now");
	    } else {
	        $("#" + param2[1][param2[0].indexOf('page')]).addClass("now");
	    }
	} else {
	    // Если get-параметров нет - значит, мы на главной
		$("#home").addClass("now");
	}
});

// Выставляем значение ползунка в input type=text под ним
function updateTextInput(val, id) {
    $("#rangetext-" + id).val(val);
}

// Выставлем значение input type=text в ползунок над ним
function updateRangeInput(val, id) {
    $("#range-" + id).val(val);
}

// Показать элемент
function showElem(elem) {
    $(elem).fadeIn(250);
}

// Скрыть элемент
function hideElem(elem) {
    $(elem).fadeOut(250);
}

// Показать/скрыть информацию о заказе
function slideElem(elem) {
    $(".cart_string .in_cart").animate({"opacity" : 0.7},250).slideUp(250);
	$(elem).css("display") == "none" ? $(elem).slideDown(250).animate({"opacity" : 1.0},250) : $(elem).animate({"opacity" : 0.7},250).slideUp(250);
}

// Показать/скрыть отчёт
function slideElem2(elem) {
    $(".in_cart").animate({"opacity" : 0.7},250).slideUp(250);
	$(elem).css("display") == "none" ? $(elem).slideDown(250).animate({"opacity" : 1.0},250) : $(elem).animate({"opacity" : 0.7},250).slideUp(250);
}

// Спойлер
function spoiler(elem, spoiler, text) {
    if($(spoiler).css("display") == "none") {
	    $(spoiler).slideDown(250).animate({"opacity" : 1.0},250);
		$(elem).text('Скрыть ' + text);
	} else {
	    $(spoiler).animate({"opacity" : 0.7},250).slideUp(250);
		$(elem).text('Показать ' + text);
	}
}

// Добавляем товар в корзину пользователя
function addToCart(id) {
    var cnt = $("#rangetext-" + id).val(); // Количество товара
	$.ajax({
        type: 'POST',
        url: '/?page=addToCart&action=system',
        data: 'id=' + id + '&cnt=' + cnt,
        success: function(data){
			if($("#cart_icon").css("display") == "none") $("#cart_icon").fadeIn(250);
			hideElem('#tocart-' + id);
			setTimeout(function() {
			    if(parseInt($("#range-" + id).attr("max")) - parseInt(cnt) <= 0) document.location.href = document.location.href;
				$("#range-" + id).attr("max", parseInt($("#range-" + id).attr("max")) - parseInt(cnt));
			    $("#range-" + id).val('1');
			    $("#rangetext-" + id).val('1');
			},250);
		}
    });
}

// Удаляем товар из корзины пользователя
function delFromCart(cnt, id) {
	$.ajax({
        type: 'POST',
        url: '/?page=delFromCart&action=system',
        data: 'id=' + id + '&cnt=' + cnt,
        success: function(data){
            $("#in_cart-" + id).fadeOut(250);
			setTimeout(function() {
			    document.location.href = document.location.href;
			},250);
		}
    });
}

// Отправка заказа
function cartGo() {
	$.ajax({
        type: 'POST',
        url: '/?page=cartGo&action=system',
        data: '',
        success: function(data){
		    $(".in_cart").fadeOut(250);
			setTimeout(function() {
			    document.location.href = "/?page=cart&section=send";
			},250);
		}
    });
}

// Удаление в админ-панели
function delItem(id, tbl, elem) {
	$.ajax({
        type: 'POST',
        url: '/?page=admin&action=system&go=delItem',
        data: 'tbl=' + tbl + '&id=' + id,
        success: function(data){
		    $(elem).css("background", "#AA0000").fadeOut(250);
			setTimeout(function() {
			    document.location.href = document.location.href;
			},250);
		}
    });
}

// "Живой" поиск по пользователям в админ-панели
function userSearch(query) {
	if(query.length >= 3 || query.length == 0) {
	    $.ajax({
            type: 'POST',
            url: '/?page=admin&action=system&go=userSearch',
            data: 'squery=' + query,
            success: function(data){
	    	    $("#user_search_result").html(data);
	    	}
        });
	};
}

// "Живой" поиск по товарам в админ-панели
function thingSearch(query) {
	if(query.length >= 3 || query.length == 0) {
	    $.ajax({
            type: 'POST',
            url: '/?page=admin&action=system&go=thingSearch',
            data: 'squery=' + query,
            success: function(data){
	    	    $("#user_search_result").html(data);
	    	}
        });
	};
}

// "Живой" поиск по заказам в админ-панели
function tradeSearch(query) {
	if(query.length >= 3 || query.length == 0) {
	    $.ajax({
            type: 'POST',
            url: '/?page=admin&action=system&go=tradeSearch',
            data: 'squery=' + query,
            success: function(data){
	    	    $("#user_search_result").html(data);
	    	}
        });
	};
}

// "Живой" поиск по отчётам в админ-панели
function blanksSearch(query) {
	if(query.length == 10 || query.length == 0) {
	    $.ajax({
            type: 'POST',
            url: '/?page=admin&action=system&go=blanksSearch',
            data: 'squery=' + query,
            success: function(data){
	    	    $("#user_search_result").html(data);
	    	}
        });
	};
}


// Обновить информацию о пользователе
function updateUser(id) {
	var arr = ["login", "fio", "adress", "post_index", "phone", "password"], params = "";
	for(i = 0; i < arr.length; i++) {
	    params += '&' + arr[i] + '=' + $("#user_settings_adm_form-" + id + " #" + arr[i] + "-" + id).val();
	}
	$.ajax({
        type: 'POST',
        url: '/?page=admin&action=system&go=updateUser',
        data: 'id=' + id + params,
        success: function(data){
			$("#adm_edit-" + id).fadeOut(250);
			setTimeout(function() {
			    document.location.href = document.location.href;
			},250);
		}
    });
}

// Обновить информацию о товаре
function updateThing(id) {
	var arr = ["title", "description", "price", "count"], params = "";
	for(i = 0; i < arr.length; i++) {
	    params += '&' + arr[i] + '=' + $("#user_settings_adm_form-" + id + " #" + arr[i] + "-" + id).val();
	}
	$.ajax({
        type: 'POST',
        url: '/?page=admin&action=system&go=updateThing',
        data: 'id=' + id + params,
        success: function(data){
			$("#adm_edit-" + id).fadeOut(250);
			setTimeout(function() {
			    document.location.href = document.location.href;
			},250);
		}
    });
}

// Список товаров в админ-панеле, раздел "товары"
function thingsInCat(id) {
	$.ajax({
        type: 'POST',
        url: '/?page=admin&action=system&go=thingsInCat',
        data: 'id=' + id,
        success: function(data){
			$(".adm_things").animate({"opacity" : 0.7},250).slideUp(250);
			$("#adm_things-" + id).html(data);
			$("#adm_things-" + id).css("display") == "none" ? $("#adm_things-" + id).slideDown(250).animate({"opacity" : 1.0},250) : $("#adm_things-" + id).animate({"opacity" : 0.7},250).slideUp(250);
		}
    });
}

// Смена картинки товара
function change_poster(id) {
    $("#change_poster-" + id).trigger('click');
}

// Загрузка картинки
function upload_poster(id) {
	tourl = '/?page=admin&action=system&go=uploadPoster&id=' + id;
	$$f({
		formid: 'upload_poster-' + id,
		url: tourl
	});
	$("#change_poster-" + id).val('');
}

// Переключение разделов в разделе "заказы" в админ-панели
function openTradeSection(open, close) {
	$.ajax({
        type: 'POST',
        url: '/?page=admin&action=system&go=openTradeSection',
        data: 'open=' + open,
        success: function(data){
            document.location.href = document.location.href;
		}
    });
}

// Смена статуса заказа на "выполнен"
function tradeCheck(id) {
	$.ajax({
        type: 'POST',
        url: '/?page=admin&action=system&go=tradeCheck',
        data: 'id=' + id,
        success: function(data){
			hideElem("#cart_string-" + id);
			setTimeout(function() {
			    document.location.href = "/?page=admin";
			},250);
		}
    });
}

// Очищаем список заказов от uid пользователя
function clearTrade() {
    $(".admin_trade_header_search").animate({"opacity" : 0.7},250).slideUp(250);
	setTimeout(function() {
	    document.location.href = "/?page=admin&section=trade";
	},500);
}