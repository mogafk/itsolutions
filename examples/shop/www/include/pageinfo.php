<?php
// Узнаём адрес текущей страницы из GET-параметра Page
(isset($_GET["page"]) || trim(chop($_GET["page"])) != "") ? $page = $_GET["page"] : $page = "index";

// Если у нас страница категории - цепляем о ней информацию
if($page == "cat") {
    $cat = InfoById($_GET["id"], "cats");
	$sections[$page] = $cat["title"];
};

// Если у нас страница товара - цепляем о нем информацию
if($page == "item") {
    $item = InfoById($_GET["id"], "things");
	$cat = InfoById($item["cat"], "cats");
	$sections[$page] = $item["title"];
};

// Если поиск - ищем по базе
if($page == "search") {
    $search = search(sip($_GET["query"]));
};

// Проверяем, авторизован ли пользователь
if(isset($_SESSION["user"]["id"]) && (int)$_SESSION["user"]["id"] > 0) {
	// Выход из системы
	if(isset($_GET["action"]) && sip($_GET["action"]) == "exit") {
	    session_destroy();
		echo '<script>document.location.href = "/?page='.$page.'";</script>';
	};
	// Смена пароля
	if(isset($_GET["action"]) && sip($_GET["action"]) == "settings") {
	    change_password($_POST, $page);
	};
	// Смена контактных данных пользователя
	if(isset($_GET["action"]) && sip($_GET["action"]) == "settings2") {
	    change_user_settings($_POST, $page);
	};
	// Раздел "мои заказы"
	if($page == "cart") {
	    (!isset($_GET["section"]) || trim(chop($_GET["section"])) == "") ? $cart_section = $_COOKIE["cart_section"] : $cart_section = $_GET["section"];
	    setcookie("cart_section", $cart_section);
		if(!isset($_COOKIE["cart_section"]) || trim(chop($_COOKIE["cart_section"])) == "") {
		    echo '<script>document.location.href = "/?page='.$page.'&section=now";</script>';
			setcookie("cart_section", "now");
		};
	};
	// Добавление товара в корзину пользователя
	if($page == "addToCart") {
	    addToCart($_POST);
	};
	// Удаление товара из корзины
	if($page == "delFromCart") {
	    delFromCart($_POST);
	};
	if($page == "cartGo") {
	    cartGo();
	};
	// Раздел администрирования
	if($_SESSION["user"]["rank"] == "admin" && $page == "admin") {
	    (!isset($_GET["section"]) || trim(chop($_GET["section"])) == "") ? $admin_section = $_COOKIE["admin_section"] : $admin_section = $_GET["section"];
	    setcookie("admin_section", $admin_section);
		if(!isset($_COOKIE["admin_section"]) || trim(chop($_COOKIE["admin_section"])) == "") {
		    echo '<script>document.location.href = "/?page='.$page.'&section=trade";</script>';
			setcookie("admin_section", "trade");
		};
		if(isset($_GET["go"])) {
		    $it = trim(chop($_GET["go"]));
			switch($it) {
			    case("createcat"):
				    create_cat(sip($_POST["title"]));
					break;
				case("updatecat"):
				    create_cat(sip($_POST["title"]), sip($_POST["id"]));
					break;
				case("delItem"):
				    delItem(sip($_POST["tbl"]), sip($_POST["id"]));
					break;
				case("updateUser"):
				    updateUser($_POST);
					break;
				case("updateThing"):
				    updateThing($_POST);
					break;
				case("thingsInCat"):
				    thingsInCat(sip($_POST["id"]));
					break;
				case("uploadPoster"):
					upload($_FILES["file"], sip($_GET["id"]));
					break;
				case("addNewThing"):
				    addNewThing($_POST);
					break;
				case("openTradeSection"):
				    setcookie("openTradeSection", sip($_POST["open"]));
					break;
				case("tradeCheck"):
				    tradeCheck(sip($_POST["id"]));
					break;
				case("blanksSearch"):
                    if(trim(chop(sip($_POST["squery"]))) == "") {
                        include 'templates/admin/blanks_tpl.php';
                    } else {
                        $things = blanks(sip($_POST["squery"]).' 00:00:00', sip($_POST["squery"]).' 23:59:59');
						$array_text[0] = sip($_POST["squery"]);
						$key = 0;
						include 'templates/admin/blanks_search.php';
                    }
					break;
				case("tradeSearch"):
                    if(trim(chop(sip($_POST["squery"]))) == "") {
                        include 'templates/admin/trade_tpl.php';
                    } else {
                        tradeSearch(sip($_POST["squery"]));
                    }
					break;
				case("userSearch"):
                    if(trim(chop(sip($_POST["squery"]))) == "") {
                        adminUsers(1, "empty_search");
                    } else {
                        adminUsers(0, sip($_POST["squery"]));
                    }
					break;
				case("thingSearch"):
                    if(trim(chop(sip($_POST["squery"]))) == "") {
                        include 'templates/admin/things_tpl.php';
                    } else {
                        thingSearch(sip($_POST["squery"]));
                    }
					break;
			}
		};
	};
} else {
    // Если не авторизован, проверяем, может он пытается авторизоваться
	if(isset($_GET["action"]) && trim(chop($_GET["action"])) == "auth" && isset($_POST["login"]) && isset($_POST["pass"]) && trim(chop($_POST["login"])) != "" && trim(chop($_POST["pass"])) != "") {
	    // Если это авторизация и значения полей не пустые, то авторизуем пользователя
		auth($_POST, $page);
	};
}
?>