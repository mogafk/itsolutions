<?php
/*
 * Авторизация
 * @param $data array Данные из формы
 * @param $page string Текущая страница
 */
function auth($data, $page) {
    global $mysqli; // Подключаем нашу БД
	// Ищем пользователя с таким логином и паролем в нашей БД
	$res = $mysqli->query("SELECT * FROM `users` WHERE `login`='".sip($data["login"])."' AND `password`='".md5(sip($data["pass"]).sip($data["login"]))."'");
	// Если такой пользователь существует
	if($res->num_rows > 0) {
	    // Получаем из выборки его ID, ранг и логин
		while($user = $res->fetch_assoc()) {
		     // И заносим их в нашу сессию
			 $_SESSION["user"]["id"] = $user["id"];
			 $_SESSION["user"]["login"] = $user["login"];
			 $_SESSION["user"]["rank"] = $user["rank"];
			 $_SESSION["user"]["fio"] = $user["fio"];
			 $_SESSION["user"]["phone"] = $user["phone"];
			 $_SESSION["user"]["adress"] = $user["adress"];
			 $_SESSION["user"]["post_index"] = $user["post_index"];
		}
		$res->close(); // Не забываем закрыть результат выборки
	} else {
	    // Если пользователя с таким логином не существует, то заносим его в базу
		// В базе пароль хранится в зашифрованном виде(в виде хэша md5)
		if($mysqli->query("INSERT INTO `users`(`login`, `password`, `rank`) VALUES ('".sip($data["login"])."', '".md5(sip($data["pass"]).sip($data["login"]))."', 'user')")) {
		    // Если пользователь добавлен - авторизуем, как показано выше
			$res = $mysqli->query("SELECT * FROM `users` WHERE `login`='".sip($data["login"])."' AND `password`='".md5(sip($data["pass"]).sip($data["login"]))."'");
	        // Если такой пользователь существует
	        if($res->num_rows > 0) {
	            // Получаем из выборки его ID, ранг и логин
	        	while($user = $res->fetch_assoc()) {
	        	     // И заносим их в нашу сессию
	        		 $_SESSION["user"]["id"] = $user["id"];
	        		 $_SESSION["user"]["login"] = $user["login"];
	        		 $_SESSION["user"]["rank"] = $user["rank"];
                     $_SESSION["user"]["fio"] = $user["fio"];
                     $_SESSION["user"]["phone"] = $user["phone"];
                     $_SESSION["user"]["adress"] = $user["adress"];
					 $_SESSION["user"]["post_index"] = $user["post_index"];
	        	}
	        	$res->close(); // Не забываем закрыть результат выборки
			};
		};
	}
	// Переадресовываем на главную после авторизации(с помощью javascript, т.к. header: Location не всегда работает как надо)
	echo '<script>document.location.href = "/?page='.$page.'";</script>';
}

/*
 * Смена пароля
 * @param $data array Данные из формы
 * @param $page string Текущая страница
 */
function change_password($data, $page) {
    global $mysqli; // Подключаем БД
	// Ищем пользователя по базе
	$res = $mysqli->query("SELECT * FROM `users` WHERE `id`='{$_SESSION["user"]["id"]}'");
	while($user = $res->fetch_assoc()) {
	    // Проверяем старый пароль
		if(md5(sip($data["oldpass"]).sip($_SESSION["user"]["login"])) == $user["password"]) {
		    // Если старый пароль верен, то применяем изменения
			$mysqli->query("UPDATE `users` SET `password`='".md5(sip($data["newpass"]).sip($_SESSION["user"]["login"]))."' WHERE `id`='{$_SESSION["user"]["id"]}'");
		};
	}
	$res->close();
	echo '<script>document.location.href = "/?page='.$page.'";</script>';
}

/*
 * Смена контактных данных пользователя
 * @param $data array Данные из формы
 * @param $page string Текущая страница
 */
function change_user_settings($data, $page) {
    global $mysqli; // Подключаем БД
	// Ищем пользователя по базе
	$res = $mysqli->query("SELECT * FROM `users` WHERE `id`='{$_SESSION["user"]["id"]}'");
	while($user = $res->fetch_assoc()) {
	    // Проверяем старый пароль
		if(md5(sip($data["oldpass"]).sip($_SESSION["user"]["login"])) == $user["password"]) {
		    // Если старый пароль верен, то применяем изменения
			$mysqli->query("UPDATE `users` SET `fio`='".sip($data["fio"])."', `adress`='".sip($data["adress"])."', `post_index`='".sip($data["post_index"])."', `phone`='".sip($data["phone"])."' WHERE `id`='{$_SESSION["user"]["id"]}'");
			$_SESSION["user"]["fio"] = sip($data["fio"]);
			$_SESSION["user"]["phone"] = sip($data["phone"]);
			$_SESSION["user"]["adress"] = sip($data["adress"]);
			$_SESSION["user"]["post_index"] = sip($data["post_index"]);
		};
	}
	$res->close();
	echo '<script>document.location.href = "/?page='.$page.'";</script>';
}

/*
 * Информация по ID
 * @param $id integer ID
 * @param $tbl string таблица, из которой взять информацию
 * @return $array array Массив с информацией.
 */
function InfoById($id, $tbl) {
    global $mysqli;
	$res = $mysqli->query("SELECT * FROM `{$tbl}` WHERE `id`='{$id}'");
	while($row = $res->fetch_assoc()) {
	    $array = $row;
	}
	$res->close();
	return $array;
}

/*
 * Поиск
 * @param $q string Запрос с формы
 * @return $search array Массив с информацией.
 */
function search($q) {
    global $mysqli;
	$i = 0;
	// В каких таблицах искать
	$from = array("things", "cats");
	
	// Ищем только если задано не меньше n символов(настраивается в config.php)
	if(mb_strlen($q) >= SEARCH_MIN_LENGHT) {
	    // Ищем поочерёдно в каждой таблице из массива $from
		foreach($from as $tbl) {
	        // Формируем регистронезависимый запрос
			$res = $mysqli->query("SELECT * FROM `{$tbl}` WHERE LOWER(`title`) LIKE LOWER('%{$q}%') OR LOWER(`description`) LIKE LOWER('%{$q}%')");
	    	// Если результаты есть - кидаем всю инфу в массив с ссответствующим индексом
			if($res->num_rows >= 1) {
	    	    while($row = $res->fetch_assoc()) {
	    	        $search[$i] = $row;
	    	    	$i++;
	    	    }
	    	    $res->close();
	    	};
	    }
	};
	return $search;
}

/*
 * Взять все записи из таблицы $tbl
 * @param $tbl string таблица, из которой взять информацию
 * @param $order string по какому полю сортировать
 * @return $array array Массив с информацией.
 */
function getAll($tbl, $order="") {
    global $mysqli;
	$i = 0;
	$order == "" ? $q = "SELECT * FROM `{$tbl}`" : $q = "SELECT * FROM `{$tbl}` ".$order;
	$res = $mysqli->query($q);
	while($row = $res->fetch_assoc()) {
	    $array[$i] = $row;
		$i++;
	}
	$res->close();
	return $array;
}

/*
 * Список товаров
 * @param $cat integer ID категории товара. Если 0 или не указано - выводить все товары.
 * @return $things array Массив с информацией о товарах.
 */
function show_things($cat=-1) {
    global $mysqli; // Подключаем БД
	$i = 0; // Индекс для массива
	// Формируем запрос
	$cat == -1 ? $q = "SELECT * FROM `things` ORDER BY `title`" : $q = "SELECT * FROM `things` WHERE `cat`='{$cat}' ORDER BY `title`";
	$res = $mysqli->query($q); // Выполняем запрос
	while($row = $res->fetch_assoc()) {
	    $things[$i] = $row;
		// Узнаем также имя категории товара
		$res2 = $mysqli->query("SELECT `title` as ttl FROM `cats` WHERE `id`='{$row["cat"]}'");
		while($catname = $res2->fetch_object()->ttl) {
		    $things[$i]["catname"] = $catname;
		}
		$res2->close();
		$i++;
	}
	$res->close();
	return $things;
}

/*
 * Добавление товара в корзину
 * @param $data array ID и количество товара
 */
function addToCart($data) {
	if(isset($_COOKIE["cart_ids"]) && trim(chop($_COOKIE["cart_ids"])) != "") {
        $cart_ids = $_COOKIE["cart_ids"];
    	$cart_cnt = $_COOKIE["cart_cnt"];
    	$delim = "-";
    } else {
        $cart_ids = "";
    	$cart_cnt = "";
    	$delim = "";
    }
    $index = -1;
    if($cart_ids != "") {
    	$c_ids = explode("-", $cart_ids);
        $c_cnt = explode("-", $cart_cnt);
    	foreach($c_ids as $key => $val) {
            if((int)$val == (int)$data["id"]) {
    		    $index = $key;
    			break;
    		};
        }
    	if($index != -1) {
    	    $c_cnt[$index] = (int)$c_cnt[$index] + (int)$data["cnt"];
            $cart_ids = implode("-", $c_ids);
            $cart_cnt = implode("-", $c_cnt);
            setcookie("cart_ids", $cart_ids, time()+3600);
            setcookie("cart_cnt", $cart_cnt, time()+3600);
    	} else {
            setcookie("cart_ids", $cart_ids.$delim.sip($data["id"]), time()+3600);
            setcookie("cart_cnt", $cart_cnt.$delim.sip($data["cnt"]), time()+3600);
    	}
    } else {
        setcookie("cart_ids", sip($data["id"]), time()+3600);
        setcookie("cart_cnt", sip($data["cnt"]), time()+3600);
    }
}

/*
 * Удаление товара из корзины
 * @param $data array ID и количество товара
 */
function delFromCart($data) {
	$cart_ids = explode("-", $_COOKIE["cart_ids"]);
    $cart_cnt = explode("-", $_COOKIE["cart_cnt"]);
    foreach($cart_ids as $key => $val) {
        if((int)$val == (int)$data["id"]) {
    	    $index = $key;
    		break;
    	};
    }
    unset($cart_ids[$index]);
    unset($cart_cnt[$index]);
    if(count($cart_ids) > 0) {
	    $c_ids = implode("-", $cart_ids);
        $c_cnt = implode("-", $cart_cnt);
	} else {
	    $c_ids = "";
		$c_cnt = "";
	}
    setcookie("cart_ids", $c_ids, time()+3600);
    setcookie("cart_cnt", $c_cnt, time()+3600);
}

/*
 * Отправка заказа
 * @param $data array ID и количество товара
 */
function cartGo() {
    global $mysqli;
	$cart_ids = explode("-", $_COOKIE["cart_ids"]);
	$cart_cnt = explode("-", $_COOKIE["cart_cnt"]);
	$count = 0;
	$summary = 0;
	foreach($cart_ids as $key => $val) {
	    $res = $mysqli->query("SELECT `price` FROM `things` WHERE `id`='{$val}'");
		while($row = $res->fetch_assoc()) {
		    $cart_prices[$key] = $row["price"];
		}
		$res->close();
		$res = $mysqli->query("SELECT `count` FROM `things` WHERE `id`='{$val}'");
        while($row = $res->fetch_assoc()) {
            $mysqli->query("UPDATE `things` SET `count`='".((int)$row["count"] - (int)$cart_cnt[$key])."' WHERE `id`='{$val}'");
        }
        $res->close();
		$count += (int)$cart_cnt[$key];
		$summary += (int)$cart_prices[$key]*(int)$cart_cnt[$key];
	}
	$c_prices = implode("-", $cart_prices);
	$mysqli->query("INSERT INTO `cart`(
	                                   `user`, 
									   `things`, 
									   `count`, 
									   `count_per_one`, 
									   `prices_per_one`, 
									   `summary`, 
									   `adress`, 
									   `post_index`, 
									   `phone`, 
									   `fio`, 
									   `checked`
									) VALUES (
									    '".$_SESSION["user"]["id"]."',
										'".$_COOKIE["cart_ids"]."',
										'".$count."',
										'".$_COOKIE["cart_cnt"]."',
										'".$c_prices."', 
										'".$summary."',
										'".$_SESSION["user"]["adress"]."',
										'".$_SESSION["user"]["post_index"]."',
										'".$_SESSION["user"]["phone"]."',
										'".$_SESSION["user"]["fio"]."',
										'0'
									)");
	setcookie("cart_ids", "", time()-3600);
	setcookie("cart_cnt", "", time()-3600);
}

/*
 * Заказы пользователя
 * @param $id integer ID пользователя
 * @param $checked Статус заказа
 * @param $page текущая страница
 *
 * @return $cart array список заказов
 */
function userCart($id, $checked, $page) {
    global $mysqli;
	$i = 0;
	$id == 0 ? $q = "SELECT * FROM `cart` WHERE `checked`='{$checked}' ORDER BY `whenadd` DESC LIMIT ".($page-1)*CART_PER_PAGE.", ".CART_PER_PAGE : $q = "SELECT * FROM `cart` WHERE `user`='{$id}' AND `checked`='{$checked}' ORDER BY `whenadd` DESC LIMIT ".($page-1)*CART_PER_PAGE.", ".CART_PER_PAGE;
	$res = $mysqli->query($q);
	while($row = $res->fetch_assoc()) {
	    $cart[$i] = $row;
		$date = explode(" ", $row["whenadd"]);
		$rusdate = explode("-", $date[0]);
		$time = explode(":", $date[1]);
		$cart[$i]["whenadd"] = $rusdate[2].'.'.$rusdate[1].'.'.$rusdate[0].' в '.$time[0].':'.$time[1];
		$date = explode(" ", $row["whenchecked"]);
		$rusdate = explode("-", $date[0]);
		$time = explode(":", $date[1]);
		$cart[$i]["whenchecked"] = $rusdate[2].'.'.$rusdate[1].'.'.$rusdate[0].' в '.$time[0].':'.$time[1];
		$i++;
	}
	$res->close();
	return $cart;
}

/*
 * Страницы для постраничной навигации в разделе "Мои заказы"
 * @param $id integer ID пользователя
 * @param $checked Статус заказа
 * @param $page текущая страница
 *
 * @return $pages string ссылки на страницы
 */
function userCartPages($id, $checked, $page) {
    global $mysqli;
	$pages = array();
	$k = 0;
	$id == 0 ? $q = "SELECT COUNT(*) as cnt FROM `cart` WHERE `checked`='{$checked}'" : $q = "SELECT COUNT(*) as cnt FROM `cart` WHERE `user`='{$id}' AND `checked`='{$checked}'";
	$res = $mysqli->query($q);
	while($cnt = $res->fetch_object()->cnt) {
	    $all = (int)$cnt;
		$all%CART_PER_PAGE == 0 ? $all_pages = $all/CART_PER_PAGE : $all_pages = ($all/($all-($all%CART_PER_PAGE)))+1;
	}
	$res->close();
	$page = (int)$page;
	$from = $page-2;
	$to = $page+2;
	if($page == 1 || $page == 2) $to = 5;
	if($page == $all_pages || $page == $all_pages-1) $from = $all_pages-4;
	for($i=$from; $i<=$to; $i++) {
	    if($i > 0 && $i <= $all_pages) $pages[$k] = $i;
		$k++;
	}
	return $pages;
}

/*
 * ######  ###     ###   ###  ##  ###    ##
 * ##  ##  ## ##   ## # # ##      ## #   ##
 * ######  ##  ##  ##  #  ##  ##  ##  #  ##
 * ##  ##  ## ##   ##     ##  ##  ##   # ##
 * ##  ##  ###     ##     ##  ##  ##    ###
 *
 */
 
 /*
 * Добавление/изменение категории
 * @param $title string Имя категории
 * @param $id integer ID категории
 */
function create_cat($title, $id="") {
    global $mysqli;
	$id == "" ? $q = "INSERT INTO `cats`(`title`) VALUES ('{$title}')" : $q = "UPDATE `cats` SET `title`='{$title}' WHERE `id`='{$id}'";
	$mysqli->query($q);
	echo '<script>document.location.href = "/?page=admin&section=cats";</script>';
}

 /*
 * Удаление в админ-панели
 * @param $tbl string Имя таблицы бд
 * @param $id integer ID элемента
 */
function delItem($tbl, $id) {
    global $mysqli;
	if($tbl == "cats") $mysqli->query("UPDATE `things` SET `cat`='0' WHERE `cat`='{$id}'");
	if($tbl == "cart") {
	    $res = $mysqli->query("SELECT * FROM `cart` WHERE `id`='{$id}'");
		while($row = $res->fetch_assoc()) {
		    $cart_ids = explode("-", $row["things"]);
			$cart_cnt = explode("-", $row["count"]);
			foreach($cart_ids as $key => $val) {
			    $res2 = $mysqli->query("SELECT * FROM `things` WHERE `id`='{$val}'");
				while($row2 = $res2->fetch_assoc()) {
				    $mysqli->query("UPDATE `things` SET `count`='".((int)$row2["count"] + (int)$cart_cnt[$key])."' WHERE `id`='{$val}'");
				}
				$res2->close();
			}
		}
		$res->close();
	};
	$mysqli->query("DELETE FROM `{$tbl}` WHERE `id`='{$id}'");
}

/*
 * Пользователи для показа на страницу админ-панели "пользователи"
 * @param $page integer текущая страница
 * @param $query string Поисковый запрос
 *
 * @return $users array массив с информацией
 */
function adminUsers($page, $query="") {
    global $mysqli;
	$i = 0;
	($query == "" || $query == "empty_search") ? $q = "SELECT * FROM `users` ORDER BY `fio` LIMIT ".($page-1)*ADMIN_USERS_PER_PAGE.", ".ADMIN_USERS_PER_PAGE : $q = "SELECT * FROM `users` WHERE LOWER(`id`) LIKE LOWER('%{$query}%') OR LOWER(`login`) LIKE LOWER('%{$query}%') OR LOWER(`fio`) LIKE LOWER('%{$query}%') OR LOWER(`adress`) LIKE LOWER('%{$query}%') OR LOWER(`phone`) LIKE LOWER('%{$query}%') ORDER BY `fio`";
	$res = $mysqli->query($q);
	$cnt = $res->num_rows;
	while($row = $res->fetch_assoc()) {
	    $users[$i] = $row;
		$i++;
	}
	$res->close();
	if($query == "") {
	    return $users;
	} else {
	    if($query == "empty_search") {
		    include 'templates/admin/user_tpl.php';
		} else {
		    if($cnt > 0) {
		        include 'templates/admin/user_search.php';
		    } else {
		        echo '<span class="grey">По вашему запросу </span><span class="grey bold">"'.$query.'"</span><span class="grey"> ничего не найдено.</span>';
		    }
		}
	}
}

/*
 * Страницы для постраничной навигации в разделе "Пользователи"
 * @param $page текущая страница
 *
 * @return $pages string ссылки на страницы
 */
function adminUserPages($page) {
    global $mysqli;
	$pages = array();
	$k = 0;
	$res = $mysqli->query("SELECT COUNT(*) as cnt FROM `users`");
	while($cnt = $res->fetch_object()->cnt) {
	    $all = (int)$cnt;
		$all%ADMIN_USERS_PER_PAGE == 0 ? $all_pages = $all/ADMIN_USERS_PER_PAGE : $all_pages = ($all/($all-($all%ADMIN_USERS_PER_PAGE)))+1;
	}
	$res->close();
	$page = (int)$page;
	$from = $page-2;
	$to = $page+2;
	if($page == 1 || $page == 2) $to = 5;
	if($page == $all_pages || $page == $all_pages-1) $from = $all_pages-4;
	for($i=$from; $i<=$to; $i++) {
	    if($i > 0 && $i <= $all_pages) $pages[$k] = $i;
		$k++;
	}
	return $pages;
}

/*
 * Изменение информации пользователя
 * @param $data array Данные с формы
 */
function updateUser($data) {
    global $mysqli;
	$params = "";
	foreach($data as $key => $val) {
	    if(trim(chop($key)) != "login" && trim(chop($key)) != "id" && trim(chop($key)) != "password" && trim(chop($val)) != "") $params .= "`{$key}`='{$val}', ";
		if(trim(chop($key)) == "password" && trim(chop($val)) != "") $params .= "`{$key}`='".md5(sip($data["password"]).sip($data["login"]))."', ";
	}
	$params = mb_substr($params, 0, mb_strlen($params)-2);
	$mysqli->query("UPDATE `users` SET {$params} WHERE `id`='{$data["id"]}'");
}

/*
 * Изменение информации товара
 * @param $data array Данные с формы
 */
function updateThing($data) {
    global $mysqli;
	$params = "";
	foreach($data as $key => $val) {
	    if(trim(chop($key)) != "id" && trim(chop($val)) != "") $params .= "`{$key}`='{$val}', ";
	}
	$params = mb_substr($params, 0, mb_strlen($params)-2);
	$mysqli->query("UPDATE `things` SET {$params} WHERE `id`='{$data["id"]}'");
}

/*
 * Список товаров в админ-панели
 * @param $id integer ID категории
 */
function thingsInCat($id) {
    global $mysqli;
	$i = 0;
	if($id > 0) {
	    $res = $mysqli->query("SELECT `title` AS ttl FROM `cats` WHERE `id`='{$id}'");
		while($name = $res->fetch_object()->ttl) {
		    $catname = $name;
		}
		$res->close();
	} else {
	    $catname = "Без категории";
	}
	$res = $mysqli->query("SELECT * FROM `things` WHERE `cat`='{$id}' ORDER BY `title`");
	while($row = $res->fetch_assoc()) {
	    $things[$i] = $row;
		$things[$i]["catname"] = $catname;
		$i++;
	}
	$res->close();
	if(count($things) > 0) {
	    include 'templates/admin/things_in_cat.php';
	} else {
	    echo '<span class="grey">Нет товаров в данной категории.</span>';
	}
}

/*
 * Поиск по товарам
 * @param $query string Поисковый запрос
 */
function thingSearch($query) {
    global $mysqli;
	$i = 0;
	$res = $mysqli->query("SELECT * FROM `things` WHERE LOWER(`id`) LIKE LOWER('%{$query}%') OR LOWER(`title`) LIKE LOWER('%{$query}%') OR LOWER(`description`) LIKE LOWER('%{$query}%') ORDER BY `title`");
	while($row = $res->fetch_assoc()) {
	    $things[$i] = $row;
        if($row["cat"] > 0) {
            $res2 = $mysqli->query("SELECT `title` AS ttl FROM `cats` WHERE `id`='{$row["cat"]}'");
        	while($name = $res2->fetch_object()->ttl) {
        	    $catname = $name;
        	}
        	$res2->close();
        } else {
            $catname = "Без категории";
        }
		$things[$i]["catname"] = $catname;
		$i++;
	}
	$res->close();
	if(count($things) > 0) {
	    echo '<div class="grey" style="margin: 5px auto;">
			      Результаты поиска по запросу <span class="grey bold">"'.$query.'"</span>:
			  </div>';
		include 'templates/admin/things_in_cat.php';
	} else {
	    echo '<span class="grey">По вашему запросу </span><span class="grey bold">"'.$query.'"</span><span class="grey"> ничего не найдено.</span>';
	}
}

/*
 * Поиск по заказам
 * @param $query string Поисковый запрос
 */
function tradeSearch($query) {
    global $mysqli;
	$i = 0;
	$ids = "";
	$res = $mysqli->query("SELECT * FROM `things` WHERE LOWER(`title`) LIKE LOWER('%{$query}%') OR LOWER(`description`) LIKE LOWER('%{$query}%') ORDER BY `title`");
	while($row = $res->fetch_assoc()) {
	    $things[$i] = $row["id"];
		$i++;
	}
	$res->close();
	$i = 0;
	foreach($things as $item) {
	    $ids .= " OR LOWER(`things`) LIKE LOWER('%{$item}%')";
	}
	$q = "SELECT * FROM `cart` WHERE (LOWER(`id`) LIKE LOWER('%{$query}%'){$ids} OR LOWER(`adress`) LIKE LOWER('%{$query}%') OR LOWER(`phone`) LIKE LOWER('%{$query}%') OR LOWER(`fio`) LIKE LOWER('%{$query}%') OR LOWER(`whenadd`) LIKE LOWER('%{$query}%')) AND `checked`='0' ORDER BY `whenadd`";
	for($k = 0; $k < 2; $k++) {
	    $res = $mysqli->query($q);
	    while($row = $res->fetch_assoc()) {
	        $cart[$i] = $row;
	    	$i++;
	    }
	    $res->close();
		$q = "SELECT * FROM `cart` WHERE (LOWER(`id`) LIKE LOWER('%{$query}%'){$ids} OR LOWER(`adress`) LIKE LOWER('%{$query}%') OR LOWER(`phone`) LIKE LOWER('%{$query}%') OR LOWER(`fio`) LIKE LOWER('%{$query}%') OR LOWER(`whenchecked`) LIKE LOWER('%{$query}%')) AND `checked`='1' ORDER BY `whenchecked`";
	}
	if(count($cart) > 0) {
	    echo '<div class="grey" style="margin: 5px auto;">
			      Результаты поиска по запросу <span class="grey bold">"'.$query.'"</span>:
			  </div>';
		include 'templates/admin/trade_search.php';
	} else {
	    echo '<span class="grey">По вашему запросу </span><span class="grey bold">"'.$query.'"</span><span class="grey"> ничего не найдено.</span>';
	}
}

/*
 * Формирование отчётов
 * @param $start string Дата, с которой начинать отчёт
 * @param $finish string Дата, на которой закончить отчёт
 * @return $things array Массив с информацией
 */
function blanks($start, $finish) {
    global $mysqli;
	$i = 0;
	$res = $mysqli->query("SELECT * FROM `things` ORDER BY `title`");
	while($row = $res->fetch_assoc()) {
	    $cat = InfoById($row["cat"], "cats");
		$things[$i]["id"] = $row["id"];
		$things[$i]["title"] = $row["title"];
		$things[$i]["poster"] = $row["poster"];
		$things[$i]["checked"] = 0;
		$things[$i]["catname"] = $cat["title"];
		$res2 = $mysqli->query("SELECT * FROM `cart` WHERE `checked`='1' AND `whenchecked`>='{$start}' AND `whenchecked`<='{$finish}' AND (LOWER(`things`) LIKE LOWER('%{$row["id"]}%'))");
		if($res2->num_rows > 0) {
		    while($row2 = $res2->fetch_assoc()) {
		        $cart_ids = explode("-", $row2["things"]);
				$cart_cnt = explode("-", $row2["count_per_one"]);
				foreach($cart_ids as $key => $val) {
				    if((int)$val == (int)$row["id"]) {
					    $things[$i]["checked"] += (int)$cart_cnt[$key];
						break;
					};
				}
		    }
		    $res2->close();
		};
		$i++;
	}
	$res->close();
	return $things;
}

/*
 * Добавление товара в админ-панели
 * @param $data array Данные с формы
 */
function addNewThing($data) {
    global $mysqli;
	$cell = "";
	$cell_data = "";
	foreach($data as $key => $val) {
	    if(trim(chop($key)) != "id" && trim(chop($val)) != "") {
		    $cell .= "`".sip($key)."`, ";
			$cell_data .= "'".sip($val)."', ";
		};
	}
	$cell = mb_substr($cell, 0, mb_strlen($cell)-2);
	$cell_data = mb_substr($cell_data, 0, mb_strlen($cell_data)-2);
	$mysqli->query("INSERT INTO `things`({$cell}) VALUES ({$cell_data})");
	echo '<script>document.location.href = "/?page=admin&section=things";</script>';
}

/*
 * Смена статуса заказа на "товар отправлен"
 * @param $id integer ID заказа
 */
function tradeCheck($id) {
    global $mysqli;
	$mysqli->query("UPDATE `cart` SET `checked`='1' WHERE `id`='{$id}'");
}

/*
 * Загрузка картинки товара
 * @param $file array Файл
 * @param $id integer ID товара
 */
function upload($file, $id) {
	global $mysqli;
	$dir = 'upload'; // Папка для картинок
	$filename = explode(".", $file["name"]); // Узнаём расширение файла
	$ext = $filename[1]; // Расширение
    $id == 0 ? $actual_image_name = time().'.'.$ext : $actual_image_name = $id.'.'.$ext; // Новое  имя полностью
	$tmp = $file['tmp_name']; // Временное имя картинки
	$ok = 0; // Пока не всё классно

	// Всё, что является картинкой
	$img = array('jpg', 'jpeg', 'JPG', 'JPEG', 'gif', 'GIF', 'tiff', 'TIF', 'png', 'PNG', 'bmp', 'BMP');
	
	// Проверим, картинку ли нам подсунули
	foreach($img as $key => $val) {
	    // Это картинка - значит, всё классно
		if($ext == $val) {
		    $ok = 1;
			break;
		};
	}
	
    // Если это всё-таки картинка
	if($ok == 1) {
	    // Если папка не существует - создаём её, притом с правами 777
        if (!is_dir($dir."/originals")) mkdir($dir."/originals", 0777);
	    if (!is_dir($dir."/thumbs")) mkdir($dir."/thumbs", 0777);
	    
	    //Загружаем файл в папку
        if(move_uploaded_file($tmp, $dir.'/originals/'.$actual_image_name)) {
	    	squarephoto($actual_image_name, $dir."/originals", $dir."/originals", 400);
			squarephoto($actual_image_name, $dir."/originals", $dir."/thumbs", 160);
			if($id == 0) {
			    echo '<script>
				          var img = parent.document.getElementById(\'new_thing_poster\');
						  img.src = "";
						  img.src = "/upload/thumbs/'.$actual_image_name.'";
						  parent.document.getElementById(\'new_thing_poster_input\').value = "'.$actual_image_name.'";
				      </script>';
			} else {
			    $res = $mysqli->query("SELECT `poster` AS poster FROM `things` WHERE `id`='{$id}'");
				while($old_poster = $res->fetch_object()->poster) {
				    unlink($dir."/originals/".$old_poster);
					unlink($dir."/thumbs/".$old_poster);
				}
				$res->close();
				$mysqli->query("UPDATE `things` SET `poster`='{$actual_image_name}' WHERE `id`='{$id}'");
			}
			echo '<script>
			          var img = parent.document.getElementById(\'thing_poster-'.$id.'\');
					  var img2 = parent.document.getElementById(\'new_thing_poster-'.$id.'\');
					  var img3 = parent.document.getElementById(\'big_poster_img-'.$id.'\');
					  img.src = "";
					  img2.src = "";
					  img3.src = "";
					  img.src = "/upload/thumbs/'.$actual_image_name.'";
					  img2.src = "/upload/thumbs/'.$actual_image_name.'";
					  img3.src = "/upload/originals/'.$actual_image_name.'";
			      </script>';
        };
	};
}

/*
 * Создаём квадратную миниатюру
 *
 * @param string $name Имя картинки
 * @param string $original папка исходного изображения
 * @param string $dir папка для сохранения
 * @param integer $to сторона квадрата
 */
function squarephoto($name, $original, $dir, $to) {
	$image_info = getimagesize($original."/".$name);
	switch($image_info[2]) {
	    case(IMAGETYPE_JPEG):
		                        $image = imagecreatefromjpeg($original."/".$name);
								break;
	    case(IMAGETYPE_GIF):
		                        $image = imagecreatefromgif($original."/".$name);
								break;
	    case(IMAGETYPE_PNG):
		                        $image = imagecreatefrompng($original."/".$name);
								break;
	}
	$w = imagesx($image);
	$h = imagesy($image);
    if($h > $w)
    {
        $im2 = imagecreatetruecolor($w, $w);
        imagecopyresampled($im2,$image,0,(-(($h - $w)/2)),0,0,$this->getWidth(),$h,$w,$h);
        $image = $im2;
    } else {
        $im2 = imagecreatetruecolor($h, $h);
        imagecopyresampled($im2,$image,(-(($w - $h)/2)),0,0,0,$w,$h,$w,$h);
        $image = $im2;
    }
	$w = imagesx($image);
	$h = imagesy($image);
    $ratio = $to / $w;
    $height = $h * $ratio;
    $new_image = imagecreatetruecolor($to, $height);
    imagecopyresampled($new_image, $image, 0, 0, 0, 0, $to, $height, $w, $h);
    $image = $new_image;
	$txt = date("d.m.Y H:i:s");
	imagestring($image, 5, 10, 180, $txt, imagecolorallocate($image, 255, 255, 255));
    imagejpeg($image,$dir."/".$name,75);
}

/*
 * Обрабатываем текст перед тем, как занести его в базу (защита от sql-инъекций)
 * @param $txt string Исходная строка
 * @return string $txt Обработанная строка
 */
function sip($txt) {
    global $mysqli; // Подключим нашу бд
    $txt = $mysqli->real_escape_string($txt); // Очищаем от "опасных" символов
    $txt = strip_tags($txt); // Убираем html-теги
    $txt = htmlspecialchars($txt); // Преобразуем строку в безопасный вид
    $txt = stripslashes($txt); // Убираем слэши
    $txt = addslashes($txt); // Экранируем кавычки
    return $txt;
}
?>