<?php
include_once 'templates/header.php';
if(file_exists('templates/'.$page.'.php')) {
    if(($page == "admin" && isset($_SESSION["user"]["rank"]) && $_SESSION["user"]["rank"] == "admin") || $page != "admin") {
		if(($page == "settings" || $page == "cart") && !isset($_SESSION["user"]["id"]) || $_SESSION["user"]["id"] < 0) {
		    echo '<script>document.location.href = "/";</script>';
		} else {
		    include_once 'templates/'.$page.'.php';
		}
	} else {
	    include_once 'templates/error.php';
	}
} else {
	include_once 'templates/error.php';
}
include_once 'templates/footer.php';
?>