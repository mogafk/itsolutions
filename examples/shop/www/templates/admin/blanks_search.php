<div class="admin_trade_header_search"><a href="#" OnClick="slideElem2('#in_cart2-<?php echo $key;?>');return false;">Продано за <?php echo $array_text[$key];?></a></div>
<div id="in_cart2-<?php echo $key;?>" class="in_cart<?php if($key > 0) echo ' hide_blank';?>">
    <table border="0" cellspacing="0" cellpadding="0">
        <thead>
	        <tr>
		        <th>ID</th>
				<th class="poster">Фото</th>
		  	    <th class="title">Название</th>
		  	    <th class="price">Продано</th>
		    </tr>
	    </thead>
	    <tbody>
		    <?php
				foreach($things as $item) {
					echo '<tr id="in_cart-'.$item["id"].'">
					               <td>'.$item["id"].'</td>
								   <td class="poster">
								       <a href="/?page=item&cat='.$item["cat"].'&id='.$item["id"].'" OnMouseOver="showElem(\'#big_poster-'.$key.'-'.$item["id"].'\');" OnMouseOut="hideElem(\'#big_poster-'.$key.'-'.$item["id"].'\');">
									       <img id="thing_poster-'.$item["id"].'" src="/upload/thumbs/'.$item["poster"].'" />
									   </a>
									   <div class="big_poster" id="big_poster-'.$key.'-'.$item["id"].'">
									       <img id="big_poster_img-'.$item["id"].'" src="/upload/originals/'.$item["poster"].'" />
									   </div>
								   </td>
					               <td class="title left">
								       <a href="/?page=item&cat='.$item["cat"].'&id='.$item["id"].'">'.$item["title"].'</a>
									   <br />
									   <span class="grey">'.$item["catname"].'</span>
								   </td>
							       <td class="count">'.$item["checked"].' шт.</td>
							   </tr>';
				}
			?>
		</tbody>
	</table>
</div>