<div class="settings_form">
    <form method="POST" style="float: none; clear: both;" action="/?page=admin&action=system&go=createcat">
        <input type="text" name="title" required placeholder="Название категории" style="display: inline-block;" />
    	<input type="submit" value="Добавить" style="display: inline-block; margin-left: 0;" />
    </form>
</div>
<?php
    $cats = getAll("cats", "ORDER BY `title`");
	$cnt = count(show_things(0));
	if($cnt > 0) {
	    echo '<div id="cart_string-0" class="cart_string"><div class="cart_string_link" style="background: #888888;">
	              <span class="adm_del bold" style="max-width: 200px; margin-right: 42px; cursor: text; opacity: 1.0 !important;">
	    		      '.$cnt.' товаров
	    		  </span>
	    		  <span class="bold" style="font-size: 14px; color: #FFFFFF;">Без категории</span>
	    	  </div></div>';
	}
	foreach($cats as $item) {
	    $cnt = count(show_things($item["id"]));
		echo '<div id="cart_string-'.$item["id"].'" class="cart_string">
		          <div class="cart_string_link">
				      <img class="adm_del" src="/templates/images/delete.png" OnClick="showElem(\'#adm_del_appr-'.$item["id"].'\');" />
					  <img class="adm_del" src="/templates/images/edit.png" OnClick="showElem(\'#adm_edit-'.$item["id"].'\');" />
					  <span class="adm_del bold" style="max-width: 200px; margin-right: 10px; cursor: text; opacity: 1.0 !important;">'.$cnt.' товаров</span>
					  <div id="adm_del_appr-'.$item["id"].'" class="adm_del_appr" style="margin-left: 400px;">
					      Вы действительно хотите удалить эту категорию?
					      <input type="button" value="Да" OnClick="delItem(\''.$item["id"].'\', \'cats\', \'#cart_string-'.$item["id"].'\');" />
					      <input type="button" value="Отмена" OnClick="hideElem(\'#adm_del_appr-'.$item["id"].'\');" />
					  </div>
					  <div id="adm_edit-'.$item["id"].'" class="adm_del_appr" style="margin-left: 400px;">
                        Название категории
						<form method="POST" style="float: none; clear: both;" action="/?page=admin&action=system&go=updatecat">
                            <input type="text" name="title" placeholder="Название категории" style="cursor: text; background: #FFFFFF; color: #AA0000; width: 90%;" value="'.$item["title"].'" />
					        <input type="hidden" name="id" value="'.$item["id"].'" />
                        	<input type="submit" value="Изменить" />
					        <input type="button" value="Отмена" OnClick="hideElem(\'#adm_edit-'.$item["id"].'\');" />
                        </form>
					  </div>
				      <a href="/?page=cat&id='.$item["id"].'"><span class="bold" style="font-size: 14px; color: #FFFFFF;">'.$item["title"].'</span></a>
				  </div>
			  </div>';
	}
?>