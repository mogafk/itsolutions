			<?php $cats_for_select = getAll("cats", "ORDER BY `title`");?>
			<form class="search" style="display: block; margin: 5px; float: none; clear: both;" method="POST" action="/">
			    <input id="adm_user_search_input" type="text" autocomplete="off" required style="width: 500px;" OnKeyUp="thingSearch(this.value);" placeholder="Название товара или его описание" />
				<input type="button" value="Новый товар" OnClick="showElem('#create_new_thing');return false;" />
			</form>
			<div id="create_new_thing" class="adm_del_appr adm_users_edit">
			    <form method="POST" action="/?page=admin&action=system&go=addNewThing" />
				    <input type="text" name="title" required placeholder="Название товара" />
					<select name="cat">
					    <option value="0" disabled selected>Выберите категорию</option>
						<?php
							foreach($cats_for_select as $cat) {
							    echo '<option value="'.$cat["id"].'">'.$cat["title"].'</option>';
							}
						?>
					</select>
					<textarea name="description" required placeholder="Описание товара"></textarea>
				    <span class="toolbar" style="margin-left: 154px;">Цена за 1 шт., руб</span><input type="number" name="price" required placeholder="Цена за 1 шт." value="0" />
				    <span class="toolbar" style="margin-left: 154px;">Кол-во на складе</span><input type="number" name="count" required placeholder="Количество" value="0" />
				    <input type="hidden" id="new_thing_poster_input" required name="poster" value="no-photo.png" />
					<img id="new_thing_poster" class="new_thing_poster" src="/templates/images/add-photo.png" OnClick="change_poster(0);return false;" />
					<input type="submit" value="Добавить" />
                    <input type="button" value="Отмена" OnClick="hideElem('#create_new_thing');" />
				</form>
                <form id="upload_poster-0" method="post" enctype="multipart/form-data" style="display: none;">
                    <input id="change_poster-0" type="file" name="file" accept="image/*,image/jpeg" OnChange="upload_poster('0');return false;" style="display: none;" />
                </form>
			</div>
			<div id="user_search_result" class="adm_things_wrapper">
			    <?php include_once 'templates/admin/things_tpl.php';?>
			</div>