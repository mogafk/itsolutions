<?php
$array_text = array("сегодня", "последние 7 дней", "последние 30 дней", "последний год");
$array[0] = $today = blanks(date('Y-m-d').' 00:00:00', date('Y-m-d').' 23:59:59');
$array[1] = $week = blanks(date('Y-m-d', strtotime('-7 days')).' 00:00:00', date('Y-m-d').' 23:59:59');
$array[2] = blanks(date('Y-m-d', strtotime('-30 days')).' 00:00:00', date('Y-m-d').' 23:59:59');
$array[3] = blanks(date('Y-m-d', strtotime('-365 days')).' 00:00:00', date('Y-m-d').' 23:59:59');
foreach($array as $key => $things) {
    include 'templates/admin/blanks_search.php';
}
?>