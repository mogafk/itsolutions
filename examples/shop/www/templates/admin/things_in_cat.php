<div class="in_cart">
    <table border="0" cellspacing="0" cellpadding="0">
        <thead>
	        <tr>
		        <th>ID</th>
				<th class="poster">Фото</th>
		  	    <th class="title">Название</th>
		  	    <th class="price">Цена за 1 шт.</th>
		  	    <th class="count">Кол-во</th>
		  	    <th class="delete"></th>
		    </tr>
	    </thead>
	    <tbody>
		    <?php
				$cats_for_select = getAll("cats", "ORDER BY `title`");
				foreach($things as $item) {
				    $select = '<select name="cat">
					               <option value="0" disabled>Выберите категорию</option>';
					foreach($cats_for_select as $cat) {
					    if($cat["id"] == $item["cat"]) {
						    $select .= '<option selected value="'.$cat["id"].'">'.$cat["title"].'</option>';
						} else {
						    $select .= '<option value="'.$cat["id"].'">'.$cat["title"].'</option>';
						}
					}
					$select .= '</select>';
					echo '<tr id="in_cart-'.$item["id"].'">
					               <td>'.$item["id"].'</td>
								   <td class="poster">
								       <a href="#" OnMouseOver="showElem(\'#big_poster-'.$item["id"].'\');" OnMouseOut="hideElem(\'#big_poster-'.$item["id"].'\');" OnClick="change_poster(\''.$item["id"].'\');return false;">
									       <img id="thing_poster-'.$item["id"].'" src="/upload/thumbs/'.$item["poster"].'" />
									   </a>
									   <div class="big_poster" id="big_poster-'.$item["id"].'">
									       <img id="big_poster_img-'.$item["id"].'" src="/upload/originals/'.$item["poster"].'" />
									   </div>
									   <form id="upload_poster-'.$item["id"].'" method="post" enctype="multipart/form-data" style="display: none;">
									       <input id="change_poster-'.$item["id"].'" type="file" name="file" accept="image/*,image/jpeg" OnChange="upload_poster(\''.$item["id"].'\');return false;" style="display: none;" />
									   </form>
								   </td>
					               <td class="title left">
								       <a href="/?page=item&cat='.$item["cat"].'&id='.$item["id"].'">'.$item["title"].'</a>
									   <br />
									   <span class="grey">'.$item["catname"].'</span>
									   <br />
									   <a href="#" OnClick="spoiler(this, \'#description_thing-'.$item["id"].'\', \'описание\');return false;">Показать описание</a>
									   <div class="hide_thing_description" id="description_thing-'.$item["id"].'">'.$item["description"].'</div>
								   </td>
							       <td class="price">'.$item["price"].' руб.</td>
							       <td class="count">'.$item["count"].' шт.</td>
							       <td class="delete">
								       <img class="adm_del" src="/templates/images/delete.png" OnClick="showElem(\'#adm_del_appr-'.$item["id"].'\');" />
									   <img class="adm_del" src="/templates/images/edit.png" OnClick="showElem(\'#adm_edit-'.$item["id"].'\');" />
                                          <div id="adm_del_appr-'.$item["id"].'" class="adm_del_appr" style="color: #FFFFFF;">
                                              Вы действительно хотите удалить этот товар?
                                              <input type="button" value="Да" OnClick="delItem(\''.$item["id"].'\', \'things\', \'#in_cart-'.$item["id"].'\');" />
                                              <input type="button" value="Отмена" OnClick="hideElem(\'#adm_del_appr-'.$item["id"].'\');" />
                                          </div>
                                          <div id="adm_edit-'.$item["id"].'" class="adm_del_appr adm_users_edit">
                                              Редактирование товара
											  <br /><span class="bold">'.$item["title"].'</span>
                                              <form id="user_settings_adm_form-'.$item["id"].'" method="POST" action="/">
												  <span class="toolbar">Название товара</span><input type="text" id="title-'.$item["id"].'" placeholder="Название товара" value="'.$item["title"].'" />
												  '.$select.'
												  <span class="toolbar">Описание товара</span><textarea id="description-'.$item["id"].'" placeholder="Описание товара">'.$item["description"].'</textarea>
												  <span class="toolbar" style="margin-left: 154px;">Цена за 1 шт., руб</span><input type="number" id="price-'.$item["id"].'" placeholder="Цена за 1 шт." value="'.$item["price"].'" />
												  <span class="toolbar" style="margin-left: 154px;">Кол-во на складе</span><input type="number" id="count-'.$item["id"].'" placeholder="Количество" value="'.$item["count"].'" />
												  <img id="new_thing_poster-'.$item["id"].'" class="new_thing_poster" src="/upload/thumbs/'.$item["poster"].'" OnClick="change_poster(\''.$item["id"].'\');return false;" />
												  <input type="button" value="Применить" OnClick="updateThing(\''.$item["id"].'\');" />
                                                  <input type="button" value="Отмена" OnClick="hideElem(\'#adm_edit-'.$item["id"].'\');" />
											  </form>
                                          </div>
								   </td>
							   </tr>';
				}
			?>
		</tbody>
	</table>
</div>