		    <div class="trade_sections">
			    <?php
				    if(isset($_GET["uid"]) && trim(chop($_GET["uid"])) != "") {
					    $user = InfoById(sip($_GET["uid"]), "users");
						echo '<div class="admin_trade_header_search">Заказы с аккаунта <span class="bold">'.$user["login"].'</span><img src="/templates/images/delete.png" OnClick="clearTrade();" /></div>';
					};
					if(isset($_COOKIE["openTradeSection"]) && trim(chop($_COOKIE["openTradeSection"])) != "") {
					    if($_COOKIE["openTradeSection"] == "new") {
				        echo '<input id="open_button_new" type="button" value="Новые" class="active" OnClick="openTradeSection(\'new\', \'checked\');" />
			                  <input id="open_button_checked" type="button" value="Выполненные" OnClick="openTradeSection(\'checked\', \'new\');" />';
						} else {
				        echo '<input id="open_button_new" type="button" value="Новые" OnClick="openTradeSection(\'new\', \'checked\');" />
			                  <input id="open_button_checked" type="button" value="Выполненные" class="active" OnClick="openTradeSection(\'checked\', \'new\');" />';
						}
					} else {
				        echo '<input id="open_button_new" type="button" value="Новые" class="active" OnClick="openTradeSection(\'new\', \'checked\');" />
			                  <input id="open_button_checked" type="button" value="Выполненные" OnClick="openTradeSection(\'checked\', \'new\');" />';
					}
				?>
			</div>
			<?php
			$prefix = "new";
			(isset($_COOKIE["openTradeSection"]) && trim(chop($_COOKIE["openTradeSection"])) != "") ? $open = $_COOKIE["openTradeSection"] : $open = "new";
			for($i = 0; $i < 2; $i++) {
			    $open == $prefix ? $style = 'style="display: block;"' : $style = '';
				echo '<div id="trade_section_'.$prefix.'" class="trade_section_content" '.$style.'>';
				(isset($_GET["p_".$prefix]) && (int)sip($_GET["p_".$prefix]) > 0) ? $p = sip($_GET["p_".$prefix]) : $p = 1;
			    (isset($_GET["uid"]) && trim(chop($_GET["uid"])) != "") ? $cart = userCart(sip($_GET["uid"]), $i, $p) : $cart = userCart(0, $i, $p);
			    (isset($_GET["uid"]) && trim(chop($_GET["uid"])) != "") ? $uid = "&uid=".sip($_GET["uid"]) : $uid = "";
				if(count($cart) > 0) {
                    $pages = userCartPages(0, $i, $p);
                    $pages_nav = '<div class="pages_nav">';
                    foreach($pages as $item) {
                        (int)$p == (int)$item ? $class = 'class="active"' : $class = '';
			    		$pages_nav .= '<a '.$class.' href="/?page='.sip($_GET["page"]).'&section='.sip($_GET["section"]).'&p_'.$prefix.'='.$item.$uid.'">'.$item.'</a>';
                    }
                    $pages_nav .= '</div>';
                    echo $pages_nav;
			    	foreach($cart as $key2 => $val2) {
			    	    $cart_ids = explode("-", $val2["things"]);
			    	    $cart_cnt = explode("-", $val2["count_per_one"]);
			    		$cart_prices = explode("-", $val2["prices_per_one"]);
						$prefix == "new" ? $trade_time = 'Заказ сделан: '.$val2["whenadd"] : $trade_time = 'Товар отправлен: '.$val2["whenchecked"];
                        $prefix == "new" ? $trade_checked = '<input type="button" value="Заказ выполнен" OnClick="tradeCheck(\''.$val2["id"].'\');" />' : $trade_checked = '';
						$block = '<div id="cart_string-'.$val2["id"].'" class="cart_string">
			    				  <a href="#" OnClick="slideElem(\'#in_cart-'.$val2["id"].'\');return false;">
			    				      <div class="cart_string_link">
			    					      Заказ #'.$val2["id"].'
			    				          <span style="float: right;">'.$trade_time.'</span>
			    					  </div>
			    				  </a>
			    		          <div id="in_cart-'.$val2["id"].'" class="in_cart">
                                      <table border="0" cellspacing="0" cellpadding="0">
                        			      <thead>
                        				      <tr>
                        					      <th class="poster">Фото</th>
                        						  <th class="title">Название</th>
                        						  <th class="price">Цена за 1 шт.</th>
                        						  <th class="count">Количество</th>
                        						  <th class="summary">Сумма</th>
                        					  </tr>
                        				  </thead>
                        				  <tbody>';
			    	    foreach($cart_ids as $key => $val) {
			    	        $item = InfoById($val, "things");
			    	    	$block .= '<tr id="in_cart-'.$item["id"].'">
			    	    	               <td class="poster"><a href="/?page=item&cat='.$item["cat"].'&id='.$item["id"].'"><img src="/upload/thumbs/'.$item["poster"].'" /></a></td>
			    	    	               <td class="title left"><a href="/?page=item&cat='.$item["cat"].'&id='.$item["id"].'">'.$item["title"].'</a></td>
			    	    			       <td class="price">'.$cart_prices[$key].' руб.</td>
			    	    			       <td class="count">'.$cart_cnt[$key].' шт.</td>
			    	    			       <td class="summary">'.(int)$cart_prices[$key]*(int)$cart_cnt[$key].' руб.</td>
			    	    			   </tr>';
			    	    }
			    	    $block .= '</tbody></table>
						           <span class="grey">
								       Всего товаров: '.$val2["count"].'<br />
									   На сумму: '.$val2["summary"].' руб.<br />
									   Получатель: '.$val2["fio"].'<br />
									   Адрес получателя: '.$val2["adress"].'<br />
									   Индекс почтового отделения: '.$val2["post_index"].'<br />
									   Телефон получателя: '.$val2["phone"].'
								   </span>
								       <div class="trade_sections" style="margin: 5px auto;">
								           '.$trade_checked.'
										   <input type="button" value="Удалить заказ" OnClick="showElem(\'#del_trade-'.$val2["id"].'\');" />
								       </div>
                                       <div id="del_trade-'.$val2["id"].'" class="adm_del_appr" style="color: #FFFFFF; margin-left: 0px;">
                                           Вы действительно хотите удалить этот заказ?
                                           <input type="button" value="Да" OnClick="delItem(\''.$val2["id"].'\', \'cart\', \'#in_cart-'.$val2["id"].'\');" />
                                           <input type="button" value="Отмена" OnClick="hideElem(\'#del_trade-'.$val2["id"].'\');" />
                                       </div>
								   </div></div>';
			    	    echo $block;
                    }
			    	echo $pages_nav;
			    } else {
			        echo '<div class="in_cart"><span class="grey">Не найдено ни одного заказа.</span></div>';
			    }
				echo '</div>';
				$prefix = "checked";
			}
			?>