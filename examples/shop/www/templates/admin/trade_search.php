			<?php
			    if(count($cart) > 0) {
			    	foreach($cart as $key2 => $val2) {
			    	    if((int)$key2 == 0 && (int)$val2["checked"] == 0) echo '<div class="admin_trade_header_search">Новые</div>';
						if((!isset($cart[$key2-1]) && (int)$val2["checked"] == 1) || ((int)$cart[$key2-1]["checked"] == 0 && (int)$val2["checked"] == 1)) echo '<div class="admin_trade_header_search">Выполненные</div>';
						(int)$val2["checked"] == 0 ? $prefix = "new" : $prefix = "checked";
						$cart_ids = explode("-", $val2["things"]);
			    	    $cart_cnt = explode("-", $val2["count_per_one"]);
			    		$cart_prices = explode("-", $val2["prices_per_one"]);
						$prefix == "new" ? $trade_time = 'Заказ сделан: '.$val2["whenadd"] : $trade_time = 'Товар отправлен: '.$val2["whenchecked"];
                        $prefix == "new" ? $trade_checked = '<input type="button" value="Заказ выполнен" OnClick="tradeCheck(\''.$val2["id"].'\');" />' : $trade_checked = '';
						$block = '<div id="cart_string-'.$val2["id"].'" class="cart_string">
			    				  <a href="#" OnClick="slideElem(\'#in_cart-'.$val2["id"].'\');return false;">
			    				      <div class="cart_string_link">
			    					      Заказ #'.$val2["id"].'
			    				          <span style="float: right;">'.$trade_time.'</span>
			    					  </div>
			    				  </a>
			    		          <div id="in_cart-'.$val2["id"].'" class="in_cart">
                                      <table border="0" cellspacing="0" cellpadding="0">
                        			      <thead>
                        				      <tr>
                        					      <th class="poster">Фото</th>
                        						  <th class="title">Название</th>
                        						  <th class="price">Цена за 1 шт.</th>
                        						  <th class="count">Количество</th>
                        						  <th class="summary">Сумма</th>
                        					  </tr>
                        				  </thead>
                        				  <tbody>';
			    	    foreach($cart_ids as $key => $val) {
			    	        $item = InfoById($val, "things");
			    	    	$block .= '<tr id="in_cart-'.$item["id"].'">
			    	    	               <td class="poster"><a href="/?page=item&cat='.$item["cat"].'&id='.$item["id"].'"><img src="/upload/thumbs/'.$item["poster"].'" /></a></td>
			    	    	               <td class="title left"><a href="/?page=item&cat='.$item["cat"].'&id='.$item["id"].'">'.$item["title"].'</a></td>
			    	    			       <td class="price">'.$cart_prices[$key].' руб.</td>
			    	    			       <td class="count">'.$cart_cnt[$key].' шт.</td>
			    	    			       <td class="summary">'.(int)$cart_prices[$key]*(int)$cart_cnt[$key].' руб.</td>
			    	    			   </tr>';
			    	    }
			    	    $block .= '</tbody></table>
						           <span class="grey">
								       Всего товаров: '.$val2["count"].'<br />
									   На сумму: '.$val2["summary"].' руб.<br />
									   Получатель: '.$val2["fio"].'<br />
									   Адрес получателя: '.$val2["adress"].'<br />
									   Индекс почтового отделения: '.$val2["post_index"].'<br />
									   Телефон получателя: '.$val2["phone"].'
								   </span>
								       <div class="trade_sections" style="margin: 5px auto;">
								           '.$trade_checked.'
										   <input type="button" value="Удалить заказ" OnClick="showElem(\'#del_trade-'.$val2["id"].'\');" />
								       </div>
                                       <div id="del_trade-'.$val2["id"].'" class="adm_del_appr" style="color: #FFFFFF; margin-left: 0px;">
                                           Вы действительно хотите удалить этот заказ?
                                           <input type="button" value="Да" OnClick="delItem(\''.$val2["id"].'\', \'cart\', \'#in_cart-'.$val2["id"].'\');" />
                                           <input type="button" value="Отмена" OnClick="hideElem(\'#del_trade-'.$val2["id"].'\');" />
                                       </div>
								   </div></div>';
			    	    echo $block;
                    }
			    } else {
			        echo '<div class="in_cart"><span class="grey">Не найдено ни одного заказа.</span></div>';
			    }
			?>