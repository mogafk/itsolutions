				<div class="user_list">
			    	<div class="grey" style="margin: 5px auto;">
					    Результаты поиска по запросу <span class="grey bold">"<?php echo $query;?>"</span>:
					</div>
					<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
			            <thead>
			        	    <tr>
			        		    <th>ID</th>
			    				<th>E-mail(логин)</th>
			        			<th>Ф.И.О.</th>
			        			<th>Адрес</th>
			        			<th>Индекс</th>
			        			<th>Телефон</th>
			        			<th>Заказов</th>
			        			<th style="width: 32px;"></th>
			        		</tr>
			        	</thead>
			        	<tbody id="users_content">
			        <?php
			        	foreach($users as $item) {
			    		    $res = $mysqli->query("SELECT * FROM `cart` WHERE `user`='{$item["id"]}'");
			    			$cnt = $res->num_rows;
			    			$res->close();
			    			$item["id"] == $_SESSION["user"]["id"] ? $delete = '' : $delete = '<img class="adm_del" src="/templates/images/delete.png" OnClick="showElem(\'#adm_del_appr-'.$item["id"].'\');" />';
			    			echo '<tr id="cart_string-'.$item["id"].'" class="userrow">
			    			          <td>'.$item["id"].'</td>
			    					  <td>'.$item["login"].'</td>
			    					  <td>'.$item["fio"].'</td>
			    					  <td>'.$item["adress"].'</td>
			    					  <td>'.$item["post_index"].'</td>
			    					  <td>'.$item["phone"].'</td>
			    					  <td><a href="/?page=admin&section=trade&uid='.$item["id"].'" style="font-size: 10px;">'.$cnt.'</a></td>
			    					  <td>
			    					      '.$delete.'
			    						  <img class="adm_del" src="/templates/images/edit.png" OnClick="showElem(\'#adm_edit-'.$item["id"].'\');" />
                                          <div id="adm_del_appr-'.$item["id"].'" class="adm_del_appr" style="color: #FFFFFF;">
                                              Вы действительно хотите удалить этого пользователя?
                                              <input type="button" value="Да" OnClick="delItem(\''.$item["id"].'\', \'users\', \'#cart_string-'.$item["id"].'\');" />
                                              <input type="button" value="Отмена" OnClick="hideElem(\'#adm_del_appr-'.$item["id"].'\');" />
                                          </div>
                                          <div id="adm_edit-'.$item["id"].'" class="adm_del_appr adm_users_edit">
                                              Редактирование аккаунта
											  <br /><span class="bold">'.$item["login"].'</span>
                                              <form id="user_settings_adm_form-'.$item["id"].'" method="POST" action="/">
											      <input type="email" id="login-'.$item["login"].'" disabled placeholder="E-mail" value="'.$item["login"].'" />
												  <input type="text" id="fio-'.$item["id"].'" placeholder="Ф.И.О." value="'.$item["fio"].'" />
												  <input type="text" id="adress-'.$item["id"].'" placeholder="Адрес" value="'.$item["adress"].'" />
												  <input type="text" id="post_index-'.$item["id"].'" placeholder="Индекс" value="'.$item["post_index"].'" />
												  <input type="phone" id="phone-'.$item["id"].'" placeholder="Телефон" value="'.$item["phone"].'" />
												  <input type="password" id="password-'.$item["id"].'" placeholder="Новый пароль" />
												  <input type="button" value="Применить" OnClick="updateUser(\''.$item["id"].'\');" />
                                                  <input type="button" value="Отмена" OnClick="hideElem(\'#adm_edit-'.$item["id"].'\');" />
											  </form>
                                          </div>
			    					  </td>
			    			      </tr>';
			    		}
			        ?>
			            </tbody>
			        </table>
			    </div>