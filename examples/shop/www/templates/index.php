<?php
$things = show_things();
echo '
<H1>Все товары <span class="grey">('.count($things).')</span></H1>
<br />
';

if(count($things) > 0) {
    if(isset($_SESSION["user"]["id"]) && $_SESSION["user"]["id"] > 0) {
	    $ok = 1;
	};
	foreach($things as $key => $val) {
        $ok == 1 ? $zakaz = '<input type="button" value="В корзину" OnClick="showElem(\'#tocart-'.$val["id"].'\');" />' : $zakaz = '';
		if($zakaz != '' && $val["count"] < 1) $zakaz = '<input type="button" disabled value="Нет в наличии" style="color: #AA0000; background: #FFFFFF; cursor: text;" />';
		echo '
        <div class="thing">
            <H3>'.$val["title"].'</H3>
        	<a href="/?page=cat&id='.$val["cat"].'">'.$val["catname"].'</a>
        	<br />
        	<img src="/upload/thumbs/'.$val["poster"].'" />
			<div class="price">'.$val["price"].' руб.</div>
        	<input type="button" value="Описание" OnClick="document.location.href=\'/?page=item&cat='.$val["cat"].'&id='.$val["id"].'\'" />
			'.$zakaz.'
			<div id="tocart-'.$val["id"].'" class="range">
			    Количество
				<input id="range-'.$val["id"].'" type="range" min="1" max="'.$val["count"].'" step="1" value="1" OnChange="updateTextInput(this.value, '.$val["id"].');" style="width: 150px;" />
				<input type="number" min="1" max="'.$val["count"].'" id="rangetext-'.$val["id"].'" value="1" OnChange="updateRangeInput(this.value, '.$val["id"].');" style="width: 50px; margin: 5px auto; background: #FFFFFF; color: #AA0000; border: 1px solid #880000; text-align: center; cursor: text;" />
				<input type="button" value="Добавить" OnClick="addToCart(\''.$val["id"].'\');" />
				<input type="button" value="Отмена" OnClick="hideElem(\'#tocart-'.$val["id"].'\');" />
			</div>
        </div>
        ';
    }
} else {
    echo '<span class="grey">Не найдено ни одного товара.</span>';
}
?>