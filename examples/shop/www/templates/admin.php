		    <H1>Администрирование</H1>
			<div id="admin_nav">
			    <ul>
			        <?php
					    foreach($admin_sections as $key => $val) {
						$key == $admin_section ? $class = 'class="now"' : $class = '';
						?>
                            <a href="/?page=admin&section=<?php echo $key;?>">
                                <li <?php echo $class;?> >
                                    <?php echo $val;?>
                                </li>
                            </a>
						<?php
						}
					?>
			    </ul>
			</div>
			<div id="admin_content">
			    <?php include_once 'templates/admin/'.$admin_section.'.php';?>
			</div>