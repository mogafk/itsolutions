		    <H1>Настройки аккаунта <?php echo $_SESSION["user"]["login"];?></H1>
			<div class="settings_form">
			    <form method="POST" action="/?action=settings2&page=<?php echo $page;?>">
			    	<input type="text" name="fio" required placeholder="Ф.И.О." value="<?php echo $_SESSION["user"]["fio"];?>" />
			    	<textarea name="adress" required placeholder="Адрес"><?php echo $_SESSION["user"]["adress"];?></textarea>
			    	<input type="text" name="post_index" required placeholder="Почтовый индекс" value="<?php echo $_SESSION["user"]["post_index"];?>" />
			    	<input type="phone" name="phone" required placeholder="Телефон" value="<?php echo $_SESSION["user"]["phone"];?>" />
			    	<input type="password" name="oldpass" required pattern="[A-Za-z0-9]{8,16}" placeholder="Пароль для подтверждения" />
			    	<input type="submit" value="Применить" />
			    </form>
			    <form method="POST" action="/?action=settings&page=<?php echo $page;?>">
			    	<input type="password" name="newpass" required pattern="[A-Za-z0-9]{8,16}" placeholder="Новый пароль" />
			    	<input type="password" name="oldpass" required pattern="[A-Za-z0-9]{8,16}" placeholder="Старый пароль" />
			    	<input type="submit" value="Изменить пароль" />
			    </form>
			</div>