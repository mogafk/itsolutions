<!doctype html>
<html>
    <head>
	    <meta charset="utf-8" />
		<link rel="icon" type="image/png" href="/templates/images/icon.png" />
		<link rel="stylesheet" type="text/css" href="templates/css/style.css" />
		<script src="/js/scriptjava.js"></script>
		<script src="/js/jquery-1.10.2.min.js"></script>
		<script src="/js/functions.js"></script>
		<title><?php echo SITENAME;?> | <?php echo $sections[$page];?></title>
	</head>
    <body>
		<div id="header">
		    <a href="/">
			    <img src="/templates/images/logo.png" />
			</a>
		</div>
		<div id="all">
		    <div id="breadcrumbs">
				<form class="search" method="GET" action="/">
					<input type="hidden" name="page" value="search" />
				    <?php
					    (isset($_GET["query"]) && trim(chop(sip($_GET["query"]))) != "") ? $input = '<input type="text" name="query" required value="'.sip($_GET["query"]).'" placeholder="Что-то ищем? :)" />' : $input = '<input type="text" name="query" required placeholder="Что-то ищем? :)" />';
						echo $input;
					?>
					<input type="submit" value="Найти" />
				</form>
				<?php
				    if(isset($page) && $page != "index") {
						echo '<a href="/">Главная</a>';
						$page == "item" ? $link = '<img src="/templates/images/right.png" /> <a href="/?page=cat&id='.$item["cat"].'">'.$cat["title"].'</a> <img src="/templates/images/right.png" /> <a class="now" href="/?page=item&cat='.$item["cat"].'&id='.$_GET["id"].'">'.$item["title"].'</a>' : $link = ' <img src="/templates/images/right.png" /> <a class="now" href="/?page='.$page.'">'.$sections[$page].'</a>';
						if($page == "cat") $link = ' <img src="/templates/images/right.png" /> <a class="now" href="/?page='.$page.'&id='.$_GET["id"].'">'.$cat["title"].'</a>';
						echo $link;
					} else {
					    echo '<a class="now" href="/">Главная</a>';
					}
				?>
			</div>
			<table border="0" class="all">
			    <tbody>
				    <tr>
					    <td class="content"> 
		                    <div id="content">