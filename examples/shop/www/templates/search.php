			<form class="search" style="display: block; margin: 5px;" method="GET" action="/">
				<input type="hidden" name="page" value="search" />
			    <?php
				    (isset($_GET["query"]) && trim(chop(sip($_GET["query"]))) != "") ? $input = '<input type="text" name="query" required style="width: 600px;" value="'.sip($_GET["query"]).'" placeholder="Например: Лисопед" />' : $input = '<input type="text" name="query" required style="width: 600px;" placeholder="Например: Лисопед" />';
					echo $input;
				?>
				<input type="submit" value="Найти" />
			</form>
			<H1>Результаты поиска</H1>
			<?php
			    if(isset($_GET["query"]) && trim(chop($_GET["query"])) != "") {
				    if(count($search) > 0) {
				        echo '<span class="grey">По запросу "'.sip($_GET["query"]).'" найдено '.count($search).' совпадений:</span>';
				    } else {
				        echo '<span class="grey">По запросу "'.sip($_GET["query"]).'" совпадений не найдено.<br />Возможно, вы указали меньше '.SEARCH_MIN_LENGHT.' символов в строке.</span>';
				    }
			        echo '<br /><br />';
			        foreach($search as $item) {
			            $block = '<div class="search_result">';
			        	count($item) == 3 ? 
			        	$block .= '<a href="/?page=cat&id='.$item["id"].'">Категория <span class="bold">"'.$item["title"].'"</a>' : $block .= '<a href="/?page=item&cat='.$item["cat"].'&id='.$item["id"].'">Товар <span class="bold">"'.$item["title"].'"</a><br />';
			        	$block .= '</div>';
			        	echo $block;
			        }
				} else {
				    echo '<span class="grey">Введите не менее '.SEARCH_MIN_LENGHT.' символов в поисковой строке, чтобы начать поиск.<br /></span>';
				}
			?>