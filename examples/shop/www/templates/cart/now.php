		    <?php
			if(isset($_COOKIE["cart_ids"]) && trim(chop($_COOKIE["cart_ids"])) != "") {
			    $cart_ids = explode("-", $_COOKIE["cart_ids"]);
				$cart_cnt = explode("-", $_COOKIE["cart_cnt"]);
				$summary = 0;
				$count = 0;
                $block = '<div class="in_cart">
                              <table border="0" cellspacing="0" cellpadding="0">
                			      <thead>
                				      <tr>
                					      <th class="poster">Фото</th>
                						  <th class="title">Название</th>
                						  <th class="price">Цена за 1 шт.</th>
                						  <th class="count">Количество</th>
                						  <th class="summary">Сумма</th>
                						  <th class="delete"></th>
                					  </tr>
                				  </thead>
                				  <tbody>';
				foreach($cart_ids as $key => $val) {
				    $item = InfoById($val, "things");
					$block .= '<tr id="in_cart-'.$item["id"].'">
					               <td class="poster"><a href="/?page=item&cat='.$item["cat"].'&id='.$item["id"].'"><img src="/upload/thumbs/'.$item["poster"].'" /></a></td>
					               <td class="title left"><a href="/?page=item&cat='.$item["cat"].'&id='.$item["id"].'">'.$item["title"].'</a></td>
							       <td class="price">'.$item["price"].' руб.</td>
							       <td class="count">'.$cart_cnt[$key].' шт.</td>
							       <td class="summary">'.(int)$item["price"]*(int)$cart_cnt[$key].' руб.</td>
							       <td class="delete"><img src="/templates/images/delete.png" OnClick="delFromCart(\''.$cart_cnt[$key].'\', \''.$item["id"].'\');" /></td>
							   </tr>';
					$summary += (int)$item["price"]*(int)$cart_cnt[$key];
					$count += (int)$cart_cnt[$key];
				}
				$block .= '</tbody></table></div>';
				echo $block;
                echo '<div class="in_cart"><span class="grey">Товаров в корзине: '.$count.'<br />Итоговая сумма: '.$summary.' руб.</span><br />';
				if(trim(chop($_SESSION["user"]["adress"])) != "" && trim(chop($_SESSION["user"]["post_index"])) != "" && trim(chop($_SESSION["user"]["fio"])) != "" && trim(chop($_SESSION["user"]["phone"])) != "") {
				    echo '<input class="gocart" type="button" value="Отправить заказ" OnClick="cartGo();" /></div>';
				} else {
				    echo '<span class="grey">Чтобы завершить заказ, укажите ваши контактные данные в <a href="/?page=settings">настройках вашего аккаунта</a>.</span>';
				}
			} else {
			    echo '<div class="in_cart"><span class="grey">Сейчас в корзине нет товаров.</span></div>';
			}
			?>