		    <?php
			(isset($_GET["p"]) && (int)sip($_GET["p"]) > 0) ? $p = sip($_GET["p"]) : $p = 1;
			$cart = userCart($_SESSION["user"]["id"], 1, $p);
			if(count($cart) > 0) {
                $pages = userCartPages($_SESSION["user"]["id"], 1, $p);
                $pages_nav = '<div class="pages_nav">';
                foreach($pages as $item) {
                    (int)$p == (int)$item ? $class = 'class="active"' : $class = '';
					$pages_nav .= '<a '.$class.' href="/?page='.sip($_GET["page"]).'&section='.sip($_GET["section"]).'&p='.$item.'">'.$item.'</a>';
                }
                $pages_nav .= '</div>';
                echo $pages_nav;
			    foreach($cart as $key2 => $val2) {
				    $cart_ids = explode("-", $val2["things"]);
				    $cart_cnt = explode("-", $val2["count_per_one"]);
					$cart_prices = explode("-", $val2["prices_per_one"]);
				    $summary = 0;
				    $count = 0;
                    $block = '<div id="cart_string-'.$val2["id"].'" class="cart_string">
							  <a href="#" OnClick="slideElem(\'#in_cart-'.$val2["id"].'\');return false;">
							      <div class="cart_string_link">
								      Заказ #'.$val2["id"].'
							          <span style="float: right;">Товар отправлен: '.$val2["whenchecked"].'</span>
								  </div>
							  </a>
					          <div id="in_cart-'.$val2["id"].'" class="in_cart">
                                  <table border="0" cellspacing="0" cellpadding="0">
                    			      <thead>
                    				      <tr>
                    					      <th class="poster">Фото</th>
                    						  <th class="title">Название</th>
                    						  <th class="price">Цена за 1 шт.</th>
                    						  <th class="count">Количество</th>
                    						  <th class="summary">Сумма</th>
                    					  </tr>
                    				  </thead>
                    				  <tbody>';
				    foreach($cart_ids as $key => $val) {
				        $item = InfoById($val, "things");
				    	$block .= '<tr id="in_cart-'.$item["id"].'">
				    	               <td class="poster"><a href="/?page=item&cat='.$item["cat"].'&id='.$item["id"].'"><img src="/upload/thumbs/'.$item["poster"].'" /></a></td>
				    	               <td class="title left"><a href="/?page=item&cat='.$item["cat"].'&id='.$item["id"].'">'.$item["title"].'</a></td>
				    			       <td class="price">'.$cart_prices[$key].' руб.</td>
				    			       <td class="count">'.$cart_cnt[$key].' шт.</td>
				    			       <td class="summary">'.(int)$cart_prices[$key]*(int)$cart_cnt[$key].' руб.</td>
				    			   </tr>';
				    }
				    $block .= '</tbody></table><span class="grey">Всего товаров: '.$val2["count"].'<br />На сумму: '.$val2["summary"].' руб.<br />Получатель: '.$val2["fio"].'<br />Адрес получателя: '.$val2["adress"].'<br />Индекс почтового отделения: '.$val2["post_index"].'<br />Телефон получателя: '.$val2["phone"].'</span></div></div>';
				    echo $block;
                }
				echo $pages_nav;
			} else {
			    echo '<div class="in_cart"><span class="grey">Вы ещё ничего не заказывали, либо товар по вашим заказам ещё не был отправлен.</span></div>';
			}
			?>