		                    </div>
		                </td>
		                <td class="menu">
		                    <div id="right">
								<?php
								    if(isset($_SESSION["user"]["id"]) && (int)$_SESSION["user"]["id"] > 0) {
									    (isset($_COOKIE["cart_ids"]) && trim(chop($_COOKIE["cart_ids"])) != "") ? $display = 'style="display: block;"' : $display = '';
								?>
								<H3>
								    <?php echo $_SESSION["user"]["login"];?>
									<img id="cart_icon" src="/templates/images/cart.png" <?php echo $display;?> OnClick="document.location.href = '/?page=cart&section=now';" />
								</H3>
								<ul>
								    <?php
									    if(isset($_SESSION["user"]["rank"]) && $_SESSION["user"]["rank"] == "admin") {
									?>
									<li>
									    <a id="admin" href="/?page=admin">Администрирование</a>
									</li>
									<?php
									    };
									?>
									<li>
									    <a id="cart" href="/?page=cart">Мои заказы</a>
									</li>
								    <li>
									    <a id="settings" href="/?page=settings">Настройки</a>
									</li>
								    <li>
									    <a href="/?action=exit&page=<?php echo $page;?>">Выход</a>
									</li>
								</ul>
								<?php
									} else {
								?>
								<H3>Вход</H3>
                                <div class="auth_form">
                                	<form method="POST" action="/?action=auth&page=<?php echo $page;?>">
                                	    <input type="email" name="login" required placeholder="E-mail" />
                                		<input type="password" name="pass" required pattern="[A-Za-z0-9]{8,16}" placeholder="Пароль" />
                                		<input type="submit" value="Войти" />
                                	</form>
                                </div>
								<?php
									}
								?>
								<H3>Навигация</H3>
								<ul>
								    <li>
									    <a id="home" href="/">Главная</a>
									</li>
								    <li>
									    <a id="search" href="/?page=search">Поиск</a>
									</li>
								    <li>
									    <a id="confidential" href="/?page=confidential">Политика приватности</a>
								    <li>
									    <a id="contacts" href="/?page=contacts">Контактная информация</a>
									</li>
								    <li>
									    <a id="help" href="/?page=help">Помощь</a>
									</li>
								</ul>
								<H3>Категории</H3>
								<ul>
								    <?php
									    $cats = getAll("cats", "ORDER BY `title`");
										foreach($cats as $item) {
										    echo '
                                                <li>
                                                    <a id="cat-'.$item["id"].'" href="/?page=cat&id='.$item["id"].'">'.$item["title"].'</a>
                                                </li>
											';
										}
									?>
								</ul>
		                	</div>
		                </td>
		            </tr>
		        </tbody>
		    </table>
		</div>
		<div id="footer">
		    <ul>
			    <li style="text-align: left;">
				    <a href="/?page=confidential">Политика конфиденциальности</a>
				</li>
				<li>
			        &copy; <a href="/"><?php echo SITENAME;?></a>, <?php echo date("Y");?>
				</li>
				<li style="text-align: right;">
				    <a href="/?page=contacts">Контактная информация</a>
				</li>
			</ul>
		</div>
	</body>
</html>