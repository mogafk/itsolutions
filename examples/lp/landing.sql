--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `whenadd` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `whenadd` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

--
-- Дамп данных таблицы `reviews`
--

INSERT INTO `reviews` (`id`, `name`, `photo`, `whenadd`, `description`) VALUES
(59, 'Валерия', '1423664290.png', '2015-01-25', 'Текст отзыва о продукции компании Jeunesse Текст отзыва о продукции компании Jeunesse'),
(60, 'Валерия', '1423664325.png', '2015-01-25', 'Текст отзыва о продукции компании Jeunesse Текст отзыва о продукции компании Jeunesse'),
(61, 'Валерия', '1423664345.png', '2015-01-25', 'Текст отзыва о продукции компании Jeunesse Текст отзыва о продукции компании Jeunesse Текст отзыва о продукции компании Jeunesse Текст отзыва о продукции компании Jeunesse');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `title` varchar(255) NOT NULL,
  `option_alias` varchar(255) NOT NULL,
  `value` longtext NOT NULL,
  `template` varchar(255) NOT NULL,
  UNIQUE KEY `option_alias` (`option_alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`title`, `option_alias`, `value`, `template`) VALUES
('Логин администратора', 'admin_login', 'admin', 'input'),
('Пароль администратора', 'admin_password', '32ff9ee7e841b26a966870c144fdcaec', 'input'),
('Ссылка на страницу компании', 'header_menu_1', '#', 'input'),
('Ссылка на страницу каталога', 'header_menu_2', '#', 'input'),
('Ссылка на страницу контактов', 'header_menu_3', '#', 'input'),
('Заголовок', 'header_text', 'Косметика класса люкс от Jeunesse с доставкой по всей России', 'textarea'),
('Телефон', 'phone_number', '8-800-888-88-88', 'input'),
('Название сайта', 'sitename', 'Landing Page', 'input'),
('Группа ВКонтакте', 'social_avk', '#', 'input'),
('Страница на facebook', 'social_fb', '#', 'input'),
('twitter', 'social_tw', '#', 'input'),
('Почему покупают №1', 'why_buy_1', 'Почему девушки покупают косметику Jeunesse', 'textarea'),
('Почему покупают №2', 'why_buy_2', 'Почему девушки покупают косметику Jeunesse', 'textarea'),
('Почему покупают №3', 'why_buy_3', 'Почему девушки покупают косметику Jeunesse', 'textarea'),
('ID видео на youtube', 'youtube_video', '4RuQ4WdoT7U', 'input');

-- --------------------------------------------------------

--
-- Структура таблицы `trade`
--

CREATE TABLE IF NOT EXISTS `trade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `whenadd` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `trade`
--

INSERT INTO `trade` (`id`, `name`, `photo`, `whenadd`, `description`) VALUES
(2, 'Омолаживающая сыворотка Luminesce', '1423663876.png', '122.40', 'Описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара'),
(3, 'Омолаживающая сыворотка Luminesce', '1423664108.png', '122.40', 'Описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара'),
(4, 'Омолаживающая сыворотка Luminesce', '1423664144.png', '122.40', 'Описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара'),
(5, 'Омолаживающая сыворотка Luminesce', '1423664194.png', '122.40', 'Описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара описание товара');
