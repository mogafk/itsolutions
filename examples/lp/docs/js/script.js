jQuery(function(){
	$("#close-1,#close-2,#close-3,#close-4").click(function() {
		$('#more,#call,#call2,#video').css({'visibility':'hidden','opacity':'0'});
		$('#more input:not([type=submit]),#call input:not([type=submit]),#call2 input:not([type=submit])').val('');
		return false;
	});
	
	$("#book").click(function() {
		$('#call').css({'visibility':'visible','opacity':'1'});
		return false;
	});
	
	$("#videolink").click(function() {
		$('#video').css({'visibility':'visible','opacity':'1'});
		return false;
	});
	
	$("#call2-1,#call2-2,#call2-3,#call2-4").click(function() {
		$('#call2').css({'visibility':'visible','opacity':'1'});
		return false;
	});
	
	$("#about-1,#about-2,#about-3,#about-4").click(function() {
		$("#goods_title, #goods_title2").text($(this).attr("data-title"));
		$("#goods_description").text($(this).attr("data-description"));
		$("#goods_photo").attr("src", "/upload/trade/" + $(this).attr("data-image"));
		$('#more').css({'visibility':'visible','opacity':'1'});
		return false;
	});
});

function call(num) {
    var params = 'action=call', arr = [], arr2 = ['name', 'whenadd', 'photo'], ok = 1, otv = '';
	arr.push($("#name" + num).val());
	arr.push($("#phone" + num).val());
	if(num == 1) arr.push($("#email" + num).val());
	for(i = 0; i < arr.length; i++) {
		if(arr[i] == '') {
		    ok = 0;
			break;
		};
		params += '&' + arr2[i] + '=' + arr[i];
	}
	if(num == 3) params += '&description=' + $("#goods_title").text();
	if(ok == 1) {
		$.ajax({
	        type: 'POST', 
	    	url: '/', 
	    	data: params, 
	    	success: function(data) {
				otv = data.replace(/[^a-z]+/ig,"");
				alert_animation("#" + otv + num);
				if(otv == 'success') {
				    setTimeout(function() {
                        $('#more input:not([type=submit]),#call input:not([type=submit]),#call2 input:not([type=submit])').val('');
                        setTimeout(function() {
                            $('#more,#call,#call2').css({'visibility':'hidden','opacity':'0'});
                        },250);
				    },2500);
				};
	    	}
	    });
	} else {
		alert_animation("#invalid_data" + num);
	}
}

function alert_animation(elem) {
    $(elem).slideDown(250).animate({"opacity" : 1.0},250);
	setTimeout(function() {
	    $(elem).animate({"opacity" : 0.7},250).slideUp(250);
	},1500);
}