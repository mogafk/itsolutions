<?php
define('DB_LOCATION', 'localhost');
define('DB_NAME', 'landing');
define('DB_USER', 'root');
define('DB_PASSWORD', '');

$mysqli = new mysqli(DB_LOCATION, DB_USER, DB_PASSWORD, DB_NAME);
$mysqli->set_charset("utf8");

if ($mysqli->connect_errno) {
    printf("Ошибка подключения к БД <b>".DB_NAME."</b>: %s\n", $mysqli->connect_error);
    exit();
}

/*
 * Выполнение mysql-запроса
 * @param string $q SQL-запрос
 * @return string/array $msg/$content Сообщение об итоге операции/Массив данных
 */
function q($q) {
	global $mysqli;
	$i = 0;
	$q2 = explode(" ", $q);
	if(trim($q2[0]) == "SELECT") {
	    $res = $mysqli->query($q);
		if($res->num_rows == 1) {
		    while($row = $res->fetch_assoc()) {
			    $content = $row;
			}
		} else {
		    while($row = $res->fetch_assoc()) {
		        $content[$i] = $row;
		    	$i++;
		    }
		}
		$res->close();
		if(isset($content)) return $content;
	} else {
	    $mysqli->query($q) ? $msg = 'success' : $msg = 'error';
		return $msg;
	}
}

/*
 * Обрабатываем текст перед тем, как занести его в базу
 * @param $txt string Исходная строка
 * @return string $txt Обработанная строка
 */
function sip($txt) {
    global $mysqli;
    $txt = $mysqli->real_escape_string($txt);
    $txt = strip_tags($txt);
    $txt = htmlspecialchars($txt);
    $txt = stripslashes($txt);
    $txt = addslashes($txt);
    return $txt;
}
?>