<?php
/*
 * Шаблонизатор
 * @param string $tpl Название шаблона
 * @return string $page Вывод страницы на экран
 */
function page($tpl) {
    $page = file_get_contents('templates/'.$tpl.'.tpl');
	switch($tpl) {
	    case('index'):
		    $sitename = q("SELECT * FROM `settings` WHERE `option_alias`='sitename'");
			$tags = array('{TITLE_TEXT}', '{SITENAME}');
			$values = array('Административная панель | Главная', $sitename['value']);
			break;
		case('login'):
		    $tags = array('{TITLE_TEXT}', '{TITLE_TEXT_H}');
			$values = array('Административная панель | Вход', 'Административная панель');
			break;
        case('orders'):
            $go = array("action" => "pagination", "tbl" => $tpl, "p" => 1);
			$page = ajax($go);
			break;
        case('reviews'):
            $go = array("action" => "pagination", "tbl" => $tpl, "p" => 1);
			$page = ajax($go);
			break;
        case('trade'):
            $go = array("action" => "pagination", "tbl" => $tpl, "p" => 1);
			$page = ajax($go);
			break;
        case('settings'):
            $settings = q("SELECT * FROM `settings` ORDER BY `title`");
			foreach($settings as $key => $val) {
			    $key2 = (int)$key + count($settings);
				$tags[$key] = '{'.strtoupper($val["option_alias"]).'_TITLE}';
				$tags[$key2] = '{'.strtoupper($val["option_alias"]).'_VALUE}';
				$values[$key] = $val["title"];
				$values[$key2] = $val["value"];
			}
        	break;
	}
	if($tpl != 'reviews' && $tpl != 'orders' && $tpl != 'trade') $page = str_replace($tags, $values, $page);
	return $page;
}

/*
 * Выполнение ajax-запросов
 * @param array $data Данные с формы
 * @return string $msg Сообщение об итоге операции
 */
function ajax($data) {
    foreach($data as $key => $val) {
	    $data[$key] = sip($val);
	}
	switch($data["action"]) {
	    case('login'):
	        $login = q("SELECT * FROM `settings` WHERE `option_alias`='admin_login'");
	        $password = q("SELECT * FROM `settings` WHERE `option_alias`='admin_password'");
			if($data["login"] == $login["value"] && md5($data["password"].'-'.$data["login"]) == $password["value"]) {
			    $_SESSION["admin"] = true;
				$msg = 'success';
			} else {
			    $msg = 'error';
			}
			break;
		case('add'):
		    $tbl = $data["tbl"];
			unset($data["tbl"]);
			unset($data["action"]);
			unset($data["id"]);
			$cells = "";
			$values = "";
			foreach($data as $key => $val) {
			    $cells .= "`{$key}`, ";
				$values .= "'{$val}', ";
			}
            $cells = substr($cells, 0, (strlen($cells) - 2));
            $values = substr($values, 0, (strlen($values) - 2));
			q("INSERT INTO `{$tbl}`({$cells}) VALUES ({$values})") ? $msg = 'success' : $msg = 'error';
		    if(isset($_COOKIE[$tbl.'_tmp_images']) && trim($_COOKIE[$tbl.'_tmp_images']) != '') {
			    $tmp_images = explode("-0-", $_COOKIE[$tbl.'_tmp_images']);
			    foreach($tmp_images as $key => $val) {
					if(trim($val) != '' && trim($val) != trim($data["photo"])) unlink('../upload/'.$tbl.'/'.$val);
			    }
			    setcookie($tbl.'_tmp_images', "", time() - 3600, "/", $_SERVER["SERVER_NAME"]);
			};
			break;
		case('del'):
		    $item = q("SELECT * FROM `{$data["tbl"]}` WHERE `id`='{$data["id"]}'");
			unlink('../upload/'.$data["tbl"].'/'.$item["photo"]);
			q("DELETE FROM `{$data["tbl"]}` WHERE `id`='{$data["id"]}'") ? $msg = 'success' : $msg = 'error';
		    if(isset($_COOKIE[$data["tbl"].'_tmp_images']) && trim($_COOKIE[$data["tbl"].'_tmp_images']) != '') {
			    $tmp_images = explode("-0-", $_COOKIE[$data["tbl"].'_tmp_images']);
			    foreach($tmp_images as $key => $val) {
			        if(trim($val) != '') unlink('../upload/'.$data["tbl"].'/'.$val);
			    }
			    setcookie($data["tbl"].'_tmp_images', "", time() - 3600, "/", $_SERVER["SERVER_NAME"]);
			};
			break;
		case('upd'):
		    $tbl = $data["tbl"];
			$id = $data["id"];
			unset($data["tbl"]);
			unset($data["action"]);
			unset($data["id"]);
		    $item = q("SELECT * FROM `{$tbl}` WHERE `id`='{$id}'");
			if(trim($data["photo"]) != trim($item["photo"])) unlink('../upload/'.$tbl.'/'.$item["photo"]);
			$upd = "";
			foreach($data as $key => $val) {
			    $upd .= "`{$key}`='{$val}', ";
			}
			$upd = substr($upd, 0, (strlen($upd) - 2));
			q("UPDATE `{$tbl}` SET {$upd} WHERE `id`='{$id}'") ? $msg = 'success' : $msg = 'error';
		    if(isset($_COOKIE[$tbl.'_tmp_images']) && trim($_COOKIE[$tbl.'_tmp_images']) != '') {
			    $tmp_images = explode("-0-", $_COOKIE[$tbl.'_tmp_images']);
			    foreach($tmp_images as $key => $val) {
					if(trim($val) != '' && trim($val) != trim($data["photo"])) unlink('../upload/'.$tbl.'/'.$val);
			    }
			    setcookie($tbl.'_tmp_images', "", time() - 3600, "/", $_SERVER["SERVER_NAME"]);
			};
			break;
		case('settings'):
		    unset($data["action"]);
			foreach($data as $key => $val) {
			    q("UPDATE `settings` SET `value`='{$val}' WHERE `option_alias`='{$key}'") ? $msg = 'success' : $msg = 'error';
			}
			break;
		case('admin_settings'):
		    q("UPDATE `settings` SET `value`='{$data["admin_login"]}' WHERE `option_alias`='admin_login'") ? $msg = 'success' : $msg = 'error';
			q("UPDATE `settings` SET `value`='".md5($data["admin_password"].'-'.$data["admin_login"])."' WHERE `option_alias`='admin_password'") ? $msg = 'success' : $msg = 'error';
			break;
		case('exit'):
		    $_SESSION["admin"] = false;
			session_destroy();
			$msg = 'success';
			break;
		case('content_tab'):
		    isset($data["tab"]) ? $msg = page($data["tab"]) : $msg = 'error';
			break;
		case('pagination'):
			$content = content($data["tbl"], $data["p"]);
			if(count($content) == 0 || (count($content[1]) == 0 && count($content[2]) == 0)) {
			    $tags = array('{'.strtoupper($data["tbl"]).'_1}', '{'.strtoupper($data["tbl"]).'_2}', '{PAGES}');
			    $values = array('<br /><span class="grey">Ничего не найдено.</span>', '', '');
				if($data["tbl"] == 'orders') {
				    array_push($tags, '{ORDERS_3}');
					array_push($values, '');
				};
			} else {
			    $pages = pages($data["tbl"], $data["p"]);
			    $all_reviews[1] = '';
			    $all_reviews[2] = '';
			    if($data["tbl"] == 'orders') $all_reviews[3] = '';
			    $tags = array('{'.strtoupper($data["tbl"]).'_PHOTO}', '{'.strtoupper($data["tbl"]).'_NAME}', '{'.strtoupper($data["tbl"]).'_WHENADD}', '{'.strtoupper($data["tbl"]).'_DESCRIPTION}', '{'.strtoupper($data["tbl"]).'_ID}', '{'.strtoupper($data["tbl"]).'_WHENADD2}');
			    foreach($content as $key => $val) {
			        if(count($val) == 0) {
			    	    unset($content[$key]);
			    	} else {
			    	    if(isset($val["id"]) && $val["id"] != '') {
                            $review = file_get_contents('templates/'.$data["tbl"].'_block.tpl');
                            $whenadd = explode("-", $val['whenadd']);
                            $values = array($val['photo'], $val['name'], $whenadd[2].'.'.$whenadd[1].'.'.$whenadd[0], $val['description'], $val['id'], $val['whenadd']);
                            if($data["tbl"] == 'orders') {
			    			    array_push($tags, '{ORDERS_CLASS}');
			    				trim(chop($val["description"])) != '' ? $class = 'order_tovar' : $class = 'order_zvonok';
			    				array_push($values, $class);
			    			};
			    			$all_reviews[$key] .= str_replace($tags, $values, $review);
                            unset($values);
			    		} else {
			    		    foreach($val as $key2 => $val2) {
			    	            if($val2["id"] != '') {
			    				    $review = file_get_contents('templates/'.$data["tbl"].'_block.tpl');
			    					if($data["tbl"] == 'reviews') {
			    	        	    $whenadd = explode("-", $val2['whenadd']);
			    					    $it = $whenadd[2].'.'.$whenadd[1].'.'.$whenadd[0];
			    					} else {
			    					    $it = '';
			    					}
			    	        	    $values = array($val2['photo'], $val2['name'], $it, $val2['description'], $val2['id'], $val2['whenadd']);
                                    if($data["tbl"] == 'orders') {
                                        array_push($tags, '{ORDERS_CLASS}');
                                    	trim(chop($val2["description"])) != '' ? $class = 'order_tovar' : $class = 'order_zvonok';
                                    	array_push($values, $class);
                                    };
			    					$all_reviews[$key] .= str_replace($tags, $values, $review);
			    	        	    unset($values);
			    				};
			    	        }
			    		}
			    	}
			    }
			    unset($tags);
			    $tags2 = '';
			    foreach($pages as $key => $val) {
			        if($key != 'current') {
			    	    $val == $pages['current'] ? $tags2 .= '<a href="#" class="active_page" OnClick="return false;">'.$val.'</a>' : $tags2 .= '<a href="#" OnClick="to_page(\''.$data["tbl"].'\', \''.$val.'\');return false;">'.$val.'</a>';
			    	};
			    }
			    $tags = array('{'.strtoupper($data["tbl"]).'_1}', '{'.strtoupper($data["tbl"]).'_2}', '{PAGES}');
			    count($content) == 1 ? $it = '' : $it = $all_reviews[2];
			    $values = array($all_reviews[1], $it, $tags2);
				if($data["tbl"] == 'orders') {
				    array_push($tags, '{ORDERS_3}');
					array_push($values, $all_reviews[3]);
				};
			}
			$page = file_get_contents('templates/'.$data["tbl"].'.tpl');
			$msg = str_replace($tags, $values, $page);
			break;
	}
	return $msg;
}

/*
 * Загрузка файлов
 * @param $file array Файл
 * @param $action string Что это за файл
 * @return string $msg Сообщение, удачно или не очень
 */
function upload($file, $action) {
    $filename = explode(".", $file['name']);
	$tmp = $file['tmp_name'];
	if($action == 'newlogo') {
		$name = 'logo.'.$filename[1];
		$dir = 'images';
		unlink('../'.$dir.'/'.$name);
		if(!is_dir('../'.$dir)) mkdir('../'.$dir, 0777);
		move_uploaded_file($tmp, '../'.$dir.'/'.$name) ? $msg = 'success' : $msg = 'error';
	} else {
	    $name = time().'.'.$filename[1];
		$dir = 'upload/'.$action;
		if(!is_dir('../'.$dir)) mkdir('../'.$dir, 0777);
		if(isset($_COOKIE[$action.'_tmp_images']) && trim($_COOKIE[$action.'_tmp_images']) != '') {
		    $tmp_images = explode("-0-", $_COOKIE[$action.'_tmp_images']);
			foreach($tmp_images as $key => $val) {
			    if(trim($val) == '') unset($tmp_images[$key]);
			}
			$tmp2 = implode("-0-", $tmp_images);
			setcookie($action.'_tmp_images', $tmp2, time() + (3600*24*365), "/", $_SERVER["SERVER_NAME"]);
		} else {
		    setcookie($action.'_tmp_images', $name, time() + (3600*24*365), "/", $_SERVER["SERVER_NAME"]);
		}
		move_uploaded_file($tmp, '../'.$dir.'/'.$name) ? $msg = '<script>window.parent.document.getElementById(\''.$action.'_photo\').value = "'.$name.'";</script>' : $msg = 'error';
	}
	
	return $msg;
}

/*
 * Забираем контент из бд
 * @param string $tbl таблица в БД
 * @param string $p Текущая страница
 * @return array $content Массив с информацией
 */
function content($tbl, $p) {
    $n = 20;
	$m = 0;
	$s = 0;
	$k = 2;
	$v = 2;
	if($tbl == 'orders') {
	    $k = 3;
		$v = 3;
		$n = 30;
	};
	$all = q("SELECT * FROM `{$tbl}` ORDER BY `id` DESC LIMIT ".(($p*$n)-$n).", ".$n);
	if(count($all) > 0) {
	    count($all) > ($n/$v) ? $k = $k+1 : $k = $k;
		if($tbl == 'orders' && count($all) < ($n/$v)) $k = 2;
	    for($i = 1; $i < $k; $i++) {
	    	if(isset($all["id"]) && $all["id"] != '') {
	    	    $content[$i][0] = $all;
	    	} else {
	    		foreach($all as $key => $val) {
	    			if(isset($all[$m+$s]) && count($all[$m+$s]) != 0) $content[$i][$m] = $all[$m+$s];
	    	    	$m++;
	    	    	if($m >= $n/$v) break;
	    	    }
	    	    $m = 0;
	    	    $s += $n/$v;
	    	}
	    }
	} else {
	    $content = array();
	}
	
	return $content;
}

/**
 * Пагинация для разделов админки с общим экшном
 * 
 * @return array $pages Массив с информацией
 */
function pages($tbl, $p) {
	$n = 20;
	if($tbl == 'orders') $n = 30;
	$pp = q("SELECT * FROM `{$tbl}` ORDER BY `id` DESC");
	$p2 = count($pp);
	
	if($p2 >= $n) {
	    $p2%$n == 0 ? $p3 = $p2/$n : $p3 = (($p2-($p2%$n))/$n)+1;
	} else {
	    $p3 = 1;
	}
	
	for($i = $p-2; $i <= $p+2; $i++) {
	    if($i > 0 && $i <= $p3) $pages[$i] = $i;
	}
	
	$pages['current'] = $p;
	return $pages;
}
?>