<?php
session_start();
require_once('includes/config.php');
require_once('includes/functions.php');
(isset($_SESSION["admin"]) && $_SESSION["admin"]) ? $tpl = 'index' : $tpl = 'login';
if(isset($_FILES) && !empty($_FILES) && isset($_GET) && !empty($_GET)) {
    $page = upload($_FILES["file"], sip($_GET["action"]));
} else {
    (isset($_POST) && !empty($_POST)) ? $page = ajax($_POST) : $page = page($tpl);
}
if($page != '') echo $page;
?>