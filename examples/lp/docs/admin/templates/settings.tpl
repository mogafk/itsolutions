<form id="site_settings" class="settings_form">
<div class="block_relative col_50">
	<img id="sitelogo_image" src="/images/logo.png" title="Сменить логотип" OnMouseOver="hover_elem(this);" OnMouseOut="hover_elem(this);" OnClick="change_logo();" />
	<textarea name="why_buy_1" placeholder="{WHY_BUY_1_TITLE}">{WHY_BUY_1_VALUE}</textarea>
	<textarea name="why_buy_2" placeholder="{WHY_BUY_2_TITLE}">{WHY_BUY_2_VALUE}</textarea>
	<textarea name="why_buy_3" placeholder="{WHY_BUY_3_TITLE}">{WHY_BUY_3_VALUE}</textarea>
</div>
<div class="block_relative col_50">
	<input type="text" name="header_menu_1" placeholder="{HEADER_MENU_1_TITLE}" value="{HEADER_MENU_1_VALUE}" />
	<input type="text" name="header_menu_2" placeholder="{HEADER_MENU_2_TITLE}" value="{HEADER_MENU_2_VALUE}" />
	<input type="text" name="header_menu_3" placeholder="{HEADER_MENU_3_TITLE}" value="{HEADER_MENU_3_VALUE}" />
	<input type="text" name="sitename" placeholder="{SITENAME_TITLE}" value="{SITENAME_VALUE}" />
	<input type="text" name="header_text" placeholder="{HEADER_TEXT_TITLE}" value="{HEADER_TEXT_VALUE}" />
	<input type="text" name="phone_number" placeholder="{PHONE_NUMBER_TITLE}" value="{PHONE_NUMBER_VALUE}" />
	<input type="text" name="social_fb" placeholder="{SOCIAL_FB_TITLE}" value="{SOCIAL_FB_VALUE}" />
	<input type="text" name="social_instagram" placeholder="{SOCIAL_TW_TITLE}" value="{SOCIAL_TW_VALUE}" />
	<input type="text" name="social_vk" placeholder="{SOCIAL_AVK_TITLE}" value="{SOCIAL_AVK_VALUE}" />
	<input type="text" name="youtube_video" placeholder="{YOUTUBE_VIDEO_TITLE}" value="{YOUTUBE_VIDEO_VALUE}" />
	<button class="success" type="submit" OnClick="return false;">Изменения применены</button>
	<button class="error" type="submit" OnClick="return false;">Изменения не были применены</button>
	<button type="submit" OnClick="settings_update('#site_settings');return false;">Изменить</button>
</div>
</form>
<div id="adm_block" class="block_relative col_33">
    <form id="admin_settings">
	<input type="text" name="admin_login" placeholder="Логин администратора" value="{ADMIN_LOGIN_VALUE}" />
	<input type="password" name="admin_password" placeholder="Пароль администратора" />
	<button class="success" type="submit" OnClick="return false;">Изменения применены</button>
	<button class="error" type="submit" OnClick="return false;">Изменения не были применены</button>
	<button type="submit" OnClick="settings_update('#admin_settings');return false;">Изменить</button>
	</form>
</div>