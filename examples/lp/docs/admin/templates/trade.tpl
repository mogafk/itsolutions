<div class="pages col_100">
    {PAGES}
</div>
<div class="block_relative col_33">
    <ul class="reviews">
	    {TRADE_1}
	</ul>
</div>
<div class="block_relative col_33">
    <ul class="reviews">
	    {TRADE_2}
	</ul>
</div>
<div class="block_relative col_33">
    <form id="add_trade">
	    <img id="sitelogo_image" src="/admin/templates/assets/img/photo.png" title="Добавить фото" OnMouseOver="hover_elem(this);" OnMouseOut="hover_elem(this);" OnClick="change_photo();" style="cursor: pointer;" />
		<input type="text" name="name" placeholder="Название" />
		<input id="whenadd" type="number" name="whenadd" placeholder="Стоимость товара" />
		<input id="trade_photo" type="hidden" name="photo" />
		<input type="hidden" name="id" value="0" />
		<textarea name="description" placeholder="Описание товара"></textarea>
	    <button class="success" type="submit" OnClick="return false;">Материал добавлен!</button>
	    <button class="error" type="submit" OnClick="return false;">Заполните поля!</button>
	    <button id="trade_submit" type="submit" OnClick="submit_form('#add_trade', 'add', 'trade');return false;">Добавить</button>
	</form>
    <form class="hide_elem" id="change_logo_form" enctype="multipart/form-data" method="post">
        <input id="new_logo" type="file" name="file" accept="image/*,image/jpeg" OnChange="add_new_photo();" />
        <input type="submit" class="submit" value="Загрузить!" />
    </form>
</div>