<li id="trade_items-{TRADE_ID}">
    <div class="edit_icons">#{TRADE_ID}
	    <a href="#" OnClick="edititem('trade', '{TRADE_ID}');return false;">Редактировать</a> / <a href="#" OnClick="delitem('trade', '{TRADE_ID}');return false;">Удалить</a>
	</div>
	<form class="hide_elem">
	    <input type="hidden" name="id" value="{TRADE_ID}" />
		<input type="hidden" name="name" value="{TRADE_NAME}" />
		<input type="hidden" name="photo" value="{TRADE_PHOTO}" />
		<input type="hidden" name="whenadd" value="{TRADE_WHENADD2}" />
		<textarea name="description" value="{TRADE_DESCRIPTION}">{TRADE_DESCRIPTION}</textarea>
	</form>
	<img src="/upload/trade/{TRADE_PHOTO}" />
	<h3>{TRADE_NAME}</h3>
	<br />
	<span class="grey bold">${TRADE_WHENADD2}</span>
	<br />
	<br />
	{TRADE_DESCRIPTION}
</li>