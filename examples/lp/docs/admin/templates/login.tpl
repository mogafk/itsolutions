<!DOCTYPE html>
<html lang="ru" class="no-js">
    <head>
        <meta charset="utf-8">
        <title>{TITLE_TEXT}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		
        <!-- CSS -->
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
        <link rel="stylesheet" href="/admin/templates/assets/css/reset.css">
        <link rel="stylesheet" href="/admin/templates/assets/css/supersized.css">
        <link rel="stylesheet" href="/admin/templates/assets/css/style.css">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

    </head>

    <body>

        <div class="page-container">
            <h1>{TITLE_TEXT_H}</h1>
            <form method="post">
                <input id="login" type="text" name="username" class="username" placeholder="Логин">
                <input id="pass" type="password" name="password" class="password" placeholder="Пароль">
                <button type="submit">Войти</button>
				<div class="msg success">Добро пожаловать!</div>
				<div class="msg error">Ошибка входа!</div>
            </form>
        </div>

        <!-- Javascript -->
        <script src="/admin/templates/assets/js/jquery-1.8.2.min.js"></script>
        <script src="/admin/templates/assets/js/supersized.3.2.7.min.js"></script>
        <script src="/admin/templates/assets/js/supersized-init.js"></script>
        <script src="/admin/templates/assets/js/scripts.js"></script>

    </body>

</html>

