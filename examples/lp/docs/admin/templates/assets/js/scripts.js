var p = 1;
jQuery(document).ready(function() {
	var cur = new Date(), tags = ['orders', 'reviews', 'trade', 'settings'], txt = ['Заказы', 'Отзывы', 'Товары', 'Настройки'];
	$("#main").css({"min-height" : (getClientHeight()-50) + "px"});
	cur.getFullYear() == 2015 ? $('#year').html("&copy; " + cur.getFullYear() + " ") : $('#year').html("&copy; 2015-" + cur.getFullYear() + " ");
	if(typeof($("#main").attr("id")) !== "undefined" && window.location.hash == '') window.location.hash = '#orders';
	$("header ul a").each(function(i) {
		if($(this).attr("href") == window.location.hash) {
		    $(".hide_content").css({"display" : "none"});
		    $($(this).attr("href") + " .content").load('/admin/templates/loading.tpl');
			$($(this).attr("href")).fadeIn(250);
			$(this).addClass('current_tab');
			var tab = window.location.hash.split("#");
			$("title").text('Административная панель | ' + txt[tags.indexOf(tab[1].replace(/[^a-z]+/ig,""))]);
            $.ajax({
        	    type: 'POST', 
        		url: '/admin/', 
        		data: 'action=content_tab&tab=' + tab[1].replace(/[^a-z]+/ig,""), 
        		success: function(data) {
        			var otv = data.replace(/[^a-z]+/ig,"");
					otv == 'error' ? go = 'Ошибка. Попробуйте ещё раз.' : go = data;
					setTimeout(function() {
						$(window.location.hash + " .content").css({"display" : "none"});
						$(window.location.hash + " .content").html(go).fadeIn(250);
                        $(".block_relative .success").css({"display" : "none"});
                        $(".block_relative .error").css({"display" : "none"});
                        window.p = 1;
						if(window.location.hash == '#settings') {
                            if(typeof($("#change_logo_container").attr("id")) === "undefined") {
                                $("#adm_block").append('<div id="change_logo_container"></div>');
                                $("#change_logo_container").load('/admin/templates/change_logo_form.tpl');
                            };
                        };
					},500);
        		}
        	});
		};
	});
	$("header ul a").click(function() {
		window.location.hash = $(this).attr("href");
		$(".hide_content").css({"display" : "none"});
		$($(this).attr("href") + " .content").load('/admin/templates/loading.tpl');
		$($(this).attr("href")).fadeIn(250);
		$("header ul a").each(function(i) {
		    if($(this).attr("class") != "new_blank") $(this).removeClass();
			if($(this).attr("href") == window.location.hash) $(this).removeClass().addClass('current_tab');
		});
		var tab = window.location.hash.split("#");
		$("title").text('Административная панель | ' + txt[tags.indexOf(tab[1].replace(/[^a-z]+/ig,""))]);
        $.ajax({
            type: 'POST', 
        	url: '/admin/', 
        	data: 'action=content_tab&tab=' + tab[1].replace(/[^a-z]+/ig,""), 
        	success: function(data) {
                var otv = data.replace(/[^a-z]+/ig,"");
                otv == 'error' ? go = 'Ошибка. Попробуйте ещё раз.' : go = data;
				setTimeout(function() {
        			$(window.location.hash + " .content").css({"display" : "none"});
					$(window.location.hash + " .content").html(go).fadeIn(250);
                    $(".block_relative .success").css({"display" : "none"});
                    $(".block_relative .error").css({"display" : "none"});
					window.p = 1;
                    if(window.location.hash == '#settings') {
                        if(typeof($("#change_logo_container").attr("id")) === "undefined") {
                            $("#adm_block").append('<div id="change_logo_container"></div>');
                            $("#change_logo_container").load('/admin/templates/change_logo_form.tpl');
                        };
					};
        		},500);
        	}
        });
	});
	$('.page-container form').submit(function(){
        var username = $(this).find('.username').val();
        var password = $(this).find('.password').val();
        if(username != '' && password != '') {
            $.ajax({
        	    type: 'POST', 
        		url: '/admin/', 
        		data: 'action=login&login=' + username + '&password=' + password, 
        		success: function(data) {
                    var otv = data.replace(/[^a-z]+/ig,"");
                    $('.page-container form').find('.' + otv).slideDown(250).animate({opacity: "1.0"},250).delay(1000).animate({opacity: "0.7"},250).slideUp(250);
        			setTimeout(function() {
						if(otv == 'success') {
						    $("body").fadeOut(250);
						    setTimeout(function(){
							    document.location.href = '/admin/';
							},250);
						};
					}, 2000);
        		}
        	});
        } else {
            $(this).find('.error').slideDown(250).animate({opacity: "1.0"},250).delay(1000).animate({opacity: "0.7"},250).slideUp(250);
            if(username == '') $(this).parent().find('.username').focus();
            if(username != '' && password == '') $(this).parent().find('.password').focus();
        }
		return false;
    });
});

function exit() {
    $.ajax({
        type: 'POST', 
    	url: '/admin/', 
    	data: 'action=exit', 
    	success: function(data) {
            var otv = data.replace(/[^a-z]+/ig,"");
            if(otv == 'success') {
                $("body").fadeOut(250);
                setTimeout(function(){
            	    document.location.href = '/admin/';
            	},250);
            };
    	}
    });
}

function settings_update(formid) {
    var action = '', params = '', ok = 1;
	formid == '#site_settings' ? action = 'settings' : action = 'admin_settings';
	params = 'action=' + action;
	$(formid + ' input, ' + formid + ' textarea').each(function(i) {
	    if($(this).val() == '') {
		    ok = 0;
		} else {
		    params += '&' + $(this).attr("name") + '=' + $(this).val();
			if($(this).attr("name") == 'admin_password') $(this).val('');
		}
	});
	if(ok == 1) {
	    $.ajax({
            type: 'POST', 
        	url: '/admin/', 
        	data: params, 
        	success: function(data) {
                var otv = data.replace(/[^a-z]+/ig,"");
                $(formid + ' .' + otv).fadeIn(250);
				setTimeout(function() {
				    $(formid + ' .' + otv).fadeOut(250);
				},1250);
        	}
        });
	} else {
        $(formid + ' .error').fadeIn(250);
        setTimeout(function() {
            $(formid + ' .error').fadeOut(250);
        },1250);
	}
}

function getClientHeight() {
    return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight:document.body.clientHeight;
}

function hover_elem(elem) {
	$(elem).css("opacity") < 1.0 ? $(elem).animate({"opacity" : 1.0},250) : $(elem).animate({"opacity" : 0.7},250);
}

function change_logo() {
    $("#new_logo").trigger('click');
}

function change_photo() {
    $("#new_logo").trigger('click');
}

function accept_new_logo() {
	var lol = new Date().getTime();
	$$f({
		formid:'change_logo_form',
		url:'/admin/?action=newlogo', 
		onstart:function() {
		    $$('sitelogo_image',$$('sitelogo_image').src = "/admin/templates/assets/img/loading.gif");
		},
		onsend:function() {
			setTimeout(function() {
			    $$('sitelogo_image',$$('sitelogo_image').src = "/images/logo.png?" + Math.floor(Date.now() / 1000));
			},1000);
		}
	});
}

function add_new_photo() {
	var tab = window.location.hash.split("#");
	$$f({
		formid:'change_logo_form',
		url:'/admin/?action=' + tab[1].replace(/[^a-z]+/ig,""), 
		onstart:function() {
		    $$('sitelogo_image',$$('sitelogo_image').src = "/admin/templates/assets/img/loading.gif");
		},
		onsend:function() {
			setTimeout(function() {
			    $$('sitelogo_image',$$('sitelogo_image').src = "/upload/" + tab[1].replace(/[^a-z]+/ig,"") + "/" + $(window.location.hash + "_photo").val());
			},1000);
		}
	});
}

function to_page(tbl, p) {
    window.p = p;
	$.ajax({
        type: 'POST', 
    	url: '/admin/', 
    	data: 'action=pagination&tbl=' + tbl + '&p=' + p, 
    	success: function(data) {
            var otv = data.replace(/[^a-z]+/ig,"");
            otv == 'error' ? go = 'Ошибка. Попробуйте ещё раз.' : go = data;
            setTimeout(function() {
            	$("#" + tbl + " .content").css({"display" : "none"});
            	$("#" + tbl + " .content").html(go).fadeIn(250);
                $(".block_relative .success").css({"display" : "none"});
                $(".block_relative .error").css({"display" : "none"});
            },250);
    	}
    });
}

function submit_form(formid, action, tbl) {
	var ok = 1, params = 'action=' + action + '&tbl=' + tbl;
	$(formid + ' input, ' + formid + ' textarea').each(function(i) {
	    if(typeof($(this).attr("name")) !== "undefined") {
		    if($(this).val() == '') {
		        ok = 0;
		    } else {
		        params += '&' + $(this).attr("name") + '=' + $(this).val();
		    }
		};
	});
	if(ok == 1) {
        $.ajax({
            type: 'POST', 
        	url: '/admin/', 
        	data: params, 
        	success: function(data) {
                var otv = data.replace(/[^a-z]+/ig,"");
				$(formid + " ." + otv).fadeIn(250);
                if(otv == 'success') {
					$(formid + ' input, ' + formid + ' textarea').val('');
				    $("#sitelogo_image").attr("src", "/admin/templates/assets/img/loading.gif");
				    $("#" + tbl + "_submit").attr("OnClick", "submit_form('#add_" + tbl + "', 'add', '" + tbl + "');return false;").text('Добавить');
				    setTimeout(function() {
				        $("#sitelogo_image").attr("src", "/admin/templates/assets/img/photo.png");
				    },500);
				};
				setTimeout(function() {
                    $(formid + " ." + otv).fadeOut(250);
					setTimeout(function() {
					    $("#" + tbl + " .success").text('Материал добавлен!');
						if(otv == 'success') refresh_content(tbl);
					},250);
                },1250);
        	}
        });
	} else {
        $(formid + " .error").fadeIn(250);
        setTimeout(function() {
            $(formid + " .error").fadeOut(250);
        },1250);
	}
}

function set_value(value, elem) {
    $(elem).val(value);
}

function delitem(tbl, id) {
    if(confirm('Подтвердите удаление')) {
        $.ajax({
            type: 'POST', 
        	url: '/admin/', 
        	data: 'action=del&tbl=' + tbl + '&id=' + id, 
        	success: function(data) {
                var otv = data.replace(/[^a-z]+/ig,"");
                if(otv == 'success') {
				    $("#" + tbl + "_items-" + id).fadeOut(250);
				    setTimeout(function() {
					    refresh_content(tbl);
                    },250);
				};
        	}
        });
	};
}

function edititem(tbl, id) {
    var arr = [], arr2 = [];
	$("#" + tbl + "_items-" + id + " .hide_elem input, #" + tbl + "_items-" + id + " .hide_elem textarea").each(function(i) {
	    if(typeof($(this).attr("name")) !== "undefined" && $(this).val() != '') {
		    arr.push($(this).attr("name"));
			arr2.push($(this).val());
		};
	});
	$("#add_" + tbl + " input, #add_" + tbl + " textarea").each(function(i) {
	    if(typeof($(this).attr("name")) !== "undefined") {
			for(k = 0; k < arr.length; k++) {
				if($(this).attr("name") == arr[k]) {
					$(this).val(arr2[k]);
					if($(this).attr("name") == "whenadd") $("#whenadd_date").val(arr2[k]);
				};
			}
		};
	});
	$("#" + tbl + " .success").text('Материал изменён!');
	$("#sitelogo_image").attr("src", "/admin/templates/assets/img/loading.gif");
	$("#" + tbl + "_submit").attr("OnClick", "submit_form('#add_" + tbl + "', 'upd', '" + tbl + "');return false;").text('Изменить');
	setTimeout(function() {
	    $("#sitelogo_image").attr("src", "/upload/" + tbl + "/" + arr2[arr.indexOf('photo')]);
	},500);
}

function refresh_content(tbl) {
    $.ajax({
        type: 'POST', 
    	url: '/admin/', 
    	data: 'action=pagination&tbl=' + tbl + '&p=' + window.p, 
    	success: function(data) {
            var otv = data.replace(/[^a-z]+/ig,"");
            otv == 'error' ? go = 'Ошибка. Попробуйте ещё раз.' : go = data;
            setTimeout(function() {
            	$("#" + tbl + " .content").html(go);
                $(".block_relative .success").css({"display" : "none"});
                $(".block_relative .error").css({"display" : "none"});
            },250);
    	}
    });
}

function scroll_to_top() {
    $("html, body").animate({scrollTop: 0},250)
}