<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <title>{TITLE_TEXT}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="Cache-Control" content="no-cache">
        <!-- CSS -->
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
        <link rel="stylesheet" href="/admin/templates/assets/css/reset.css">
        <link rel="stylesheet" href="/admin/templates/assets/css/style.css">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

    </head>

    <body>
	
	<header>
	    <ul>
		    <span class="new_blank" OnClick="window.open('/','_blank'); return false;"><li>{SITENAME}</li></span>
			<a href="#orders"><li>Заявки</li></a>
			<a href="#reviews"><li>Отзывы</li></a>
			<a href="#trade"><li>Товары</li></a>
			<a href="#settings"><li>Настройки</li></a>
			<a href="#" OnClick="exit();return false;"><li>Выход</li></a>
		</ul>
	</header>
	
	<div id="main" class="main">
	    <div id="orders" class="hide_content">
		    <H1>Заявки</H1>
			<div class="content">
                <div class="loading">
                    <img src="/admin/templates/assets/img/loading.gif" />
                </div>
			</div>
		</div>
	    <div id="reviews" class="hide_content">
		    <H1>Отзывы</H1>
			<div class="content">
                <div class="loading">
                    <img src="/admin/templates/assets/img/loading.gif" />
                </div>
			</div>
		</div>
	    <div id="trade" class="hide_content">
		    <H1>Товары</H1>
			<div class="content">
                <div class="loading">
                    <img src="/admin/templates/assets/img/loading.gif" />
                </div>
			</div>
		</div>
	    <div id="settings" class="hide_content">
		    <H1>Настройки</H1>
			<div class="content">
                <div class="loading">
                    <img src="/admin/templates/assets/img/loading.gif" />
                </div>
			</div>
		</div>
	</div>
	
	<footer>
		<div style="position: absolute; margin: 0;"><a href="#" OnClick="scroll_to_top();return false;">&#9650; Наверх</a></div>
		<a href="http://itcenter.me" target="_blank">Центр IT Решений</a> <span id="year"></span>
	</footer>

        <!-- Javascript -->
        <script src="/admin/templates/assets/js/jquery-1.10.2.min.js"></script>
		<script src="/admin/templates/assets/js/scriptjava.js"></script>
        <script src="/admin/templates/assets/js/scripts.js"></script>

    </body>

</html>

