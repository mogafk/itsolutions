<div class="pages col_100">
    {PAGES}
</div>
<div class="block_relative col_33">
    <ul class="reviews">
	    {REVIEWS_1}
	</ul>
</div>
<div class="block_relative col_33">
    <ul class="reviews">
	    {REVIEWS_2}
	</ul>
</div>
<div class="block_relative col_33">
    <form id="add_reviews">
	    <img id="sitelogo_image" src="/admin/templates/assets/img/photo.png" title="Добавить фото" OnMouseOver="hover_elem(this);" OnMouseOut="hover_elem(this);" OnClick="change_photo();" style="cursor: pointer;" />
		<input type="text" name="name" placeholder="Имя" />
		<input id="whenadd_date" type="date" placeholder="Дата" OnChange="set_value(this.value, '#whenadd');" />
		<input id="whenadd" type="hidden" name="whenadd" />
		<input id="reviews_photo" type="hidden" name="photo" />
		<input type="hidden" name="id" value="0" />
		<textarea name="description" placeholder="Текст отзыва"></textarea>
	    <button class="success" type="submit" OnClick="return false;">Материал добавлен!</button>
	    <button class="error" type="submit" OnClick="return false;">Заполните поля!</button>
	    <button id="reviews_submit" type="submit" OnClick="submit_form('#add_reviews', 'add', 'reviews');return false;">Добавить</button>
	</form>
    <form class="hide_elem" id="change_logo_form" enctype="multipart/form-data" method="post">
        <input id="new_logo" type="file" name="file" accept="image/*,image/jpeg" OnChange="add_new_photo();" />
        <input type="submit" class="submit" value="Загрузить!" />
    </form>
</div>