<?php
require_once 'admin/includes/config.php';
require_once 'admin/includes/frontend.php';
if(isset($_POST) && !empty($_POST)) {
    echo call($_POST);
} else {
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="/css/style.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="/js/script.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1">
<title><?php echo $vars["ss"]["sitename"];?></title>
</head>
<body>
	<div class="window" id="video">
		<div class="more">
			<div class="window-wrapper">
				<div class="window-header">
					<input type="button" class="close" value="" id="close-4"/>
				</div>
				<div class="info">
					<iframe width="693" height="500" style="margin: 0 auto;" src="https://www.youtube.com/embed/<?php echo $vars["ss"]["youtube_video"];?>" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
	<div class="window" id="call">
		<div class="call">
			<div class="window-wrapper">
				<div class="window-header">
					<input type="button" class="close" value="" id="close-1"/>
				</div>
				<div class="info">
					<form>
						<input id="name1" type="text" pattern="[A-Za-zА-Яа-яЁё]{2,30}" placeholder="Ваше имя"/>
						<input id="email1" type="email" placeholder="Ваш E-mail"/>
						<input id="phone1" type="tel" pattern="[0-9]{5,11}" placeholder="Номер телефона"/>
						<input type="submit" value="Заказать звонок" class="order" OnClick="call(1);return false;"/>
						<div id="success1" class="msg success">Данные приняты! Скоро мы вам позвоним!</div>
						<div id="error1" class="msg error">Ошибка! Данные не отправлены!</div>
						<div id="invalid_data1" class="msg error">Пожалуйста, заполните все поля.</div>
						<div id="invalid_name1" class="msg error">В поле имени присутствуют недопустимые символы.</div>
						<div id="invalid_phone1" class="msg error">Номер введён неверно. Введите от 5 до 11 цифр.</div>
						<div id="invalid_email1" class="msg error">Некорректный Email-адрес.</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="window" id="call2">
		<div class="call">
			<div class="window-wrapper">
				<div class="window-header">
					<input type="button" class="close" value="" id="close-3"/>
				</div>
				<div class="info">
					<form>
						<input id="name2" type="text" pattern="[A-Za-zА-Яа-яЁё]{2,30}" placeholder="Ваше имя"/>
						<input id="phone2" type="tel" pattern="[0-9]{5,11}" placeholder="Номер телефона"/>
						<input type="submit" value="Заказать звонок" class="order" OnClick="call(2);return false;"/>
						<div id="success2" class="msg success">Данные приняты! Скоро мы вам позвоним!</div>
						<div id="error2" class="msg error">Ошибка! Данные не отправлены!</div>
						<div id="invalid_data2" class="msg error">Пожалуйста, заполните все поля.</div>
						<div id="invalid_name2" class="msg error">В поле имени присутствуют недопустимые символы.</div>
						<div id="invalid_phone2" class="msg error">Номер введён неверно. Введите от 5 до 11 цифр.</div>
						<div id="invalid_email2" class="msg error">Некорректный Email-адрес.</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="window" id="more">
		<div class="more">
			<div class="window-wrapper">
				<div class="window-header">
					<h2 id="goods_title">Название товара</h2>
					<input type="button" class="close" value="" id="close-2"/>
				</div>
				<div class="details">
					<h3 id="goods_title2">Омолаживающая сыровотка</h3>
					<p id="goods_description"></p>
					<h3>Заказать товар</h3>
					<div class="order-good">
						<img id="goods_photo" src="/images/good.png"/>
						<div class="info">
							<form>
								<input id="name3" type="text" pattern="[A-Za-zА-Яа-яЁё]{2,30}" placeholder="Ваше имя"/>
								<input id="phone3" type="tel" pattern="[0-9]{5,11}" placeholder="Номер телефона"/>
								<input type="submit" value="Заказать звонок" class="order" OnClick="call(3);return false;"/>
                                <div id="success3" class="msg success">Данные приняты! Скоро мы вам позвоним!</div>
                                <div id="error3" class="msg error">Ошибка! Данные не отправлены!</div>
                                <div id="invalid_data3" class="msg error">Пожалуйста, заполните все поля.</div>
                                <div id="invalid_name3" class="msg error">В поле имени присутствуют недопустимые символы.</div>
                                <div id="invalid_phone3" class="msg error">Номер введён неверно. Введите от 5 до 11 цифр.</div>
                                <div id="invalid_email3" class="msg error">Некорректный Email-адрес.</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="menu">
		<?php
			$menu = array('Компания', 'Каталог', 'Контакты');
			$i = 0;
			foreach($vars["ss"]["header"] as $item) {
			    echo '<a href="'.$item.'">'.$menu[$i].'</a>';
				$i++;
			}
		?>
	</div>
	<div class="header">
		<div class="gradient">
			<div class="header-wrapper">
				<div class="header-main">
					<div class="logo">
						<a href="#"><img src="/images/logo.png"/></a>
						<h1><?php echo $vars["ss"]["header_text"];?></h1>
					</div>
					<div class="book">
						<h1>Есть вопросы? Звоните!</h1>
						<input type="button" value="Заказать звонок" class="book-btn" id="book"/>
					</div>
					<div class="telephone">
						<h1><?php echo $vars["ss"]["phone_number"];?></h1>
					</div>
				</div>
				<div class="premium">
					<div class="prem">
						<img src="/images/car.png"/>
						<h1>Бесплатная доставка </br>по всей России</h1>
					</div>
					<a id="videolink" href="#">
					    <div class="prem">
					    	<img src="/images/video.png" class="video-img"/>
					    	<h1>Видео презентация</br>компании <?php echo $vars["ss"]["sitename"];?></h1>
					    </div>
					</a>
				</div>
				<div class="why">
					<h1>Почему девушки покупают косметику <?php echo $vars["ss"]["sitename"];?></h1>
					<ul>
						<li>
							<img src="/images/list-img1.png"/>
							<h1><?php echo $vars["ss"]["why_buy_1"];?></h1>
						</li>
						<li>
							<img src="/images/list-img3.png"/>
							<h1><?php echo $vars["ss"]["why_buy_2"];?></h1>
						</li>
						<li>
							<img src="/images/list-img5.png"/>
							<h1><?php echo $vars["ss"]["why_buy_3"];?></h1>
						</li>
					</ul>
				</div>
				<div class="bonus">
					<h1>Бонус при заказе</h1>
					<img src="/images/bonus.png"/>
				</div>
			</div>
		</div>
	</div>
	<div class="goods">
		<div class="gradient">
			<div class="goods-wrapper">
				<?php
				    foreach($vars["goods"] as $key => $val) {
                        echo '
					        <div class="good">
                            	<img src="/upload/trade/'.$val["photo"].'"/>
                            	<h3>'.$val["name"].'<br>$'.$val["whenadd"].'</h3>
                            	<div class="good-btn">
                            		<input type="button" value="Подробнее" data-image="'.$val["photo"].'" data-title="'.$val["name"].'" data-description="'.$val["description"].'" class="about" id="about-'.((int)$key+1).'"/>
                            		<input type="button" value="Заказать" class="zakaz" id="call2-'.((int)$key+1).'"/>
                            	</div>
                            </div>
					    ';
					}
				?>
			</div>
		</div>
	</div>
	<div class="company">
		<div class="company-wrapper">
			<img src="/images/logo2.png"/>
			<h1><?php echo $vars["ss"]["sitename"];?></h1>
		</div>
	</div>
	<div class="footer-bg">
		<div class="gradient">
			<div class="footer-wrapper">
				<div class="comments">
					<?php
					    foreach($vars["reviews"] as $key => $val) {
                            $key == 0 ? $c = ' first' : $c = '';
							echo '
						        <div class="comment'.$c.'">
                                	<div class="autor">
                                		<img src="/upload/reviews/'.$val["photo"].'"/>
                                		<h2>Валерия</h2>
                                		<h3>'.$val["whenadd"].'</h3>
                                	</div>
                                	<p>'.$val["description"].'</p>
                                </div>
						    ';
						}
					?>
				</div>
				<div class="footer">
					<h2><?php echo $vars["ss"]["sitename"];?> © 2015</h2>
					<ul>
						<?php
						    foreach($vars["ss"]["social"] as $key => $val) {
						        if($val != '' && $val != '#') echo '<li><a href="'.$val.'" target="_blank"><img src="/images/'.$key.'.png"/></a></li>';
						    }
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<?php
}
?>