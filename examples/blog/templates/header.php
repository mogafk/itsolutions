﻿<!DOCTYPE html>
<html>
<head>
    <title>Блог | <? echo $title.' | '.title($_GET['cat'], $_GET['post']);?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="description" content="<? meta_description($_GET['post']);?>">
	<link rel="shortcut icon" href="<? echo $siteurl;?>/templates/img/logo.jpg" type="image/x-icon" />
    <link rel="icon" href="<? echo $siteurl;?>/templates/img/logo.jpg" type="image/x-icon" />
    <link rel="stylesheet" href="<? echo $siteurl;?>/templates/style.css" type="text/css; charset=utf-8">
	<noscript>
	Для использования сайта необходимо включить Javascript.
	</noscript>
</head>
<body>
    <div id="all">
        <div id="header">
		    <a href="<? echo $siteurl;?>">
                <img id="logo" src="/templates/img/logo.jpg">
                <H1><?php echo $title;?></H1>
			</a>
            <b class="ann"><? echo title($_GET['cat'], $_GET['post']);?></b>
        </div>
		<div id="nav"><? links($_GET['cat'], $_GET['post']);?></div>
        <div id="content">