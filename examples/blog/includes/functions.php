﻿<?php
$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
$mysqli->set_charset("utf8");

function showcats() {
global $mysqli;
    $res = $mysqli->query("SELECT * FROM `sections`");
	    if(!$res->num_rows)
		{
		    echo 'Нет разделов :(';
		} else {
			while($row = $res->fetch_assoc())
			{
			    echo '<li><a href="/cathegory/'.$row['link'].'/">'.$row['title'].'</a></li>';
			}
		}
	$res->close();
}

function title($cat, $post) {
global $mysqli;
    if(isset($cat) && $cat != '')
	{
	    $res = $mysqli->query("SELECT `title` AS t FROM `sections` WHERE `link`='{$cat}'");
		    $title = $res->fetch_object()->t;
		$res->close();
		} elseif (isset($post) && $post != '')
		{
			$res = $mysqli->query("SELECT `title` AS t, `id` AS d FROM `posts` WHERE `link`='{$post}'");
			$titl = $res->fetch_object();
			$cat = postinfo($titl->d);
		        $title .= $cat['title'].' | '.$titl->t;
		    $res->close();
	} else {
			$title = 'Главная';
	}
return $title;
}

function links($cat, $post) {
global $mysqli;
global $siteurl;
$title = '<a href="'.$siteurl.'">Главная</a>';
    if(isset($cat) && $cat != '')
	{
	    $res = $mysqli->query("SELECT * FROM `sections` WHERE `link`='{$cat}'");
		    $row = $res->fetch_assoc();
		        $title .= ' <img class="arrow" src="/templates/img/right.png" /> <a href="/cathegory/'.$row['link'].'/">'.$row['title'].'</a>';
		$res->close();
	};
	if(isset($post) && $post != '')
	{
		$res2 = $mysqli->query("SELECT * FROM `posts` WHERE `link`='{$post}'");
		    $row2 = $res2->fetch_assoc();
			$cinfo = postinfo($row2['id']);
		        $title .= ' <img class="arrow" src="/templates/img/right.png" /> <a href="/cathegory/'.$cinfo['link'].'/">'.$cinfo['title'].'</a> <img class="arrow" src="/templates/img/right.png" /> <a href="/post/'.$row2['link'].'/">'.$row2['title'].'</a>';
		$res2->close();
	};
echo $title;
}

function cat($cat, $postid) {
global $mysqli;
$posts = '';
    if(isset($postid) && $postid != '') 
    {
        $mode = 1;
		$res = $mysqli->query("SELECT `id` AS d FROM `posts` WHERE `link`='{$postid}'");
		$id = $res->fetch_object()->d;
		echo post($id, $mode);
		$res->close();
    } else {
        $mode = 0;
        if(isset($cat) && $cat != '')
	    {
	        $res = $mysqli->query("SELECT `id` AS cid FROM `sections` WHERE `link`='{$cat}'");
	            $cid = $res->fetch_object()->cid;
	            $q = "SELECT `id` AS id FROM `posts` WHERE `section`='{$cid}' ORDER BY `id` DESC";
				$res->close();
	    } else {
	    	$q = "SELECT `id` AS id FROM `posts` ORDER BY `id` DESC";
	    }
	    $res = $mysqli->query($q);
	    if(!$res->num_rows)
	    {
	        $posts = 'Пока нет статей :(';
	    } else {
	    	$cnt = $res->num_rows;
	    	$n = 5;
	    	$i = 1;
		    $current = 0;
	    	while($id = $res->fetch_object())
	    	{
		    	if($i == 1 || ($i-1)%$n == 0)
		        {
				    $current = $current+1;
		    		$posts .= '<div class="allposts" id="posts-'.$current.'">';
		    	};
		    	$posts .= post($id->id, $mode);

		    	if($i%$n == 0 || $i == $cnt)
			    {
			        $posts .= '</div>';
			    };
			    $i = $i+1;
		    }
	    }
	$res->close();
    echo $posts;
    echo '<br />
    <div id="more"><a href="#footer" id="showmore" value="1">Хочу ещё :)</a></div>
	<br />';
	}
}

function post($id,$mode) {
global $mysqli;
$full = '';
    $res = $mysqli->query("SELECT * FROM `posts` WHERE `id`='{$id}'");
	    $row = $res->fetch_assoc();
	    $date = new DateTime($row['date']);
	    $text = $row['text'];
	    $cat = postinfo($id);
	if($mode == 0) 
	{
	    $text = cutString($text, 300);
		$full = ' | <a href="/article/'.$row['link'].'/">Читать полностью</a>';
	} else {
	    hits($id);
	}
	$post = '<div class="post">
	            <div class="posttitle"><a href="/article/'.$row['link'].'/">'.$row['title'].'</a></div>
				<div class="posttime">'.$date->format('d.m.y - G:i').'</div>
				<br />
				<div class="posttext">'.nl2br(isip($text)).'</div>
				<br />
				<div class="postcat">Раздел: <a href="/cathegory/'.$cat['link'].'/">'.$cat['title'].'</a> | Просмотров: '.$cat['hits'].$full.'</div>
			 </div>';
	$res->close();
return $post;
}

function postinfo($id) {
global $mysqli;
$cat = array();
    $res = $mysqli->query("SELECT `sections`.`title`, `sections`.`link`, `posts`.`hits` FROM `sections` INNER JOIN `posts` ON `posts`.`section`=`sections`.`id` WHERE `posts`.`id`='{$id}'");
	    $row = $res->fetch_assoc();
	    $cat['link'] = $row['link'];
	    $cat['hits'] = $row['hits'];
	    $cat['title'] = $row['title'];
	$res->close();
return $cat;
}

function cutString($string, $maxlen) {
    $len = (mb_strlen($string) > $maxlen)
        ? mb_strripos(mb_substr($string, 0, $maxlen), ' ')
        : $maxlen
    ;
    $cutStr = mb_substr($string, 0, $len);
    return (mb_strlen($string) > $maxlen)
        ? $cutStr.'...'
        : $cutStr
    ;
}

function hits($id) {
global $mysqli;
//If you have browscap.ini(PHP lib) on your hosting, you can get more info about client.
//$browser = get_browser(null, true);
//$browser['parent'].$browser['platform'].$browser['browser'].$browser['version']
    $user = md5(getenv("COMPUTERNAME").$_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);
    $res = $mysqli->query("SELECT * FROM `hits` WHERE `user`='{$user}' AND `item_id`='{$id}'");
	    if(!$res->num_rows)
		{
			$res2 = $mysqli->query("SELECT `hits` AS hits FROM `posts` WHERE `id`='{$id}'");
				$hits = $res2->fetch_object()->hits;
			$res2->close();
			    $hits = (int)$hits+1;
		        $mysqli->query("INSERT INTO `hits` (`user`, `item_id`) VALUES ('{$user}', '{$id}')");
			    $mysqli->query("UPDATE `posts` SET `hits`='{$hits}' WHERE `id`='{$id}'");
		};
	$res->close();
}

function meta_description($id) {
global $mysqli;
global $title;
    if(isset($id) && $id != '')
	{
        $res = $mysqli->query("SELECT `posts`.`text`, `posts`.`date` FROM `posts` WHERE `posts`.`link`='{$id}'");
	        $row = $res->fetch_assoc();
		       $date = new DateTime($row['date']);
		       $text = cutString($row['text'], 300);
		           echo $date->format('d.m.y - G:i').' | '.$text;
        $res->close();
	} else {
	echo 'Блог | '.$title.' | Обо всём по-немногу';
	}
}

function rus2translit($string) {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    );
    return strtr($string, $converter);
}

function str2url($str) {
    $str = rus2translit($str);
    $str = strtolower($str);
    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
    $str = trim($str, "-");
    return $str;
}

function admpass($pw) {
global $pass;
    if($pw == $pass || $_SESSION["admin"] == 1) {
        $_SESSION["admin"] = 1;
        echo '<script>
		        var e = $("#header").attr("id");
				alert(e);
				if(e == undefined)
				{
                    window.location.href = "'.$siteurl.'/admin";
				};
              </script>';
        include("templates/header.php");
        include("templates/content.php");
        include("templates/footer.php");
    } else {
        echo '<!DOCTYPE html>
        <html>
        <head>
            <title>Доступ запрещён!!!</title>
        </head>
        <body>
            <H3>Доступ запрещён!!!</H3>
            <b style="color: #777777;">Но быть может, вы знаете пароль?)</b>
            <br />
            <form method="POST" action="index.php">
                <input type="password" name="pass" />
                <input type="submit" value="Такой?" action="index.php" />
            </form>
        </body>
        </html>';
    }
}

function admquit($quit) {
global $siteurl;
    if(isset($quit) && $quit == 1) {
        $_SESSION["admin"] = 0;
        Header("Location: ".$siteurl."/admin");
    };
}

function showcats_adm() {
global $siteurl;
	echo '<li><a href="'.$siteurl.'/admin/index.php?do=sections">Редактировать разделы</a></li>
	      <li><a href="'.$siteurl.'/admin/index.php?do=posts">Редактировать статьи</a></li>
	      <li><a href="'.$siteurl.'">Вернуться на сайт</a></li>
	      <li><a href="'.$siteurl.'/admin/index.php?quit=1">Выйти</a></li>';
}

function links_adm($cat) {
global $siteurl;
$title = '<a href="'.$siteurl.'/admin">Admin</a>';
    if(isset($cat) && $cat != '')
	{
		switch($cat) {
                        case "sections":
                            $cattitle = 'Редактировать разделы';
                        break;

                        case "posts":
                            $cattitle = 'Редактировать статьи';
                        break;
                     }
		$title .= ' <img class="arrow" src="/templates/img/right.png" /> <a href="'.$siteurl.'/admin/index.php?do='.$cat.'">'.$cattitle.'</a>';
	};
echo $title;
}

function do_adm($do, $page) {
global $mysqli;
global $siteurl;
$content = '';
$script = '';
$script2 = '';
$textform = '';
$n = 5;
$i = 1;
$nocat = '<div style="height: 100px;"><b>Привет! Чтобы начать работу, воспользуйся меню! :)</b></div>';
    if(isset($do) && $do != '')
	{
	    switch($do) 
		    {
                case "sections":
				    $content .= addform();
                    $q = "SELECT * FROM `sections` ORDER BY `id` DESC";
                break;

                case "posts":
                    $q = "SELECT * FROM `posts` ORDER BY `id` DESC";
					$content .= addform();
                break;

                case "":
                    $content .= $nocat;
                break;
            }
		    $res = $mysqli->query($q);
				if(!$res->num_rows)
				{
				    $content .= '<br /><b>Вы ещё ничего не написали :(</b><br />';
				} else {
				    $cnt = $res->num_rows;
					if(isset($page) && $page != '')
                    {
		                $current = (int)$page;
					} else {
					    $current = 1;
						$page = 1;
					}
					$content .= '<H4>Редактировать</H4>
					             '.navlinks($cnt, $n, $page, $do).'
					             <form action="javascript:empty();">';
					$script .= '<script>';
					$script2 .= '$("#submit").click(function(){';
					if($do == "sections") $q2 = "SELECT * FROM `sections` ORDER BY `id` DESC LIMIT ".($page*$n - $n).",".$n;
					if($do == "posts") $q2 = "SELECT * FROM `posts` ORDER BY `id` DESC LIMIT ".($page*$n - $n).",".$n;
					$res2 = $mysqli->query($q2);
			        while($row = $res2->fetch_assoc())
				    {
					if($i != 1) $current = $current+1;
				    if($i == 1 || ($i-1)%$n == 0)
		            {
						$content .= '<ul class="allposts" id="posts-'.$current.'">';
			        };
				        $content .= '<li class="post" id="string-'.$row['id'].'">
				    	                <input id="check-'.$i.'" type="hidden" value="'.$row['id'].'" />
				    	                <a href="#del-'.$row['id'].'" id="del-'.$row['id'].'" style="color: #AA0000;">X</a>
				    	                <input class="newname" id="newname-'.$row['id'].'" type="text" name="newname" placeholder="Переименовать" />
				    					<a href="'.$siteurl.'/'.$row['link'].'/" style="font-weight: bold;">'.$row['title'].'</a>';
						if($do == "posts") $textform = '<br /><textarea class="newtext" id="newtext-'.$row['id'].'" placeholder="Редактировать">'.nl2br(isip($row['text'])).'</textarea>';
						$content .= $textform.'
				    				</li>';
				    	$script .= '$("#del-'.$row['id'].'").click(function(){
				    	                $("#delmsg").load("action.php?do='.$do.'&id='.$row['id'].'").fadeIn(500).delay(500).fadeOut(500);
				    					$("#string-'.$row['id'].'").fadeOut(500);
										$("#delmsg").empty();
				    				});';
						$script2 .= 'var name = $("#newname-'.$row['id'].'").val();
						             var str2 = $("#newtext-'.$row['id'].'").val();
									 if(str2 == undefined)
									 {
									     str = "";
									 } else {
									     str = str2.replace(" ", "-_-_-", str2);
									 }
						             if(name != null || name != "" || str != null || str != "")
						                 {
									         $("#delmsg").load("action.php?newname=" + name + "&check='.$row['id'].'&mode='.$do.'&text=" + str).fadeIn(500).delay(500).fadeOut(500);
										     $("#delmsg").empty();
									     };';
						if($i == 1 || $i%$n == 0 || $i == $cnt)
			            {
			                $content .= '</ul>';
			            };
			            $i = $i+1;
				    }
					$res->close();
					$res2->close();
			        $content .= '<input class="submit" id="submit" type="submit" value="Применить" />
						         </form>
			                     <div id="delmsg"></div>
								 <br />';
			        $script .= $script2.'setTimeout(function () {
                                             location.href = "'.$siteurl.'/admin/index.php?do='.$do.'"
                                         }, 1500);});</script>';
			    }
	echo $content.$script;
	echo '<script>$(".allposts").css("display", "none");
	          $("#posts-'.($page).'").delay(100).fadeIn("fast");
		  </script>
		  <br />';
	} else {
	    echo $nocat;
	}
}

function addform() {
include("templates/add.php");
}

function sip($txt) {
global $mysqli;
//$txt = $mysqli->real_escape_string($txt);
$txt = strip_tags($txt);
$txt = htmlspecialchars($txt);
$txt = stripslashes($txt);
$txt = addslashes($txt);
return $txt;
}

function isip($txt) {
$txt = htmlspecialchars_decode($txt);
$txt = stripslashes($txt);
return $txt;
}

function delete($id, $do) {
global $mysqli;
    if(isset($id) && $id != '' && isset($do) && $do != '')
    {
        $mysqli->query("DELETE FROM `{$do}` WHERE `id`='{$id}'");
	    switch($do) 
		    {
                case "sections":
                    $msg = 'Раздел удалён';
                break;

                case "posts":
                    $msg = 'Статья удалена';
                break;
            }
	    echo $msg;
    };
}

function update($name, $id, $mode, $text) {
global $mysqli;
	if(isset($name) && $name != '')
	{
		$link = str2url(sip($name));
		$mysqli->query("UPDATE `{$mode}` SET `title`='".sip($name)."', `link`='{$link}' WHERE `id`='{$id}'");
		echo 'Изменения сохранены';
	};

	if(isset($text) && $text != '')
	{
		$text = str_replace("-_-_-", " ", $text);
		$mysqli->query("UPDATE `{$mode}` SET `text`='".sip($text)."' WHERE `id`='{$id}'");
		echo 'Изменения сохранены';
	};
}

function add($title, $text, $do, $section) {
global $mysqli;
    if(isset($title) && $title != '')
    {
        $link = str2url(sip($title));
        $redirect = '<script>
                         location.href = "index.php?do='.$do.'";
	                 </script>';
            if(isset($text) && $text !='' && $text != 'undefined')
    	    {
    	    	$mysqli->query("INSERT INTO `posts` (`title`, `text`, `link`, `section`) VALUES ('".sip($title)."', '".sip($text)."', '{$link}', '{$section}')");
    	    	echo $redirect;
    	    } else {
    	        $mysqli->query("INSERT INTO `sections` (`title`, `link`) VALUES ('".sip($title)."','{$link}')");
    	    	echo $redirect;
	        }
    };
}

function select() {
global $mysqli;
    $res = $mysqli->query("SELECT * FROM `sections` ORDER BY `id`");
	if(!$res->num_rows)
	{
	    echo '<option disabled>Нет разделов</option>';
	} else {
	    echo '<option disabled>Выберите раздел</option>';
		while($row = $res->fetch_assoc())
		{
		    echo '<option value="'.$row['id'].'">'.$row['title'].'</option>';
		}
	}
}

function navlinks($cnt, $n, $pages, $do) {
global $mysqli;
global $siteurl;

$total = intval(($cnt-1)/$n)+1;
$pages = intval($pages);

    if($pages > $total) $pages = $total;
	
$start = $pages * $n - $n;
$prev = $pages - 1;
$nxet = $pages + 1;
$left1 = $pages - 1;
$left2 = $pages - 2;
$right1 = $pages + 1;
$right2 = $pages + 2;

    if($pages != 1)
    {
        $pervpage = '<a href="index.php?do='.$do.'&p=1" class="linkpage">Первая</a><a href="index.php?do='.$do.'&p='.$prev.'" class="linkpage">←</a>';
    };
    if($pages != $total)
    {
        $nextpage = '<a href="index.php?do='.$do.'&p='.$nxet.'" class="linkpage">→</a><a href="index.php?do='.$do.'&p='.$total.'" class="linkpage"">Последняя</a>';
    };
    if($pages - 2 > 0)
    {
        $page2left = '<a href="index.php?do='.$do.'&p='.$left2.'" class="linkpage">'.($pages - 2).'</a>';
    };
    if($pages - 1 > 0)
    {
        $page1left = '<a href="index.php?do='.$do.'&p='.$left1.'" class="linkpage">'.($pages - 1).'</a>';
    };
    if($pages + 2 <= $total)
    {
        $page2right = '<a href="index.php?do='.$do.'&p='.$right2.'" class="linkpage">'.($pages + 2).'</a>';
    };
    if($pages + 1 <= $total)
    {
        $page1right = '<a href="index.php?do='.$do.'&p='.$right1.'" class="linkpage">'.($pages + 1).'</a>';
    };
echo '<br />
     <div class="pagelinks">
	 <b>Страницы: </b>
	 <b class="linkpage"></b>
	 '.$pervpage.$page2left.$page1left.'
	 <b class="nolinkpage">'.$pages.'</b>
	 '.$page1right.$page2right.$nextpage.'
	 <b class="linkpage"></b>
	 </div>
	 <br />';
}

?>