﻿<!DOCTYPE html>
<html>
<head>
    <title>Prosto Blog | Админ-панель</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="description" content="Prosto Blog | Админ-панель">
	<link rel="shortcut icon" href="<? echo $siteurl;?>/templates/img/logo.jpg" type="image/x-icon" />
    <link rel="icon" href="<? echo $siteurl;?>/templates/img/logo.jpg" type="image/x-icon" />
    <link rel="stylesheet" href="<? echo $siteurl;?>/templates/style.css" type="text/css; charset=utf-8">
	<script type="text/javascript" src="<? echo $siteurl;?>/includes/jquery-1.10.2.min.js"></script>
</head>
<body>
    <div id="all">
        <div id="header">
		    <a href="<? echo $siteurl;?>/admin/">
                <img id="logo" src="/templates/img/logo.jpg">
                <H1>Prosto Blog | Админ-панель</H1>
			</a>
            <b class="ann" style="color: #AA0000;">Привет! Что напишем сегодня? :)</b>
        </div>
		<div id="nav"><? links_adm($_GET['do']);?></div>
        <div id="content">