--
-- Структура таблицы `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `hits` int(11) NOT NULL,
  `whenadd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `cat`, `title`, `url`, `description`, `keywords`, `hits`, `whenadd`) VALUES
(1, 1, 'Введение', 'vvedenie', 'Введение в язык PHP', 'php, введение, для начинающих', 2, '2014-07-01 04:55:47'),
(2, 1, 'Синтаксис языка', 'sintaksis-yazika', 'Синтаксис язык PHP...', 'php, синтаксис, введение, для начинающих', 1, '2014-07-01 04:55:47'),
(3, 1, 'Логические конструкции', 'logicheskie-konstrukcii', 'Логические конструкции...', 'php, логические конструкции, введение, для начинающих', 1, '2014-07-01 04:59:02'),
(4, 1, 'Первая программа', 'pervaya-programma', 'Hello, world!', 'php, введение, для начинающих, hello world', 0, '2014-07-01 04:59:02'),
(5, 3, 'Нумерованные массивы', 'numerovannye massivy-2', 'Нумерованные массивы', 'php, массивы, нумерованный массив', 0, '2014-07-01 05:00:44'),
(7, 4, 'while', 'while', 'while', 'php, циклы, while', 1, '2014-07-01 05:01:28'),
(8, 4, 'for', 'for', 'for', 'php, циклы, for', 0, '2014-07-01 05:01:28'),
(9, 4, 'foreach', 'foreach', 'foreach', 'php, циклы, foreach', 2, '2014-07-01 05:02:25'),
(10, 4, 'do while', 'do while-2', 'do while', 'php, циклы, do while', 1, '2014-07-01 05:02:25'),
(18, 3, 'Ассоциативные массивы', 'associativnye massivy-2', '<p>Введите текст</p>\r\n', 'php, массивы, индексы, циклы, ассоциативный массив', 0, '2014-07-03 14:29:04'),
(19, 1, 'lol', 'lol-2', '<pre class="brush:php;">\r\necho &#39;lol&#39;;</pre>\r\n\r\n<p>&nbsp;</p>\r\n', 'фыв', 1, '2014-07-03 14:59:31');

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `parent`, `title`, `url`) VALUES
(1, 0, 'PHP', 'php'),
(2, 1, 'Строковые функции', 'strokovie-funkcii'),
(3, 1, 'Массивы', 'massivi'),
(4, 1, 'Циклы', 'cikli');

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `comment` longtext NOT NULL,
  `whenadd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
