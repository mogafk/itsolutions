﻿// Шаг для ajax-загрузки статей на главной странице
var page = 0;

// Выполняется или нет в данный момент ajax-запрос
var inProgress = false;

$(document).ready(function() {
	
	// Текущая страница
	var loc = document.location.href;
	var loc2 = loc.split("/");
	
	// Дата для футера
    var cur = new Date();
	cur.getFullYear() == 2014 ? $('#copy_date').text('2014') : $('#copy_date').text('2014 - ' + cur.getFullYear());

	var cur = $("#spoilerlink-" + loc2[4]).attr("data-parent");

	// Подсвечиваем текущий пункт меню
	$("#" + loc2[4] + "-spoiler").css({"display" : "block", "opacity" : 1.0});
	$("#" + loc2[4] + "-spoiler").parents(".menuspoler").css({"display" : "block", "opacity" : 1.0});
	$("#spoilerlink-" + loc2[4]).attr("data-pos", 1);

	if(cur != 0)
	{
	    // Подсвечиваем текущий пункт меню
	    $("#" + cur + "-spoiler").css({"display" : "block", "opacity" : 1.0});
	    $("#" + cur + "-spoiler").parents(".menuspoler").css({"display" : "block", "opacity" : 1.0});
		$("#" + loc2[4] + "-spoiler").css({"display" : "block", "opacity" : 1.0});
		$("#" + loc2[4] + "-spoiler").parents(".menuspoler").css({"display" : "block", "opacity" : 1.0});
	    $("#spoilerlink-" + loc2[4]).attr("data-pos", 1);
	};

	if(loc2.length == 8)
	{
	    $("#" + loc2[4] + "-spoiler .articlelist li a").each(function(i) {
	        if($(this).attr("href") == "/" + loc2[3] + "/" + loc2[4] + "/" + loc2[5] + "/" + loc2[6] + "/")
	    	{
	    	    $(this).css({"color" : "#AA0000"});
	    	};
        });
		
		$("#commentform").attr("action", document.location.href);
		
	    $.ajax({
            type: 'POST',
            url: '/ajax/',
            data: 'article=' + loc2[6],
            success: function(data){
				$(".mainwrapper").prepend(data);
            }
        });
		
		home_articles(page, 'comment');
	};
    
	if(loc2.length == 6)
	{
	    $(".spoilerlink").each(function(i) {
	        if($(this).attr("data-url") == loc2[4])
	    	{
	    	    $(this).css({"color" : "#AA0000"});
				home_articles(page, 'cat');
	    	};
        });
		
        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() >= $(document).height() - 200 && !inProgress) {
	    	    home_articles(page, 'cat');
	    	};
	    });
	};

	// Статьи на главной
	if(loc2.length == 4)
	{
		home_articles(page);
		
        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() >= $(document).height() - 200 && !inProgress) {
	    	    home_articles(page);
	    	};
	    });
	};
	
	// Анимация навигационного меню
	$(".spoilerlink").click(function() {
	    var url = $(this).attr("data-url");
		var pos = $(this).attr("data-pos");
		var parent = $(this).attr("data-parent");
		
		if(pos == 0)
		{
		    if(parent == "0")
			{
			    $(".spoilerlink").attr("data-pos", 0);
				$(".menuspoler").animate({"opacity" : 0.7},250).slideUp(250);
			    $("#" + url + "-spoiler").slideDown(250).animate({"opacity" : 1.0},250);
			    $(this).attr("data-pos", 1);
			} else {
			    $(".childspoilerlink").attr("data-pos", 0);
				$(".childspoiler").animate({"opacity" : 0.7},250).slideUp(250);
			    $("#" + url + "-spoiler").slideDown(250).animate({"opacity" : 1.0},250);
			    $(this).attr("data-pos", 1);
			}
		} else {
		    if(parent == "0")
			{
			    $("#" + url + "-spoiler").animate({"opacity" : 0.7},250).slideUp(250);
			} else {
			    $("#" + url + "-spoiler").animate({"opacity" : 0.7},250).slideUp(250);
			}
			$(this).attr("data-pos", 0);
		}
		return false;
	});
	
	$(".lastcommentssploilerlink").click(function() {
	    var id = $(this).attr("data-id");
		var pos = $(this).attr("data-pos");
		
		if(pos == 0)
		{
		    $("#lastcommentssploiler-" + id).slideDown(250).animate({"opacity" : 1.0},250);
			$(this).attr("data-pos", 1);
		} else {
		    $("#lastcommentssploiler-" + id).animate({"opacity" : 0.7},250).slideUp(250);
			$(this).attr("data-pos", 0);
		}
		return false;
	});
	
	$("#search_submit_btn").click(function() {
	    if($("#search").val().length > 2) $("#search_form").submit();
	});
	
	$("#search_submit_btn2").click(function() {
	    if($("#search2").val().length > 2) $("#search_form2").submit();
	});
	
	$(".searchkeywordslink, .tagkeywordslink").click(function() {
	    $("#search").val($(this).attr("data-keyword"));
		if($("#search").val().length > 2) $("#search_form").submit();
		return false;
	});
	
	$("#submit_comment").click(function() {
	    if($("#name").val().length > 2 && $("#email").val().length > 2 && $("#comment").val().length > 2) $("#commentform").submit();
	});
	
	$('#comment').keypress(function(event){
        var keyCode = event.keyCode ? event.keyCode :
        event.charCode ? event.charCode :
        event.which ? event.which : void 0;
    
	    if((event.ctrlKey) && ((event.keyCode == 0xA)||(event.keyCode == 0xD)))
        {
	        if($("#name").val().length > 2 && $("#email").val().length > 2 && $("#comment").val().length > 2) $("#commentform").submit();
        }
	});
	
    if(!$('#myCanvas').tagcanvas({
      textFont: null,
	  textColour: null,
	  weight: true,
      reverse: true,
	  wheelZoom: true,
	  outlineMethod: 'colour',
      outlineColour: '#F7F7F7',
      depth: 0.8,
	  fadeIn: 800,
	  shadowBlur: 2,
	  shuffleTags: true,
      maxSpeed: 0.05,
	  initial: [0.1, -0.1]
    },'tags')) {
      // something went wrong, hide the canvas container
      $('#myCanvasContainer').hide();
    }
	
	$("#search_submit_btn, #search_submit_btn2, #sendtopic_submit_btn, #submit_comment").hover(function() {
	    $(this).animate({"opacity" : 1.0},250);
	},function() {
	    $(this).animate({"opacity" : 0.7},250);
	});
	
	$(".lastcommentssploilerlink").hover(function() {
	    $('img',this).animate({"opacity" : 1.0},250);
	},function() {
	    $('img',this).animate({"opacity" : 0.7},250);
	});
	
});

function home_articles(page, mode) {
	var hh = "";
	if(mode == 'cat' || mode == 'comment')
	{
	    var loc3 = document.location.href;
	    var loc4 = loc3.split("/");
		mode == 'cat' ? hh = loc4[4] : hh = loc4[6];
	} else {
	    hh = 'home';
	}
	
	var params = 'home_articles_step=' + hh + '-0-0-' + page;
	
	if(mode == 'comment') params = params + '-0-0-comm';
	
	$.ajax({
        type: 'POST',
        url: '/ajax/',
        data: params,
        beforeSend: function() {
            window.inProgress = true;
		},
        success: function(data){
			if(mode == 'comment')
			{
			    $("#comments").append(data);
			} else {
			    $(".mainwrapper").append(data);
			}
			window.page++;
			window.inProgress = false;
        }
    });
}

function escapeHtml(text) {
  return text
      .replace(/&/g, "-0-0-")
      .replace(/</g, "-1-1-")
      .replace(/>/g, "-2-2-")
      .replace(/"/g, "-3-3-")
      .replace(/'/g, "-4-4-");
}

function sendtopic1() {
	var txt = $("#sendtopic").val();
	if(txt != '')
	{
	    $.ajax({
            type: 'POST',
            url: '/ajax/',
            data: 'txt=' + escapeHtml(txt), 
            success: function(data){
                $("#sendtopic").val('');
				$("#sendtopic_success").slideDown(250).animate({"opacity" : 1.0},250);
				setTimeout(function() {
				    $("#sendtopic_success").animate({"opacity" : 0.5},250).slideUp(250);
				},1500);
            }
        });
	};
	return false;
}