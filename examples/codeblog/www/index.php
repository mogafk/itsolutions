<?php
session_start(); // Открываем сессии до любого вывода текста на экран. Файл строго в кодировке UTF-8 без BOM
include('includes/config.php'); // Подключаем файл настроек сайта
include('includes/mainfile.php'); // Подключаем "конструтор" страниц

// Если это админ-панель, то:
if($_GET['controller'] == 'admin')
{
	if(isset($_GET['exit']) && $_GET['exit'] == 'yes')
	{
	    setcookie('admin', '', 1);
		session_destroy();
		echo '<script>document.location.href = "/admin/";</script>';
	} else {
	    if(isset($_COOKIE['admin']) && trim($_COOKIE['admin']) == "1")
	    {
	        setcookie('admin', '1', time()+600);
	    	include('templates/'.ADMIN_TEMPLATE.'/admin.php');
	    } else {
	    	if(!empty($_POST))
	        {
	            if(trim($_POST['adminlogin']) == ADMIN_LOGIN && trim($_POST['adminpass']) == ADMIN_PASS)
	    		{
	    		    setcookie('admin', '1', time()+600);
	    			echo '<script>document.location.href = "/admin/";</script>';
	    		} else {
	    		    echo '<script>document.location.href = "/admin/";</script>';
	    		}
	        };
	    	include('templates/'.ADMIN_TEMPLATE.'/login.php');
	    }
	}
	$ok = 1;
};
?>