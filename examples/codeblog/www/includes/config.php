<?php
// Настройки доступа к базе данных
define('DB_LOCATION', 'localhost'); // Адрес сервера баз данных
define('DB_NAME', 'codeblog'); // Имя базы данных
define('DB_USER', 'root'); // Имя пользователя базы данных
define('DB_PASSWORD', ''); // Пароль к базе данных

// Настройки сайта
define('SITENAME', 'codeBlog'); // Имя сайта
define('COPYRIGHT', 'Камалова Рима'); // Имя в футере
define('ARTICLES_PER_PAGE', 5); // Кратких статей на главной и странице категории
define('ARTICLES_LAST', 5); // Ссылок на статьи для блока "Последние статьи"
define('ARTICLES_POP', 5); // Ссылок на статьи для блока "Популярные статьи"
define('COMMENTS_LAST', 10); // Ссылок на комментарии в блоке "Последние комментарии"
define('ADVERTISMENT', 'off'); // Показывать рекламу? on - вкл., иначе - выкл.
define('TEMPLATE', 'default'); // Шаблон сайта
define('LOGO', '/templates/'.TEMPLATE.'/images/logo.png'); // Логотип сайта
define('SITEURL', $_SERVER['SERVER_NAME']); // Адрес сайта. Не менять.

// Настройки админ-панели
define('ADMIN_TEMPLATE', 'admin'); // Шаблон админ-панели
define('ADMIN_LOGIN', 'admin'); // Логин админ-панели
define('ADMIN_PASS', 'admin'); // Пароль админ-панели
define('ADMIN_EMAIL', 'admin@admin.ru'); // E-mail администратора для получения предложений тем для статей на почту
?>