<?php
// Подключаем базу данных
$mysqli = new mysqli(DB_LOCATION, DB_USER, DB_PASSWORD, DB_NAME);
// Задаём "хорошую" кодировку для работы с базой данных
$mysqli->set_charset("utf8");

// Проверяем, произошло ли подключение
if ($mysqli->connect_errno)
{
    // Если ошибка - выдаём сообщение
	printf("Ошибка подключения к БД <b>".DB_NAME."</b>: %s\n", $mysqli->connect_error);
    exit();
}

// Подключаем функции, необходимые для работы сайта
include('includes/functions.php');

/* Задаём контроллеры для конструктора страниц и их названия(для навигационной панели) в виде массивов.
 * При этом первый - нумерованный, с начальным индексом 0, второй - ассоциативный.
 *
 * Пример:
 * http://mysite.ru/category/testovaya-categoriya/article/testovaya-statya/ - ссылка на статью.
 *
 * Category - контроллер, testovaya-categoriya - URL-адрес категории, article - экшн, testovaya-statya - URL-адрес статьи
 *
 */
$controllers = array('home', 'category', 'search', 'contacts', 'admin', 'ajax', 'error');
$controllers_name = array('home' => 'Главная', 
                          'category' => 'Раздел', 
						  'search' => 'Поиск', 
						  'admin' => 'Админ-панель', 
						  'ajax' => 'AJAX', 
						  'error' => 'Ошибка!');
// Обнуляем переменные перед работой с ними
$controller = ""; // Контроллер
$action = ""; // Экшн. Есть только у страниц статей
$category = ""; // URL категории
$item = ""; // URL статьи
$cat = array(); // Информация о категории
$article = array(); // Информация о статье
$ok = 0;

// Получаем параметры из адресной строки, в соответствии с ними формируем нашу страницу
(isset($_GET['controller']) && trim($_GET['controller']) != '') ? $controller = mb_strtolower($_GET['controller']) : $controller = 'home';
if(isset($_GET['action']) && trim($_GET['action']) != '') $action = mb_strtolower($_GET['action']);
if(isset($_GET['category']) && trim($_GET['category']) != '')
{
    $category = mb_strtolower($_GET['category']);
	unset($cat);
	$cat = category_by_url($category);
};

if(isset($_GET['item']) && trim($_GET['item']) != '')
{
    $item = mb_strtolower($_GET['item']);
	unset($article);
	$article = article_by_url($item);
}

// Если это админ-панель, то:
if($controller == $controllers[4])
{
	if(!empty($_POST) && isset($_COOKIE['admin']) && trim($_COOKIE['admin']) == "1" && it($_POST) == true)
	{
	    if(isset($_POST['catname']))
		{
		    $mysqli->query("INSERT INTO `categories` (`parent`, `title`, `url`) VALUES ('".sip($_POST['parent'])."', '".sip($_POST['catname'])."', '".str2url(sip($_POST['catname']))."');");
		};
		
	    if(isset($_POST['article_name']))
		{
		    $mysqli->query("INSERT INTO `articles` (`cat`, `title`, `url`, `description`, `keywords`) VALUES ('".sip($_POST['article_cat'])."', '".sip($_POST['article_name'])."', '".str2url(sip($_POST['article_name']))."', '".$_POST['description']."', '".sip($_POST['meta_keywords'])."');");
		};
		
		if(isset($_POST['catname2']))
		{
		    $mysqli->query("UPDATE `categories` SET `title`='".sip($_POST['catname2'])."', `url`='".str2url(sip($_POST['catname2']))."', `parent`='".sip($_POST['parent2'])."' WHERE `id`='".sip($_POST['item'])."'");
		};
		
		if(isset($_POST['article_name2']))
		{
		    $mysqli->query("UPDATE `articles` SET `title`='".sip($_POST['article_name2'])."', `url`='".str2url(sip($_POST['article_name2']))."', `cat`='".sip($_POST['article_cat2'])."', `keywords`='".sip($_POST['meta_keywords2'])."', `description`='".$_POST['description2']."' WHERE `id`='".sip($_POST['item2'])."'");
		};
	    echo '<script>document.location.href = "/admin/";</script>';
	};
	
	if(isset($_GET['delete']) && isset($_COOKIE['admin']) && trim($_COOKIE['admin']) == "1" && it($_GET) == true)
	{
	    switch($_GET['type'])
		{
		    case('comment'):
			                $q = "DELETE FROM `comments` WHERE `id`='".sip($_GET['delete'])."'";
							break;
		    case('article'):
			                $q = "DELETE FROM `comments` WHERE `item`='".sip($_GET['delete'])."'";
							$q2 = "DELETE FROM `articles` WHERE `id`='".sip($_GET['delete'])."'";
							break;
		    case('cat'):
							$res = $mysqli->query("SELECT `id` AS id FROM `articles` WHERE `cat`='{$cat}'");
							while($id = $res->fetch_object()->id)
							{
							    $mysqli->query("DELETE FROM `comments` WHERE `item`='{$id}'");
							}
							$res->close();
							$q = "DELETE FROM `articles` WHERE `cat`='".sip($_GET['delete'])."'";
			                $q2 = "DELETE FROM `categories` WHERE `id`='".sip($_GET['delete'])."'";
							break;
		}
	    $mysqli->query($q);
		if(isset($q2) && trim($q2) != '') $mysqli->query($q2);
	    echo '<script>document.location.href = "/admin/";</script>';
	};
	
	$ok = 1;
};

// Заполняем массив полученной выше информацей о странице.
$pageinfo = array('controller' => $controller,
                  'controllers' => $controllers,
				  'controllers_name' => $controllers_name,
				  'action' => $action,
				  'category' => $category,
				  'item' => $item,
				  'catinfo' => $cat,
				  'article' => $article);

if($action == 'article' && $item != '' && (int)$_COOKIE[$item] != 1)
{
    $res = $mysqli->query("SELECT `hits` AS hits FROM `articles` WHERE `url`='{$item}'");
	$mysqli->query("UPDATE `articles` SET `hits`='".((int)$res->fetch_object()->hits + 1)."' WHERE `url`='{$item}'");
	setcookie($item, 1);
	$res->close();
};

// Проверяем, какой контроллер пришёл. В зависимости от него, выбираем шаблон и генерируем страницу.
// Если это, что видит посетитель сайта, то:
if($controller == $controllers[0] || $controller == $controllers[1] || $controller == $controllers[2] || $controller == $controllers[3])
{
	trim($action) != '' ? $page = $action : $page = $controller;
	include('templates/'.TEMPLATE.'/header.php');
	if(($controller != 'home' && $controller != 'category') || $action != '') include('templates/'.TEMPLATE.'/'.$page.'.php');
	include('templates/'.TEMPLATE.'/footer.php');
	$ok = 1;
};

// Контроллер обработки AJAX-запросов
if($controller == $controllers[5] && !empty($_POST))
{
    $healthy = array("{title}", "{hits}", "{whenadd}", "{description}", "{keywords}", "{url}", "{similar}", "{comments}", "{small_description}", "{image}");
	$template = file_get_contents('templates/'.TEMPLATE.'/article.html');
	$articles = "<script type='text/javascript'>
                 function search1(keyword) {
					var elem = document.getElementById('search');
					var form = parent.document.getElementById('search_form');
					elem.value = keyword;
                 	if(elem.value.length > 2) form.submit();
                 	return false;
                 }
	             </script>";
	$keywords = "";
	$similar = "";
	if(isset($_POST['home_articles_step']) && trim($_POST['home_articles_step']) != '')
	{
		$hh = explode("-0-0-", $_POST['home_articles_step']);
		$hh[0] == 'home' ? $mode = 'more' : $mode = 'cat';
		if(isset($hh[2]) && trim($hh[2]) == 'comm')
		{
		    $mode = 'comment';
			$articles = '';
            $healthy = array("{name}", "{comment}", "{whenadd}");
	        $template = file_get_contents('templates/'.TEMPLATE.'/comment.html');
			$comment = articles($mode, $hh[0].'-0-0-'.$hh[1]);
			if(count($comment) > 0)
			{
		        foreach($comment as $key => $val)
		        {
			        $yummy = array($val['name'], $val['comment'], $val['whenadd']);
			    	$articles .= str_replace($healthy, $yummy, $template);
			    }
			} else {
			    $articles = '<script type="text/javascript">
				                 var elem = parent.document.getElementById("comments");
							     elem.innerHTML = "Комментариев к данной статье не найдено.";
							 </script>';
			}
		} else {
		    foreach(articles($mode, $hh[0].'-0-0-'.$hh[1]) as $key => $val)
		    {
                $keywords = '<ul class="searchkeywords">';
    	    	foreach($val['keywords'] as $key2 => $val2)
    	    	{
    	    	    $keywords .= '<li><a class="searchkeywordslink" href="#" data-keyword="'.$val2.'" OnClick=\'javascript:search1("'.$val2.'");\'>'.$val2.'</a></li>';
    	    	}
    	    	$keywords .= '</ul>';
		    	
		    	$yummy = array($val['title'], $val['hits'], $val['whenadd2'], croptext($val['description'], 500), $keywords, 'http://'.SITEURL.'/category/'.$val['cat']['url'].'/article/'.$val['url'].'/', $similar, $val['comments'], croptext($val['description'], 200), LOGO);
		    	$articles .= str_replace($healthy, $yummy, $template);
		    }
		}
		echo $articles;
	};
	if(isset($_POST['article']) && trim($_POST['article']) != '')
	{
		$template = str_replace('<span class="lastarticlestime" style="float: right; font-size: 8px !important; margin-top: 0px;"><a href="{url}">Читать полностью</a><br />Комментарии: {comments}</span>', '', $template);
		$val = article_by_url($_POST['article']);
		
        $keywords = '<ul class="searchkeywords">';
    	foreach($val['keywords'] as $key2 => $val2)
    	{
    	    $keywords .= '<li><a class="searchkeywordslink" href="#" data-keyword="'.$val2.'" OnClick=\'javascript:search1("'.$val2.'");\'>'.$val2.'</a></li>';
    	}
    	$keywords .= '</ul>';
		
		$yummy = array($val['title'], $val['hits'], $val['whenadd2'], $val['description'], $keywords, 'http://'.SITEURL.'/category/'.$val['cat']['url'].'/article/'.$val['url'].'/', $similar, $val['comments'], croptext($val['description'], 200), LOGO);
		$articles .= str_replace($healthy, $yummy, $template);
		echo $articles;
	};
	if(isset($_POST['txt']) && trim($_POST['txt']) != '')
	{
	    echo sendtopic(sip($_POST['txt']));
	};
	$ok = 1;
};

// Если ни один вариант из перечисленных не подошёл, делаем вывод, что такой страницы не существует, и показываем ошибку.
if($controller == $controllers[6] || $ok == 0)
{
	include('templates/'.TEMPLATE.'/header.php');
	include('templates/'.TEMPLATE.'/error.php');
	include('templates/'.TEMPLATE.'/footer.php');
};

// Обрабатываем данные из формируе
// Если данные пришли
if(!empty($_POST))
{
	// Если отправлен комментарий
	if(isset($_POST['comment']))
	{
        // Проверяем на пустоту
		if(it($_POST) == true)
	    {
			// Добавляем комментарий в базу данных
			$mysqli->query("INSERT INTO `comments`(
	    	                                        `item`, 
	    											`name`, 
	    											`email`, 
	    											`comment`
	    								) VALUES (
	    								            '{$pageinfo['article']['id']}', 
	    											'".sip($_POST['name'])."', 
	    											'".sip($_POST['email'])."', 
	    											'".sip($_POST['comment'])."'
	    								);");
			// Выдаём всплывающее сообщение об успешном итоге операции
			echo '<script type="text/javascript">
			          var elem = document.getElementById("alert_success");
					  var opcty = 0.50;
					  var hght = 0;
					  
					  elem.style.display = "block";
					  
					  var p = setInterval(function() {						  
						  if(hght < 16)
						  {
						      hght += 1;
							  elem.style.height = hght + "px";
						  } else {
						      opcty += 0.05;
							  elem.style.opacity = opcty;
						  }
						  
						  if(hght >= 16 && opcty >= 1.00)
						  {
						      clearInterval(p);
							  setTimeout(function() {
							      var o = setInterval(function() {
						              if(opcty > 0.50)
						              {
						                  opcty -= 0.05;
							              elem.style.opacity = opcty;
						              } else {
										  hght -= 1;
							              elem.style.height = hght + "px";
						              }
									  
									  if(opcty <= 0.50 && hght <= 0)
									  {
									      clearInterval(o);
										  elem.style.display = "none";
									  }
								  },10);
							  },1000);
						  };
					  },10);
			      </script>';
	    } else {
			// Выдаём всплывающее сообщение о неправильно заполненных полях
			echo '<script type="text/javascript">
			          var elem = document.getElementById("alert_error");
					  var opcty = 0.70;
					  var hght = 0;
					  
					  elem.style.display = "block";
					  
					  var p = setInterval(function() {						  
						  if(hght < 16)
						  {
						      hght += 1;
							  elem.style.height = hght + "px";
						  } else {
						      opcty += 0.05;
							  elem.style.opacity = opcty;
						  }
						  
						  if(hght >= 16 && opcty >= 1.00)
						  {
						      clearInterval(p);
							  setTimeout(function() {
							      var o = setInterval(function() {
						              if(opcty > 0.70)
						              {
						                  opcty -= 0.05;
							              elem.style.opacity = opcty;
						              } else {
										  hght -= 1;
							              elem.style.height = hght + "px";
						              }
									  
									  if(opcty <= 0.70 && hght <= 0)
									  {
									      clearInterval(o);
										  elem.style.display = "none";
									  }
								  },10);
							  },1000);
						  };
					  },10);
			      </script>';
	    }
	};
};
?>