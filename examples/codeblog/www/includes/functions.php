<?php
/* "Хлебные крошки"
 *
 * @param array $page - Массив с данными о странице
 * @param string $arrow - Картинка стрелки
 * @return string $bread - Функция возвращает строку с готовым шаблоном "хлебных крошек" для текущей страницы
 *
 */
function breadcrumbs($page, $arrow) {
	// Проверяем контроллеры, в которых нет входных данных кроме $controller
	if($page['controller'] == $page['controllers'][0] || $page['controller'] == $page['controllers'][2] || $page['controller'] == $page['controllers'][3] || 
    $page['controller'] == $page['controllers'][6])
    {
        // Формируем ссылку. Помним, что контроллер home означает главную страницу
		$page['controller'] == 'home' ? $link = '' : $link = $page['controller'].'/';
    	$bread = '<li><a href="/'.$link.'" class="activenav">'.$page['controllers_name'][$page['controller']].'</a></li>';
    };
    
	// Если задействован контроллер категорий, ссылка будет другая
    if($page['controller'] == $page['controllers'][1])
    {
        // Обязательно проверяем наличие URL категории
		if($page['category'] != '')
    	{
			// Если ссылка только на категорию
			if($page['action'] == '')
			{
			    $bread = '<li><a href="/'.$page['controller'].'/'.$page['category'].'/" class="activenav">'.$page['catinfo']['title'].'</a></li>';
			} else {
			    // Если ссылка ведёт на страницу статьи
				$item['title'] = 'Название статьи'; // Тестовое значение
				$bread = '<li><a href="/'.$page['controller'].'/'.$page['category'].'/">'.$page['catinfo']['title'].'</a></li>
						  <li> <img src="'.$arrow.'" /> </li>
    		    	      <li><a href="/'.$page['controller'].'/'.$page['category'].'/'.$page['action'].'/'.$page['item'].'/" class="activenav">'.$page['article']['title'].'</a></li>';
			}
    	};
    }
	// Выводим полученную строку в шаблон.
	echo $bread;
}

/*
 * Информация об одной категории по её URL
 *
 * @param string $url - URL категории
 *
 * @return array $cat - Информация о статье в массиве
 */
function cat_by_url($url) {
    global $mysqli;
	
	$res = $mysqli->query("SELECT * FROM `categories` WHERE `url`='{$url}'");
	while($row = $res->fetch_assoc())
	{
	    $cat = $row;
		$cat['articles'] = articles('cat', $row['id']);
	}
	$res->close();
	return $cat;
}

/*
 * Получаем информацию о статьях
 *
 * @param string $mode - По какому принципу делаем выборку(5 свежих, 5 популярных, из категории)
 * @param string $cat - ID категории
 * @return array $articles - Возвращаем массив с информацией о статьях
 *
 */
function articles($mode, $cat="") {
    global $mysqli;
	$i = 0;
	
	// Выбираем вариант, по которому будем составлять запрос
	switch($mode)
	{
	    case('last'):
		                $q = "SELECT `id` AS id FROM `articles` ORDER BY `whenadd` DESC LIMIT 0,".ARTICLES_LAST;
						break;
	    case('pop'):
		                $q = "SELECT  `id` AS id FROM `articles` ORDER BY `hits` DESC LIMIT 0,".ARTICLES_POP;
						break;
	    case('cat'):
		                // "Разрубаем" строку на массив
						$cat_parts = explode("-0-0-", $cat);
						(int)$cat_parts[1] == 0 ? $start = 0 : $start = (int)$cat_parts[1]*ARTICLES_PER_PAGE;
						$itcat = category_by_url($cat_parts[0]);
						$q = "SELECT `id` AS id FROM `articles` WHERE `cat`='{$itcat['id']}' ORDER BY `whenadd` DESC LIMIT ".$start.", ".ARTICLES_PER_PAGE;
						break;
	    case('nav'):
		                $q = "SELECT `id` AS id FROM `articles` WHERE `cat`='{$cat}' ORDER BY `title`";
						break;
	    case('more'):
		                // "Разрубаем" строку на массив
						$cat_parts = explode("-0-0-", $cat);
						// Запрос будет меняться в зависимости от того, на какой мы странице
						$cat_parts[0] == 'home' ? $wh = "" : $wh = " WHERE `cat`='{$cat_parts[0]}'";
						(int)$cat_parts[1] == 0 ? $start = 0 : $start = (int)$cat_parts[1]*ARTICLES_PER_PAGE;
						$q = "SELECT `id` AS id FROM `articles`{$wh} ORDER BY `whenadd` DESC LIMIT ".$start.", ".ARTICLES_PER_PAGE;
						break;
	    case('comment'):
		                // "Разрубаем" строку на массив
						$cat_parts = explode("-0-0-", $cat);
						// Запрос будет меняться в зависимости от того, на какой мы странице
						$art = article_by_url($cat_parts[0]);
						$wh = " WHERE `item`='{$art['id']}'";
						(int)$cat_parts[1] == 0 ? $start = 0 : $start = (int)$cat_parts[1]*ARTICLES_PER_PAGE;
						$q = "SELECT * FROM `comments`{$wh} ORDER BY `whenadd` DESC LIMIT ".$start.", ".ARTICLES_PER_PAGE;
						break;
	}
	
	// Отправляем запрос в базу данных
	$res = $mysqli->query($q);
	if($mode == 'comment')
	{
	    $i = 0;
		while($row = $res->fetch_assoc())
		{
		    $articles[$i] = $row;
			$articles[$i]['whenadd'] = rusdate($row['whenadd'], 1);
			$i++;
		}
		$res->close();
	} else {
	    // Обрабатываем результат запроса с помощью цикла. Каждая строка будет представлена в виде ассоциативного массива,
	    // индексами которого будут названия полей в бд.
	    while($id = $res->fetch_object()->id)
	    {
	        // Записываем информацию в массив
	    	$articles[$i] = article($id, $mode);
	    	$i++;
	    }
	    $res->close();
	    // Возвращаем значение
	}
	return $articles;
}

/*
 * Информация об одной категории по её URL
 *
 * @param string $url - URL категории
 *
 * @return array $category - Информация о статье в массиве
 */
function category_by_url($url) {
    global $mysqli;
	
	$res = $mysqli->query("SELECT * FROM `categories` WHERE `url`='{$url}'");
	$cnt = $res->num_rows;
	if($cnt > 0)
	{
	    while($row = $res->fetch_assoc())
	    {
	        $category = $row;
	    }
	    $res->close();
	    return $category;
	};
}

/*
 * Информация об одной статье по её URL
 *
 * @param string $url - URL статьи
 *
 * @return array $article - Информация о статье в массиве
 */
function article_by_url($url) {
    global $mysqli;
	
	$res = $mysqli->query("SELECT `id` AS id FROM `articles` WHERE `url`='{$url}'");
	$article = article($res->fetch_object()->id);
	$res->close();
	return $article;
}

/*
 * Информация об одной статье по её ID
 *
 * @param string $id - ID статьи
 * @param string $mode - В каком блоке отображаем(необяз.)
 *
 * @return array $article - Информация о статье в массиве
 */
function article($id, $mode="") {
    global $mysqli;
	$i = 0;
	
	$res = $mysqli->query("SELECT * FROM `articles` WHERE `id`='{$id}'");
	while($row = $res->fetch_assoc())
	{
	    $i = 0;
		// Записываем информацию в массив
		$article = $row;
		// Делаем "красивую" дату
	    $article['whenadd'] = rusdate($row['whenadd']);
		$article['whenadd2'] = rusdate($row['whenadd'], 1);
	    // Ограничиваем размер названия, если массив будет выведен в блоках "последние" или "популярные
	    if($mode == 'last' || $mode == 'pop')
	    {
	        if(mb_strlen($row['title'], 'UTF-8') > 29) $article['title'] = mb_substr($row['title'], 0, 29, 'UTF-8').'...';
	    };
		
	    unset($article['keywords']);
		$keywords = explode(",", $row['keywords']);
		foreach($keywords as $key => $val)
		{
		    $article['keywords'][$i] = trim($val);
			$i++;
		}
		
		unset($article['hits']);
		if((int)$row['hits'] == 1) $article['hits'] = $row['hits'].' просмотр';
		if((int)$row['hits'] >= 2 && (int)$row['hits'] <= 4) $article['hits'] = $row['hits'].' просмотра';
		if((int)$row['hits'] > 4 || (int)$row['hits'] == 0) $article['hits'] = $row['hits'].' просмотров';
		
	    // Узнаём категорию статьи
	    $res2 = $mysqli->query("SELECT * FROM `categories` WHERE `id`='{$row['cat']}'");
	    while($row2 = $res2->fetch_assoc())
	    {
	        // Удаляем элемент $articles[$i]['cat'] из массива, т.к. он строка, а нам нужен массив
	    	unset($article['cat']);
	    	// Добавляем его заново, тип переменной сменится на array
	    	$article['cat'] = $row2;
	    }
	    $res2->close();
		
		$res2 = $mysqli->query("SELECT COUNT(*) AS cnt FROM `comments` WHERE `item`='{$article['id']}'");
		$article['comments'] = $res2->fetch_object()->cnt;
		$res2->close();
	}
	return $article;
}

/*
 * Последние комментарии
 *
 * @return array $comments - Массив с информацией о последних комментариях
 */
function last_comments() {
    global $mysqli;
	$i = 0;
	
	// Формируем запрос в базу
	$res = $mysqli->query("SELECT `id` AS id FROM `comments` ORDER BY `whenadd` DESC");
	// С помощью цикла складываем полученную информацию в массив
	while($id = $res->fetch_object()->id)
	{
	    $comments[$i] = comment($id);
		$i++;
	}
	$res->close();
	
	return $comments;
}

/*
 * Информация о комментарии по его ID
 *
 * @param integer $id - ID комментариях
 * @return array $comment - Информация о комментарии в массиве
 */
function comment($id) {
    global $mysqli;
	
	$res = $mysqli->query("SELECT * FROM `comments` WHERE `id`='{$id}'");
	while($row = $res->fetch_assoc())
	{
	    $comment = $row;
		$comment['whenadd'] = rusdate($row['whenadd']);
		$comment['article'] = article($row['item']);
	}
	$res->close();
	
	return $comment;
}

/*
 * Поиск по статьям
 *
 * @param array $data - Данные с формы
 * @return array $search - Результаты поиска в виде массива
 */
function search($data) {
    global $mysqli;
	$i = 1;
	
	$res = $mysqli->query("SELECT `id` AS id FROM `articles` WHERE 
	                                                                `title` LIKE '%".sip($data['search'])."%' OR 
													        		`url` LIKE '%".sip($data['search'])."%' OR 
													        		`description` LIKE '%".sip($data['search'])."%' OR 
													        		`keywords` LIKE '%".sip($data['search'])."%' 
													        ORDER BY `whenadd` DESC");
	while($id = $res->fetch_object()->id)
	{
	    $search[$i] = article($id);
		$search[$i]['description'] = croptext($search[$i]['description'], 400);
		$i++;
	}
	$res->close();
	return $search;
}

/*
 * Список тегов для облака тегов
 *
 * @return array - Массив со всеми тегами для облака
 */
function tags() {
    global $mysqli;
	$i = 0;
	$res = $mysqli->query("SELECT `keywords` as keywords FROM `articles`");
	while($keywords = $res->fetch_object()->keywords)
	{
	    $k_parts = explode(",", $keywords);
		foreach($k_parts as $key => $val)
		{
		    $tags[$i] = trim($val);
		    $i++;
		}
	}
	$res->close();
	return array_unique($tags);
}

function navigation($cat) {
    global $mysqli;
	$l = 0;
	$k = 0;
	
	$res = $mysqli->query("SELECT * FROM `categories` WHERE `parent`='{$cat}' ORDER BY `title`");
	$cnt = $res->num_rows;
	if($cnt > 0)
	{
	    while($row = $res->fetch_assoc())
	    {
	    	$k = 0;
			$navigation[$l] = $row;
	    	$navigation[$l]['articles'] = articles('nav', $row['id']);
			
	    	$res2 = $mysqli->query("SELECT * FROM `categories` WHERE `parent`='{$row['id']}' ORDER BY `title`");
	    	$cnt = $res2->num_rows;
	    	if($cnt > 0)
	    	{
	    	    while($row2 = $res2->fetch_assoc())
	    		{
					$navigation[$l]['cats'][$k] = $row2;
					$navigation[$l]['cats'][$k]['articles'] = articles('nav', $row2['id']);
	    			$k++;
	    		}
	    		$res2->close();
	    	};
	    	$l++;
	    }
	    $res->close();
	};

	return $navigation;
}

/*
 * Переводим дату из "гггг-мм-дд чч:мм:сс" в день и название месяца
 * @param $date string дата в формате гггг-мм-дд чч:мм:сс
 * @return string Число и название месяца
 */
function rusdate($date, $notext="")
{
    // Задаём в массиве соответствия между номером месяца и его названием
	$trans = array("01" => "января",
                   "02" => "февраля",
                   "03" => "марта",
                   "04" => "апреля",
                   "05" => "мая",
                   "06" => "июня",
                   "07" => "июля",
                   "08" => "августа",
                   "09" => "сентября",
                   "10" => "октября",
                   "11" => "ноября",
                   "12" => "декабря"
                   );
	
	// Разделяем строку с датой на дату и время
	$onlydate = explode(" ", $date);
	// Разделяем дату на день/месяц/год
	$date_part = explode("-", $onlydate[0]);
	// Если день содержит одну цифру, даём ей отступ, чтобы все даты, записанные друг под другом, стояли ровно
	$day_1 = mb_substr($date_part[2], 0, 1, 'UTF-8');
	$day_2 = mb_substr($date_part[2], 1, 2, 'UTF-8');
	if((int)$day_1 > 0 && (int)$day_2 >= 0)
	{
        $day = $day_1.$day_2;
	} else {
	    $day_3 = explode("0", $date_part[2]);
	    
		if(trim($day_3[2]) == '')
		{
		    $n = 1;
			$delim = '&nbsp;';
		} else {
		    $n = 0;
			$delim = '';
		}
		
		$day = $delim.$day_3[$n];
	}
	// Возвращаем уже готовую дату
	if($notext == 1)
	{
	    $tm = explode(":", $onlydate[1]);
		return $date_part[2].'.'.$date_part[1].'.'.$date_part[0].' в '.$tm[0].':'.$tm[1];
	} else {
	    return $day.' '.strtr($date_part[1], $trans);
	}
}

/*
 * Обрабатываем текст перед тем, как занести его в базу
 * @param $txt string Исходная строка
 * @return string $txt Обработанная строка
 */
function sip($txt)
{
    global $mysqli;
    $txt = $mysqli->real_escape_string($txt);
    $txt = strip_tags($txt);
    $txt = htmlspecialchars($txt);
    $txt = stripslashes($txt);
    $txt = addslashes($txt);
    return $txt;
}

/**
 * Проверяем переменную или одномерный массив на пустоту
 *
 * @param string/array $a Входное значение
 * @return integer Пустое/не пустое
 */
function it($a)
{
    if(is_array($a))
	{
	    foreach($a as $key => $val)
		{
			if(isset($a[$key]) && trim($a[$key]) != '')
			{
			    $it = true;
			} else {
			    $it = false;
				break;
			}
		}
	} else {
	    (isset($a) && trim($a) != '') ? $it = true : $it = false;
	}
	return $it;
}

/*
 * Вытаскиваем из текста первые одно-два предложения
 * @param $str string Исходная строка
 * @param $num integer Количество символов для предварительной обрезки текста
 * @return $str string Изменённая строка
 */
function croptext($str, $num)
{
	if(trim($str) != '')
	{
	    $separator = array('?', '!', '.');
	    $str2 = $str;
	    $str = strip_tags($str);
	    $str = substr($str, 0, $num);
	
        $i = strlen($str);
        do{
            $i--;
        }while(!in_array($str[$i], $separator) && $i >= 0);
	
        $string = substr($str, 0, ($i+1));
	
	    (strlen($string) <= 1) ? $str = cutString($str2, $num) : $str = $string;
	
	    return html_entity_decode($str);
	} else {
	    return $str;
	}
}

/**
 * Обрезка длинного текста до n символов
 *
 * @param string $string Исходная строка
 * @param integer $maxlen До какого количества символов нужно обрезать
 * @return string $cutStr Обрезанная строка
 */
function cutString($string, $maxlen)
{
	if(trim($string) != '')
	{
	    //$string = stripslashes(strip_tags($string));
	    $len = (mb_strlen($string) > $maxlen) ? mb_strripos(mb_substr($string, 0, $maxlen), ' ') : $maxlen;
        $cutStr = mb_substr($string, 0, $len);
        return (mb_strlen($string) > $maxlen) ? $cutStr.'...' : $cutStr;
	} else {
	    return $string;
	}
}

/*
 * Переводим кириллицу в транслит
 * @param $string string Строка с русскими символами
 * @return string Строка в транслите
 */
function rus2translit($string)
{
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    );
    return strtr($string, $converter);
}

/*
 * Формируем ЧПУ из строки, переведённой в транслит
 * @param $str string Исходная строка в транслите
 * @return $str string Изменённая строка для вставки в адресную строку
 */
function str2url($str)
{
	global $mysqli;
	$str = rus2translit($str);
    $str = strtolower($str);
    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
	$str = trim($str);
    $str = trim($str, "-");
	
	$res = $mysqli->query("SELECT COUNT(*) AS cnt FROM `articles` WHERE `url`='{$str}' OR `url` LIKE '{$str}-%'");
	$cnt = (int)$res->num_rows;
	if($cnt == 1)
	{
	    $str = $str.'-'.($cnt);
	} else {
	    $str = $str.'-'.($cnt-1);
	}
	$res->close();
	
    return $str;
}

function sendtopic($text) {
    $to  = ADMIN_EMAIL;
    
    // тема письма
    $subject = SITENAME.' | Новая тема для статьи';
    
    // текст письма
    $message = '
    <html>
    <head>
        <meta charset="utf-8">
		<title>'.$subject.'</title>
    </head>
    <body>
        <b>Предложена тема:</b> '.$text.'
    </body>
    </html>
    ';
    
    // Для отправки HTML-письма должен быть установлен заголовок Content-type
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    
    // Дополнительные заголовки
    $headers .= 'To: Admin <'.$to.'>' . "\r\n";
    $headers .= 'From: '.SITENAME.' <noreply@'.SITENAME.'>' . "\r\n";
    $headers .= 'Cc: noreply@'.SITENAME . "\r\n";
    $headers .= 'Bcc: noreply@'.SITENAME . "\r\n";
    
    // Отправляем
    mail($to, $subject, $message, $headers);
	//file_put_contents('msg.html', $message);
}

function materials($type, $cat="") {
    global $mysqli;
	$i = 0;

	switch($type)
	{
	    case('cats'):
		                $q = "SELECT * FROM `categories` ORDER BY `id` DESC";
						break;
	    case('cats_parents'):
		                $q = "SELECT * FROM `categories` WHERE `parent`='0' ORDER BY `id` DESC";
						break;
	    case('articles'):
		                $q = "SELECT * FROM `articles` WHERE `cat`='{$cat}' ORDER BY `id` DESC";
						break;
	    case('comments'):
		                $q = "SELECT * FROM `comments` ORDER BY `id` DESC";
						break;
	}
	
	$res = $mysqli->query($q);
	while($row = $res->fetch_assoc())
	{
	    $materials[$i] = $row;
		$i++;
	}
	$res->close();
	return $materials;
}

function syntax($txt) {
    $healthy = array('[PHP]', '[/PHP]', '[JS]', '[/JS]', '[CSS]', '[/CSS]', '[SQL]', '[/SQL]', '[HTML]', '[/HTML]', '[XML]', '[/XML]');
	$yummy = array('<pre class="brush: php">', '</pre>', '<pre class="brush: js">', '</pre>', '<pre class="brush: css">', '</pre>', '<pre class="brush: sql">', '</pre>', '<pre class="brush: html">', '</pre>', '<pre class="brush: xml">', '</pre>');
	$txt = str_replace($healthy, $yummy, $txt);
	return $txt;
}
?>