﻿<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="/templates/<?php echo TEMPLATE;?>/css/main.css">
    <link href="/templates/<?php echo TEMPLATE;?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<meta charset="utf-8">
	<title>
	<?php
	    echo SITENAME.' | Вход в админ-панель';
	?>
	</title>
</head>
<body>
    <form id="login_form" method="POST" action="/admin/">
	    <input type="text" name="adminlogin" placeholder="Логин" />
		<input type="password" name="adminpass" placeholder="******" />
		<input type="submit" value="Войти" />
	</form>
</body>
</html>