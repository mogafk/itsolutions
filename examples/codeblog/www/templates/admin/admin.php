﻿<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="/templates/<?php echo TEMPLATE;?>/css/main.css">
    <link href="/templates/<?php echo TEMPLATE;?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<meta charset="utf-8">
	<title><?php echo SITENAME.' | Админ-панель'; ?></title>
	<script src="/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
	<script>
	    $(document).ready(function() {
	        $(".lastcommentssploilerlink").click(function() {
	            var id = $(this).attr("data-id");
	        	var pos = $(this).attr("data-pos");
	        	
	        	if(pos == 0)
	        	{
	        	    $("#lastcommentssploiler-" + id).slideDown(250).animate({"opacity" : 1.0},250);
	        		$(this).attr("data-pos", 1);
	        	} else {
	        	    $("#lastcommentssploiler-" + id).animate({"opacity" : 0.7},250).slideUp(250);
	        		$(this).attr("data-pos", 0);
	        	}
	        	return false;
	        });
			
	        $(".lastcommentssploilerlink, .dellink, .editlink").hover(function() {
	            $('img',this).animate({"opacity" : 1.0},250);
	        },function() {
	            $('img',this).animate({"opacity" : 0.7},250);
	        });
			
			$(".closebig").click(function() {
			    $(".big").fadeOut(250);
				return false;
			});
			
			$(".editlink").click(function() {
			    var type1 = $(this).attr("data-type");
				var item = $(this).attr("data-id");
				
				if(type1 == 'cat')
				{
				    var title = $(this).attr("data-title");
					var parent = parseInt($(this).attr("data-parent"));
					$("#catname2").val(title);
					$("#item").val(item);
					$("#type1").val(type1);
					$("#parent2 option").each(function (i) {
					    if($(this).val() == parent)
						{
							$(this).val(parent).attr("selected", "selected").change();
						};
					});
					$("#editarticle").css("display", "none");
					$("#editcat").css("display", "block");
				} else {
				    var title = $(this).attr("data-title");
					var cat = parseInt($(this).attr("data-cat"));
					var descr = $("#descr-" + item).html();
					var kw = $(this).attr("data-keywords");

					$("#item2").val(item);
					$("#type2").val(type1);
					$("#article_name2").val(title);
					$("#meta_keywords2").val(kw);
					CKEDITOR.instances.description2.setData(descr);
					$("#article_cat2 option").each(function (i) {
					    if($(this).val() == cat)
						{
							$(this).val(cat).attr("selected", "selected").change();
						};
					});
					$("#editarticle").css("display", "block");
					$("#editcat").css("display", "none");
				}
				$(".big").fadeIn(250);
				return false;
			});
	    });
	</script>
</head>
<body>
    <div class="big">
		<form id="editcat" method="POST" action="/admin/">
		    <input id="catname2" type="text" name="catname2" placeholder="Название категории" style="width: 400px;" />
			<br />
			<select id="parent2" name="parent2" style="width: 400px;">
			    <option selected disabled>Выберите родителькую категорию</option>
				<option value="0">Нет родителя(новый раздел)</option>
				<?php
				    foreach(materials('cats_parents') as $key => $val)
					{
					    echo '<option value="'.$val['id'].'">'.$val['title'].'</option>';
					}								
				?>
			</select>
			<br />
			<input type="hidden" id="item" name="item" />
			<input type="hidden" id="type1" name="type1" />
			<input type="submit" value="Применить!" />
			<input class="closebig" type="button" value="Закрыть!" />
		</form>
		<form id="editarticle" method="POST" action="/admin/" style="width: 900px; margin-top: 20px; text-align: left;">
		    Заголовок статьи: <input id="article_name2" type="text" name="article_name2" placeholder="Название статьи" style="width: 400px;" />
			<br />
			Категория: <select id="article_cat2" name="article_cat2" style="width: 400px;">
			    <option selected disabled>Выберите категорию</option>
				<?php
				    foreach(materials('cats') as $key => $val)
					{
					    echo '<option value="'.$val['id'].'">'.$val['title'].'</option>';
					}								
				?>
			</select>
			<br />
			<textarea id="description2" name="description2">Введите текст</textarea>
			<script>
		        CKEDITOR.replace( 'description2' );
            </script>
			<br />
			<input id="meta_keywords2" type="text" name="meta_keywords2" placeholder="Тэги (через запятую)" style="width: 400px;" />
			<br />
			<input type="hidden" id="item2" name="item2" />
			<input type="hidden" id="type2" name="type2" />
			<input type="submit" value="Применить!" />
			<input class="closebig" type="button" value="Закрыть!" />
		</form>
	</div>
	<div id="login_form" style="margin-top: 0px; width: 110px; margin-left: 10px;">
	    <a href="/admin/?exit=yes">Выход</a> | <a href="http://<?php echo SITEURL;?>" target="_blank">На сайт</a>
	</div>
	<div id="all">
	    <table id="content" border="0" style="width: 100%; margin: 0 auto;">
		    <thead>
			    <tr>
				    <th>Категории и статьи</th>
					<th>Комментарии</th>
				</tr>
			</thead>
			<tbody>
			    <tr>
				    <td>
					    <h3 class="sectionname">
							<a href="#" class="lastcommentssploilerlink" data-id="newsection" data-pos="0">
							    <img src="/templates/<?php echo TEMPLATE;?>/images/down.png" /> Новая категория
							</a>
						</h3>
						<div class="lastcommentssploiler" id="lastcommentssploiler-newsection">
						    <form method="POST" action="/admin/">
						        <input type="text" name="catname" placeholder="Название категории" style="width: 400px;" />
						    	<br />
						    	<select name="parent" style="width: 400px;">
						    	    <option selected disabled>Выберите родителькую категорию</option>
						    		<option value="0">Нет родителя(новый раздел)</option>
						    		<?php
						    		    foreach(materials('cats_parents') as $key => $val)
						    			{
						    			    echo '<option value="'.$val['id'].'">'.$val['title'].'</option>';
						    			}								
						    		?>
						    	</select>
						    	<br />
						    	<input type="submit" value="Создать!" />
						    </form>
						</div>
						<br />
					    <h3 class="sectionname">
							<a href="#" class="lastcommentssploilerlink" data-id="newarticle" data-pos="0">
							    <img src="/templates/<?php echo TEMPLATE;?>/images/down.png" /> Новая статья
							</a>
						</h3>
						<div class="lastcommentssploiler" id="lastcommentssploiler-newarticle">
						    <form method="POST" action="/admin/">
						        <input type="text" name="article_name" placeholder="Заголовок статьи" style="width: 400px;" />
						    	<br />
						    	<select name="article_cat" style="width: 400px;">
						    	    <option selected disabled>Выберите категорию</option>
						    		<?php
						    		    foreach(materials('cats') as $key => $val)
						    			{
						    			    echo '<option value="'.$val['id'].'">'.$val['title'].'</option>';
						    			}								
						    		?>
						    	</select>
						    	<br />
						    	<textarea id="description" name="description">Введите текст</textarea>
						    	<script>
					                CKEDITOR.replace( 'description' );
                                </script>
						    	<br />
						    	<input type="text" name="meta_keywords" placeholder="Тэги (через запятую)" style="width: 400px;" />
						    	<br />
						    	<input type="submit" value="Создать!" />
						    </form>
						</div>
						<br />
						<h3 class="sectionname"> Категории и статьи </h3>
						<br />
						<?php
						    $materials = "";
							$cats = materials('cats');
							foreach($cats as $key => $val)
							{
							    $articles = materials('articles', $val['id']);
								$cnt = count($articles);
								$materials .= '<h3 class="sectionname">
										           <a href="/admin/?delete='.$val['id'].'&type=cat" class="dellink" title="Удалить">
											           <img src="/templates/'.ADMIN_TEMPLATE.'/images/icn_alert_error.png" />
											       </a>
										           <a href="#" class="editlink" data-id="'.$val['id'].'" data-type="cat" data-title="'.$val['title'].'" data-parent="'.$val['parent'].'" data-id="'.$val['id'].'" title="Редактировать">
											           <img src="/templates/'.ADMIN_TEMPLATE.'/images/edit.png" />
											       </a>
												   <a href="#" class="lastcommentssploilerlink" data-id="article_'.$val['id'].'" data-pos="0">
							                           <img src="/templates/'.TEMPLATE.'/images/down.png" /> <b>('.$cnt.')</b> <a href="/category/'.$val['url'].'/" target="_blank">Открыть на сайте</a> '.$val['title'].'
							                       </a>
						                       </h3>
											   <div class="lastcommentssploiler" id="lastcommentssploiler-article_'.$val['id'].'">';
								if($cnt > 0)
								{
								    foreach($articles as $key2 => $val2)
								    {
								        $val3 = article($val2['id']);
								    	$delim = ', ';
								    	$keyw = "";
										foreach($val3['keywords'] as $key4 => $val4)
								    	{
								    	    if($key4 == count($val3['keywords'])-1) $delim = '';
								    		$keyw .= $val4.$delim;
								    	}
										$materials .= '
                                                       <div class="article">
										                   <a href="/admin/?delete='.$val3['id'].'&type=article" class="dellink" title="Удалить">
											                   <img src="/templates/'.ADMIN_TEMPLATE.'/images/icn_alert_error.png" />
											               </a>
										                   <a href="#" class="editlink" data-id="'.$val3['id'].'" data-type="article" data-title="'.$val3['title'].'" data-keywords="'.$keyw.'" data-cat="'.$val3['cat']['id'].'" data-id="'.$val3['id'].'" title="Редактировать">
											                   <img src="/templates/'.ADMIN_TEMPLATE.'/images/edit.png" />
											               </a>
														   <div class="descrcopypaste" id="descr-'.$val3['id'].'">
														   '.$val3['description'].'
														   </div>
														   <br />
														   <h3 class="sectionname">
														       <a href="'.$val3['url'].'">'.$val3['title'].'</a>
														   </h3>
                                                       	   <span class="lastarticlestime" style="float: right; font-size: 8px !important; margin-top: 6px; margin-right: 18px;">'.$val3['whenadd'].'<br />'.$val3['hits'].'</span>
                                                       	   <div class="searchresult">'.croptext($val3['description'], 400).'</div>
                                                       	   <span class="lastarticlestime" style="float: right; font-size: 8px !important; margin-top: 0px;"><a href="'.$val3['url'].'" target="_blank">Читать на сайте</a><br />Комментарии: '.$val3['comments'].'</span>
                                                       	   <div class="keywords">
														   '.$keyw.'
														   </div>
                                                       </div>';
								    }
								} else {
								    $materials .= '<div class="keywords">Не найдено ни одной статьи.</div>';
								}
								$materials .= '</div><br />';
							}
							echo $materials;
						?>
					</td>
					<td style="width: 260px;">
					    <ul class="noliststyle" id="admcomments">
						    <?php
								foreach(materials('comments') as $key => $val2)
						    	{
									$val = comment($val2['id']);
									echo '<li>
										      <a href="/admin/?delete='.$val['id'].'&type=comment" class="dellink" title="Удалить">
											      <img src="/templates/'.ADMIN_TEMPLATE.'/images/icn_alert_error.png" />
											  </a>
											  <a href="#" class="lastcommentssploilerlink" data-id="'.$val['id'].'" data-pos="0">
										          <img src="/templates/'.TEMPLATE.'/images/down.png" />
										      </a>
										      <span class="lastarticlestime">'.$val['whenadd'].'</span>
										      <a href="/category/'.$val['article']['cat']['url'].'/article/'.$val['article']['url'].'/" target="_blank">
										          '.$val['article']['title'].'
										      </a>
										      <br />
										      <div class="lastcommentssploiler" id="lastcommentssploiler-'.$val['id'].'">
										      <b>'.$val['name'].':</b>
											  <br />
											  <br />
											  '.$val['comment'].'
											  <br />
											  <br />
											  <a href="mailto:'.$val['email'].'">'.$val['email'].'</a>
										      </div>
										  </li>';
						    	}
						    ?>
						</ul>
					</td>
			</tbody>
		</table>
	</div>
</body>
</html>