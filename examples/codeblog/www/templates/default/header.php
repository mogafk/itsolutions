﻿<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="/templates/<?php echo TEMPLATE;?>/css/main.css">
    <link href="/templates/<?php echo TEMPLATE;?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <!-- Подключаем таблицы стилей, ядро скрипта и добавляем функцию для подсветки синтаксиса-->
    <link type="text/css" rel="stylesheet" href="/js/syntaxhighlighter_3.0.83/styles/shCore.css" />
    <link type="text/css" rel="stylesheet" href="/js/syntaxhighlighter_3.0.83/styles/shThemeDefault.css" />
    <script type="text/javascript" src="/js/syntaxhighlighter_3.0.83/scripts/shCore.js"></script>
    <script type="text/javascript" src="/js/syntaxhighlighter_3.0.83/scripts/shBrushPhp.js"></script>
    <script type="text/javascript" src="/js/syntaxhighlighter_3.0.83/scripts/shBrushCss.js"></script>
    <script type="text/javascript" src="/js/syntaxhighlighter_3.0.83/scripts/shBrushJScript.js"></script>
    <script type="text/javascript" src="/js/syntaxhighlighter_3.0.83/scripts/shBrushSql.js"></script>
    <script type="text/javascript" src="/js/syntaxhighlighter_3.0.83/scripts/shBrushXml.js"></script>
    <script type="text/javascript">SyntaxHighlighter.all();</script>
	<title>
	<?php
	    echo SITENAME;
		if($controller != 'category') echo ' | '.$controllers_name[$controller];
		if($category != '') echo ' | '.$cat['title'];
		if($item != '') echo ' | '.$article['title'];
	?>
	</title>
</head>
<body>
    <div id="all">
	    <header>
		    <a href="/">
			    <img src="<?php echo LOGO;?>" />
			</a>
			<div id="lastarticles">
			    <div>
				    <h3>Свежие статьи</h3>
				    <ul class="noliststyle">
				        <?php
						$last_articles = articles('last');
						if(count($last_articles) > 0)
						{
						    foreach($last_articles as $key => $val) {
						        echo '<li><span class="lastarticlestime">'.$val['whenadd'].'</span> <a href="/category/'.$val['cat']['url'].'/article/'.$val['url'].'/">'.$val['title'].'</a></li>';
						    }
						} else {
						    echo '<li>Статей не найдено.</li>';
						}
						?>
				    </ul>
				</div>
				<div>
				    <h3>Популярные статьи</h3>
				    <ul class="noliststyle">
				        <?php
						$pop_articles = articles('pop');
						if(count($last_articles) > 0)
						{
						    foreach($pop_articles as $key => $val) {
								echo '<li><span class="lastarticlestime">'.$val['hits'].'</span> <a href="/category/'.$val['cat']['url'].'/article/'.$val['url'].'/">'.$val['title'].'</a></li>';
						    }
						} else {
						    echo '<li>Статей не найдено.</li>';
						}
						?>
				    </ul>
				</div>
			</div>
		</header>
		<table class="navpanel">
		    <tr>
			    <td class="main">
				    <ul class="noliststyle">
					    <li><a href="/"><?php echo SITENAME;?></a></li>
						<li> <img src="/templates/<?php echo TEMPLATE;?>/images/arrow.png" /> </li>
						<?php breadcrumbs($pageinfo, '/templates/'.TEMPLATE.'/images/arrow.png');?>
					</ul>
				</td>
				<td class="right">
				    <form id="search_form" method="POST" action="/search/">
					    <input id="search" type="text" name="search" placeholder="Найти статью" value="<?php echo $_POST['search'];?>" />
						<button id="search_submit_btn" type="button"></button>
					</form>
				</td>
			</tr>
		</table>
		<table id="content">
			<tr>
			    <td class="main">
				    <div class="mainwrapper">