﻿<?php
if(!empty($_POST) && isset($_POST['search']) && trim($_POST['search']) != '')
{
?>
<h3 class="sectionname">Поиск по запросу "<?php echo sip($_POST['search']);?>"</h3>
<form id="search_form2" method="POST" action="/search/">
    <input id="search2" type="text" name="search" placeholder="Найти статью" value="<?php echo $_POST['search'];?>" />
	<button id="search_submit_btn2" type="button"></button>
</form>
<br />
<h3 class="sectionname">Результаты поиска</h3>
<br />
<?php
    $search = search($_POST);
    if(count($search) > 0)
    {
        foreach($search as $key => $val)
    	{
            $keywords = '<ul class="searchkeywords">';
    		foreach($val['keywords'] as $key2 => $val2)
    		{
    		    $keywords .= '<li><a class="searchkeywordslink" href="#" data-keyword="'.$val2.'">'.$val2.'</a></li>';
    		}
    		$keywords .= '</ul>';
    		
    		echo '<div class="searchresult">
    		          <a href="/category/'.$val['cat']['url'].'/article/'.$val['url'].'/" style="font-weight: bold;">'.$val['title'].'</a>
    				  <br />
					  <span class="lastarticlestime">в разделе <a href="#" class="searchkeywordslink" data-keyword="'.$val['cat']['title'].'">'.$val['cat']['title'].'</a></span>
					  <br />
					  <div class="smallsearchdescr">
    				      '.$val['description'].'
					  </div>
    				  <br />
    				  '.$keywords.'
					  <span class="lastarticlestime" style="float: right; font-size: 8px !important; margin-top: 12px;">'.$val['whenadd2'].'</span>
    			  </div>';
    	}
    } else {
        echo 'По вашему запросу ничего не найдено.';
    }
} else {
?>

<h3 class="sectionname">Поиск</h3>
<form id="search_form2" method="POST" action="/search/">
    <input id="search2" type="text" name="search" placeholder="Найти статью" value="<?php echo $_POST['search'];?>" />
	<button id="search_submit_btn2" type="button"></button>
</form>

<?php
}
?>