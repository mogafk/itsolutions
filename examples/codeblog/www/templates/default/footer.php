﻿					</div>
				</td>
				<td class="right">
				    <div class="rightwrapper">
					    <h3>Навигация</h3>
					    <ul class="catlist" style="background: transparent; border: none;">
					        <li class="pagelink"><a href="/">Главная</a></li>
					    	<li class="pagelink"><a href="/search/">Поиск</a></li>
					    	<!--<li class="pagelink"><a href="/contacts/">Контакты</a></li>-->
						    <?php
						    $nav = "";
							$navigation = navigation(0);
						    foreach($navigation as $key => $val)
						    {
					            $nav .= '<li><a href="#" class="spoilerlink" id="spoilerlink-'.$val['url'].'" data-parent="0" data-url="'.$val['url'].'" data-pos="0">'.$val['title'].'</a>
					            		 <div class="menuspoler" id="'.$val['url'].'-spoiler">';
											 
								if(count($val['articles']) == 0)
								{
								    //$nav .= '<span class="lastarticlestime">Статей не найдено.</span>';
								} else {
								    $nav .= '<br /><ul class="articlelist">';
									foreach($val['articles'] as $key2 => $val2)
								    {
								        $nav .= '<li><a href="/category/'.$val['url'].'/article/'.$val2['url'].'/">'.$val2['title'].'</a></li>';
								    }
									$nav .= '</ul>';
								}
								
					            $nav .= '<ul class="catlist">';
								
								foreach($val['cats'] as $key2 => $val2)
								{
									$nav .= '<li>
						        		 	     <a href="#" class="spoilerlink childspoilerlink" id="spoilerlink-'.$val2['url'].'" data-parent="'.$val['url'].'" data-url="'.$val2['url'].'" data-pos="0">'.$val2['title'].'</a>
					            		     	 <div class="menuspoler childspoiler" id="'.$val2['url'].'-spoiler">';
												 
									if(count($val2['articles']) == 0)
									{
									    $nav .= '<span class="lastarticlestime">Статей не найдено.</span>';
									} else {
									    $nav .= '<br /><ul class="articlelist">';
										foreach($val2['articles'] as $key3 => $val3)
									    {
									    	$nav .= '<li><a href="/category/'.$val2['url'].'/article/'.$val3['url'].'/">'.$val3['title'].'</a></li>';
									    }
										$nav .= '</ul>';
									}
									
									$nav .= '	  </div>
						        		 	 </li>';
								}
								$nav .= '</ul>
						             </div>
									 </li>';
						    }
							echo $nav;
						    ?>
					    </ul>
					    <br />
					    <h3>Последние комментарии</h3>
					    <ul class="noliststyle" id="lastcomments">
							<?php
						        $last_comments = last_comments();
						        if(count($last_comments) > 0)
						        {
						            foreach($last_comments as $key => $val) {
						                echo '<li>
										          <a href="#" class="lastcommentssploilerlink" data-id="'.$val['id'].'" data-pos="0">
											          <img src="/templates/'.TEMPLATE.'/images/down.png" />
											      </a>
												  <span class="lastarticlestime">'.$val['whenadd'].'</span>
												  <b>'.$val['name'].'</b> в 
												  <a href="/category/'.$val['article']['cat']['url'].'/article/'.$val['article']['url'].'/">
												      '.$val['article']['title'].'
												  </a>
												  <br />
												  <div class="lastcommentssploiler" id="lastcommentssploiler-'.$val['id'].'">
												  '.$val['comment'].'
												  </div>
											  </li>';
						            }
						        } else {
						            echo '<li>Пока никто ничего не написал...</li>';
						        }
							?>
					    </ul>
						<?php if(ADVERTISMENT == "on") { ?>
					    <br />
					    <h3>Реклама</h3>
					    <div id="advertisment">
					    </div>
						<?php }; ?>
					    <br />
					    <h3>Предложить тему статьи</h3>
					    <form id="sendtopic_form" method="POST" action="javascript:sendtopic1();">
					        <input id="sendtopic" type="text" name="sendtopic" placeholder="О чём написать?" />
					    	<button id="sendtopic_submit_btn" type="button" OnClick="javascript:sendtopic1();"></button>
							<div class="alert_form" id="sendtopic_success">Тема отправлена! Спасибо!</div>
					    </form>
						<br />
						<h3>Облако тегов</h3>
                        <div id="myCanvasContainer">
                          <canvas id="myCanvas">
                            <p>Anything in here will be replaced on browsers that support the canvas element</p>
                          </canvas>
                        </div>
                        <div id="tags">
                          <ul>
                            <?php
							    foreach(tags() as $key => $val)
							    {
							        echo '<li><a href="#" class="tagkeywordslink" data-keyword="'.$val.'">'.$val.'</a></li>';
                                }
							?>
                          </ul>
                        </div>
					</div>
				</td>
			</tr>
		</table>
		<footer>
		    <a href="/"><? echo SITENAME;?></a> by <? echo COPYRIGHT;?> &copy; <span id="copy_date">2014</span>
		</footer>
	</div>
	<script src="/js/jquery-1.10.2.min.js"></script>
	<script src="/js/jquery.tagcanvas.min.js"></script>
	<script src="/js/main.js"></script>
</body>
</html>